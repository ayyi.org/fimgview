/*
 +----------------------------------------------------------------------+
 | This file is part of Imgview.                                        |
 | copyright (C) 2007-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

typedef struct _Config       Config;
typedef struct _ConfigOption ConfigOption;

struct _ConfigOption {
   char*   name;
   GValue  val;
   GValue  min;
   GValue  max;
   void    (*save)(ConfigOption*);
};

typedef struct {
   const char*       dir;
   char*             filename;
   ConfigOption**    options;   // null terminated.
} ConfigContext;

struct _Config
{
	const char* dir;
	char        column_widths[4][8];
};

#if 0
bool          config_save              (ConfigContext*);

ConfigOption* config_option_new_int    (char* name, void (*save)(ConfigOption*), int min, int max);
ConfigOption* config_option_new_string (char* name, void (*save)(ConfigOption*));
ConfigOption* config_option_new_bool   (char* name, void (*save)(ConfigOption*));
ConfigOption* config_option_new_manual (void (*save)(ConfigOption*));
#endif
