/*
 +----------------------------------------------------------------------+
 | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
 | copyright (C) 2012-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */
#include "config.h"
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/cursorfont.h>
#include "debug/debug.h"
#include "agl/ext.h"
#include "src/glx.h"

extern Display* dpy;


/*
 *  This sends a message, so must be used after the window is mapped
 */
#if 0
void
go_above (Display* dpy, Window window)
{
	#define _NET_WM_STATE_REMOVE        0    // remove/unset property
	#define _NET_WM_STATE_ADD           1    // add/set property
	#define _NET_WM_STATE_TOGGLE        2    // toggle property

	Atom wmStateAbove = XInternAtom(dpy, "_NET_WM_STATE_ABOVE", 1 );
	if(wmStateAbove) {
		printf("_NET_WM_STATE_ABOVE has atom of %ld\n", (long)wmStateAbove);
	} else {
		printf("ERROR: cannot find atom for _NET_WM_STATE_ABOVE !\n");
	}

	Atom wmNetWmState = XInternAtom(dpy, "_NET_WM_STATE", 1);
	if(wmNetWmState != None ) {
		printf("_NET_WM_STATE has atom of %ld\n", (long)wmNetWmState);
	} else {
		printf("ERROR: cannot find atom for _NET_WM_STATE !\n");
	}

	// set window always on top hint
	if(wmStateAbove){
		XClientMessageEvent xclient;
		memset(&xclient, 0, sizeof (xclient));
		//
		//window  = the respective client window
		//message_type = _NET_WM_STATE
		//format = 32
		//data.l[0] = the action, as listed below
		//data.l[1] = first property to alter
		//data.l[2] = second property to alter
		//data.l[3] = source indication (0-unk,1-normal app,2-pager)
		//other data.l[] elements = 0
		//
		xclient.type = ClientMessage;
		xclient.window = window; // GDK_WINDOW_XID(window);
		xclient.message_type = wmNetWmState; //gdk_x11_get_xatom_by_name_for_display( display, "_NET_WM_STATE" );
		xclient.format = 32;
		xclient.data.l[0] = _NET_WM_STATE_ADD; // add ? _NET_WM_STATE_ADD : _NET_WM_STATE_REMOVE;
		xclient.data.l[1] = wmStateAbove; //gdk_x11_atom_to_xatom_for_display (display, state1);
		xclient.data.l[2] = 0; //gdk_x11_atom_to_xatom_for_display (display, state2);
		xclient.data.l[3] = 0;
		xclient.data.l[4] = 0;

		XSendEvent(dpy,
			DefaultRootWindow(dpy),
			False,
			SubstructureRedirectMask | SubstructureNotifyMask,
			(XEvent *)&xclient
		);
	}
}
#endif


void
hide_cursor (Window window)
{
	static char noData[] = {0, 0, 0, 0, 0, 0, 0, 0};
	XColor black = {0,};

	Pixmap bitmapNoData = XCreateBitmapFromData(dpy, window, noData, 8, 8);
	Cursor invisibleCursor = XCreatePixmapCursor(dpy, bitmapNoData, bitmapNoData, &black, &black, 0, 0);
	XDefineCursor(dpy, window, invisibleCursor);
	XFreeCursor(dpy, invisibleCursor);
	XFreePixmap(dpy, bitmapNoData);
}


void
show_cursor (Window window)
{
	Cursor cursor = XCreateFontCursor(dpy, XC_left_ptr);
	XDefineCursor(dpy, window, cursor);
	XFreeCursor(dpy, cursor);
}
