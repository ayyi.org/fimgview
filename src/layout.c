/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2007-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <glib/gstdio.h>
#include <errno.h>
#include <debug/debug.h>
#include "agl/actor.h"
#include "yaml/utils.h"
#include "yaml/load.h"
#include "application.h"
#include "behaviours/state.h"
#include "layout.h"

extern bool agl_is_fullscreen (Window);
extern void agl_is_maximised  (Window, int*, int*);

typedef AGlActorClass* (get_class)();
get_class dock_v_get_class, dock_h_get_class, scrollable_view_get_class, files_view_get_class, directories_view_get_class, scrollbar_view_get_class, img_view_get_class, statusbar_get_class;
#include "views/dock_v.h"
#include "views/tabs.h"

GHashTable* agl_actor_registry; // maps className to AGlActorClass


static void
agl_actor_register_class (const char* name, AGlActorClass* class)
{
	static int i = 0;
	class->type = ++i;
	g_hash_table_insert(agl_actor_registry, (char*)name, class);
}


static bool
config_load_point (yaml_parser_t* parser, const char*, gpointer _pt)
{
	AGliPt* pt = _pt;
	g_auto(yaml_event_t) event;

	for (int i=0;i<2;i++) {
		if (yaml_parser_parse(parser, &event)) {
			switch (event.type) {
				case YAML_SCALAR_EVENT:
					;int* a = i ? &pt->y : &pt->x;
					*a = atoi((char*)event.data.scalar.value);
					break;
				default:
					break;
			}
		}
		yaml_event_delete(&event);
	}

	get_expected_event(parser, &event, YAML_SEQUENCE_END_EVENT);

	return true;
}


static bool
config_load_size (yaml_parser_t* parser, const char*, gpointer _actor)
{
	AGlActor* actor = _actor;

	AGliPt pt = {0};
	config_load_point(parser, NULL, &pt);

	actor->region.x2 = actor->region.x1 + pt.x;
	actor->region.y2 = actor->region.y1 + pt.y;

	return true;
}


static bool
config_load_position (yaml_parser_t* parser, const char*, gpointer _actor)
{
	AGlActor* actor = _actor;

	AGliPt pt = {0};
	config_load_point(parser, NULL, &pt);

	actor->region.x1 = pt.x;
	actor->region.y1 = pt.y;
	actor->region.x2 += pt.x;
	actor->region.y2 += pt.y;

	return true;
}


static bool
load_size_req (yaml_parser_t* parser, const yaml_event_t* event, const char* key, gpointer _actor)
{
	g_return_val_if_fail(event->type == YAML_MAPPING_START_EVENT, false);

	AGlActor* actor = _actor;
	if (actor->class == panel_view_get_class() || actor->class == dock_v_get_class() || actor->class == dock_h_get_class()) {
		return yaml_load_section(parser,
			NULL,
			NULL,
			(YamlSequenceHandler[]){
				{"min", config_load_point, &((PanelView*)actor)->size_req.min},
				{"preferred", config_load_point, &((PanelView*)actor)->size_req.preferred},
				{"max", config_load_point, &((PanelView*)actor)->size_req.max}
			},
			actor
		);
	}
	else pwarn("no class match: class=%p", actor->class);

	return false;
}


#define STACK_SIZE 32

typedef struct {
	AGlActor* items[STACK_SIZE];
	int       sp;
	char      name[64];
} Stack;

#define STACK_PUSH(ACTOR) ({if(stack->sp >= STACK_SIZE) return FALSE; stack->items[++stack->sp] = ACTOR;})
#define STACK_POP() ({AGlActor* a = stack->items[stack->sp]; stack->items[stack->sp--] = NULL; a;})


void
_set_string (char** location, char* value)
{
	if (*location)
		g_free(*location);
	*location = g_strdup(value);
}


static bool
type_handler (const yaml_event_t* event, const char*, gpointer _stack)
{
	g_return_val_if_fail(event->type == YAML_SCALAR_EVENT, false);

	Stack* stack = _stack;

	if (!strcmp((char*)event->data.scalar.value, "ROOT")) {
		STACK_PUSH((AGlActor*)app->scene);
	} else {
		AGlActorClass* c = g_hash_table_lookup(agl_actor_registry, event->data.scalar.value);
		if (c) {
			if (c->new) {
				STACK_PUSH(c->new(NULL));
				_set_string(&stack->items[stack->sp]->name, stack->name);
			}
		} else {
			pwarn("type not found: %s", event->data.scalar.value);
		}
	}

	return true;
}


static bool
generic_handler (const yaml_event_t* event, const char* key, gpointer _stack)
{
	g_return_val_if_fail(event->type == YAML_SCALAR_EVENT, false);

	Stack* stack = _stack;
	if (stack->sp < 0) return false;
	AGlActor* actor = stack->items[stack->sp];
	g_return_val_if_fail(actor, false);

	if (state_has_parameter(actor, key)) {
		state_set_named_parameter(actor, key, (char*)event->data.scalar.value);
		return true;
	}

	return false;
}


/*
 *  The previous event should be YAML_MAPPING_START_EVENT
 *
 *  Parsing is done manually so as to handle the MAPPING_END
 */
static bool
add_node (yaml_parser_t* parser, const yaml_event_t* _event, const char* node_name, gpointer _stack)
{
	g_assert(_event->type == YAML_MAPPING_START_EVENT);

	Stack* stack = _stack;
	g_strlcpy(stack->name, node_name, 64);
	char key[64] = {0,};

	yaml_event_t event;
	while (yaml_parser_parse(parser, &event)) {
		switch (event.type) {
			case YAML_SCALAR_EVENT:
				dbg(2, "YAML_SCALAR_EVENT: value='%s' %lu plain=%i style=%i", event.data.scalar.value, event.data.scalar.length, event.data.scalar.plain_implicit, event.data.scalar.style);

				g_strlcpy(key, (char*)event.data.scalar.value, 64);

				bool handled = handle_scalar_event(parser, &event, (YamlHandler[]){
					{"type", type_handler, stack},
					{NULL}
				}) ||
				handle_mapping_event(parser, &event, (YamlMappingHandler[]){
					{"size-req", load_size_req, stack->items[stack->sp]},
					{NULL}
				}) ||
				handle_sequence_event(parser, &event, (YamlSequenceHandler[]){
					{"position", config_load_position, stack->items[stack->sp]},
					{"size", config_load_size, stack->items[stack->sp]},
					{NULL}
				});

				if (handled) {
					key[0] = '\0';
				} else {
					AGlActor* actor = stack->items[stack->sp];
					g_return_val_if_fail(actor, false);
					if (state_has_parameter(actor, key)) {
						g_auto(yaml_event_t) event2;
						get_expected_event(parser, &event2, YAML_SCALAR_EVENT);
						return generic_handler(&event2, key, stack);
					}
				}
				break;
			case YAML_MAPPING_START_EVENT:
				if (key[0]) {
					add_node(parser, &event, key, stack);
					key[0] = '\0';
				}
				break;
			case YAML_MAPPING_END_EVENT:
				dbg(2, "<-");
				if (stack->sp > 0) {
					dbg(2, "<- sp=%i: adding '%s' to '%s'", stack->sp, stack->items[stack->sp] ? stack->items[stack->sp]->name : NULL, stack->items[stack->sp - 1] ? stack->items[stack->sp - 1]->name : NULL);
					AGlActor* actor = stack->items[stack->sp];
					AGlActor* parent = stack->items[stack->sp - 1];
					AGlActorClass* parent_class = parent->class;
					AGlActor* child = STACK_POP();
					if (parent_class == dock_v_get_class() || parent_class == dock_h_get_class()) {
						dock_v_add_panel((DockVView*)stack->items[stack->sp], child);
						{
							if (actor->class == panel_view_get_class()) {
								g_return_val_if_fail(g_list_length(actor->children) == 1, false);
								AGlActor* child = (AGlActor*)actor->children->data;
								char* name = child->name;
								if (strcmp(name, "Search") && strcmp(name, "Spectrogram") && strcmp(name, "Filters") && strcmp(name, "Tabs") && strcmp(name, "Waveform") && strcmp(name, "Image")) {
									if (!strcmp(name, "Scrollable")) {
										name = ((AGlActor*)child->children->data)->name;
									}
									((PanelView*)actor)->title = name;
								}
							}
						}
					} else if (parent_class == tabs_view_get_class()) {
						tabs_view__add_tab((TabsView*)parent, actor->name, child);
					} else {
						agl_actor__add_child(stack->items[stack->sp], child);
					}
					g_signal_emit_by_name(app, "actor-added", actor);
				}

				yaml_event_delete(&event);
				return true;

			case YAML_NO_EVENT:
			case YAML_SEQUENCE_START_EVENT:
			case YAML_SEQUENCE_END_EVENT:
			default:
				pwarn("unexpected parser event type: %i", event.type);
				yaml_event_delete(&event);
				return false;
		}
		yaml_event_delete(&event);
	}

	return false;
}


static bool
window_handler (yaml_parser_t* parser, const yaml_event_t*, const char* key, gpointer)
{
	// At this point in the parsing, we have just found a "ROOT" map. First event should be a YAML_SCALAR_EVENT...

	Stack stack = {.sp = -1};

	// preserve the root size which is given by the display server
	// (usually this is not needed because the config is loaded before the display configure event)
	AGlfRegion region = ((AGlActor*)app->scene)->region;

	bool ok = yaml_load_section (parser,
		(YamlHandler[]){
			{"type", type_handler, &stack},
			{NULL, generic_handler, &stack},
			{NULL,}
		},
		(YamlMappingHandler[]){
			{NULL, add_node, &stack},
			{NULL,}
		},
		(YamlSequenceHandler[]){
			// although these are overridden, it appears they are needed during the layout process
			{"size", config_load_size, app->scene},
			{"position", config_load_position, app->scene},
			{NULL,}
		},
		&stack
	);

	((AGlActor*)app->scene)->region = region;

	return ok;
}


static gboolean
layout_set_size (gpointer data)
{
	agl_actor__set_size((AGlActor*)app->scene);

	return G_SOURCE_REMOVE;
}


static bool
config_load_windows (yaml_parser_t* parser, const yaml_event_t* _, const char* name, gpointer user_data)
{
	yaml_load_section(parser,
		NULL,
		(YamlMappingHandler[]){
			{NULL, window_handler}, // one window expected, we don't care about the name
			{NULL}
		},
		NULL,
		user_data
	);

#if 0
#ifdef DEBUG
	bool check_dock_layout(AGlActor* actor)
	{
		int height = agl_actor__height(actor);

		int yl = 0;
		if(!strcmp(actor->name, "Dock H")){
			GList* l = actor->children;
			for(;l;l=l->next){
				AGlActor* child = l->data;
				yl = child->region.y2;
			}
		}
		dbg(0, "-> %i %i parent=%i", height, yl, actor->parent->region.y2);
		return true;
	}

	bool check_layout(AGlActor* actor)
	{
		GList* l = actor->children;
		for(;l;l=l->next){
			AGlActor* child = l->data;
			dbg(0, "  %s", child->name);
			if(!strcmp(child->name, "Dock H") || !strcmp(child->name, "Dock V")){
				check_dock_layout(child);
			}else{
				check_layout(child);
			}
		}
		return true;
	}
	check_layout((AGlActor*)app->scene);
#endif
#endif

	if(!g_list_length(((AGlActor*)app->scene)->children)){
		pwarn("layout did not load - pls check config file");
		return false;
	}

	g_idle_add(layout_set_size, NULL);

	return true;
}


static FILE*
open_settings_file (bool safe)
{
	char* cwd = g_get_current_dir();

	char* paths[] = {
		g_strdup_printf("%s/.config/" PACKAGE, g_get_home_dir()),
		g_build_filename(PACKAGE_DATA_DIR "/" PACKAGE, NULL),
		g_build_filename(cwd, "resources", NULL),
		cwd
	};

	FILE* fp = ({
		for (int i = safe ? 1 : 0; i < G_N_ELEMENTS(paths); i++) {
			g_autofree char* filename = g_strdup_printf("%s/"PACKAGE".yaml", paths[i]);
			fp = fopen(filename, "rb");
			if (fp) {
				if (_debug_) printf("using config file '%s'\n", filename);
				break;
			}
		}
		fp;
	});

	if (!fp) {
		fprintf(stderr, "unable to load config file %s/"PACKAGE".yaml\n", paths[0]);

#if 0
		GBytes* bytes = g_resources_lookup_data ("/imgview/"PACKAGE".yaml", 0, NULL);
		const char* str = g_bytes_get_data(bytes, 0);
#endif
	}
	for (int i=0;i<G_N_ELEMENTS(paths);i++) {
		g_free(paths[i]);
	}

	return fp;
}


AGliPt
get_window_size_from_settings (bool* fullscreen, bool* maximised)
{
	AGliPt size = {640, 360};

	g_auto(yaml_parser_t) parser; yaml_parser_initialize(&parser);

	FILE* fp = open_settings_file(false);
	if (fp) {
		//yaml_parser_set_input_string(yaml_parser_t *parser, const unsigned char *input, size_t size);
		yaml_parser_set_input_file(&parser, fp);

		{
			g_auto(yaml_event_t) event;
			if (find_event(&parser, &event, "ROOT")) {
				yaml_parser_parse(&parser, &event); // enter the ROOT mapping

				yaml_load_section(&parser,
					(YamlHandler[]){
						{"fullscreen", yaml_set_bool, fullscreen},
						{"maximised", yaml_set_bool, maximised},
						{NULL}
					},
					NULL, NULL, NULL
				);
			}
		}

		yaml_event_t event;
		if (find_sequence(&parser, &event, "size")) {
			config_load_point(&parser, NULL, &size);
		}
		yaml_event_delete(&event);

		fclose(fp);
	}

	return size;
}


/*
 *  On first run, settings are loaded from /usr/share/imgview or cwd
 */
bool
load_settings (bool safe_only)
{
	PF;

	if (!agl_actor_registry) {
		agl_actor_registry = g_hash_table_new(g_str_hash, g_str_equal);
		agl_actor_register_class("Dock H", dock_h_get_class());
		agl_actor_register_class("Dock V", dock_v_get_class());
		agl_actor_register_class("Panel", panel_view_get_class());
		agl_actor_register_class("Scrollable", scrollable_view_get_class());
		agl_actor_register_class("Dirs", directories_view_get_class());
		agl_actor_register_class("Tabs", tabs_view_get_class());
		agl_actor_register_class("Scrollbar", scrollbar_view_get_class());
		agl_actor_register_class("Files", files_view_get_class());
		agl_actor_register_class("Img", img_view_get_class());
		agl_actor_register_class("Statusbar", statusbar_get_class());
	}

	g_autoptr(FILE) fp = open_settings_file(safe_only);


	char* path = NULL;

	bool ok = fp && yaml_load (fp,
		(YamlHandler[]){
			{"path", yaml_set_string, &path},
			{NULL}
		},
		(YamlMappingHandler[]){
			{"windows", config_load_windows,},
			{NULL}
		}
	);

	if (!app->config.dir)
		app->config.dir = path;
	else
		g_free(path);

	return ok;
}


static bool
with_fp (const char* filename, bool (*fn)(FILE*, gpointer), gpointer user_data)
{
	FILE* fp = fopen(filename, "wb");
	if (!fp) {
		pwarn("cannot open config file for writing (%s).", filename);
		return false;
	}

	bool ok = fn(fp, user_data);

	fclose(fp);

	return ok;
}


static bool
_save_settings (FILE* fp, gpointer _)
{
	if(!yaml_emitter_initialize(&emitter)){ gerr("failed to initialise yaml writer."); return false; }

	yaml_emitter_set_output_file(&emitter, fp);
	yaml_emitter_set_canonical(&emitter, false);

	yaml_event_t event;
	EMIT(yaml_stream_start_event_initialize(&event, YAML_UTF8_ENCODING));
	EMIT(yaml_document_start_event_initialize(&event, NULL, NULL, NULL, 0));
	EMIT(yaml_mapping_start_event_initialize(&event, NULL, (guchar*)"tag:yaml.org,2002:map", 1, YAML_BLOCK_MAPPING_STYLE));

	if(app->config.dir){
		char value[FILENAME_MAX];
		g_strlcpy(value, app->config.dir, FILENAME_MAX);
		if(!yaml_add_key_value_pair("path", value)) goto error;
	}

	bool add_child (yaml_event_t* event, AGlActor* actor)
	{
		AGlActorClass* c = actor->class;
		g_return_val_if_fail(c, false);
		if (c != scrollbar_view_get_class()) {
			g_return_val_if_fail(actor->name, false);

			map_open(event, actor->name);

			if (!yaml_add_key_value_pair("type", c->name)) goto error;

			if(actor == (AGlActor*)app->scene){
				bool fullscreen = agl_is_fullscreen(app->scene->gl.glx.window);
				if(fullscreen) if(!yaml_add_key_value_pair("fullscreen", "true")) goto error;

				int v, h;
				agl_is_maximised (app->scene->gl.glx.window, &h, &v);
				if(h && v) if(!yaml_add_key_value_pair("maximised", "true")) goto error;
			}

			bool is_panel_child = actor->parent && actor->parent->class == panel_view_get_class();

			if (!is_panel_child) {
				int vals1[2] = {actor->region.x1, actor->region.y1};
				if (vals1[0] || vals1[1]) {
					yaml_add_key_value_pair_array("position", vals1, 2);
				}
				int vals2[2] = {agl_actor__width(actor), agl_actor__height(actor)};
				yaml_add_key_value_pair_array("size", vals2, 2);
			}

			if (actor->class == panel_view_get_class() || actor->class == dock_v_get_class() || actor->class == dock_h_get_class()) {
				PanelView* panel = (PanelView*)actor;
				bool b[3] = {
					panel->size_req.min.x > -1 || panel->size_req.min.y > -1,
					panel->size_req.preferred.x > -1 || panel->size_req.preferred.y > -1,
					panel->size_req.max.x > -1 || panel->size_req.max.y > -1
				};
				if (b[0] || b[1] || b[2]) {
					map_open(event, "size-req");
					if (b[0])
						if(!yaml_add_key_value_pair_pt("min", &panel->size_req.min)) goto error;
					if (b[1])
						if(!yaml_add_key_value_pair_pt("preferred", &panel->size_req.preferred)) goto error;
					if (b[2])
						if(!yaml_add_key_value_pair_pt("max", &panel->size_req.max)) goto error;
					end_map(event);
				}
			}

			StateBehaviour* b = (StateBehaviour*)agl_actor__find_behaviour (actor, state_get_class());
			if (b) {
				ParamArray* params = b->params;
				if (params) {
					for (int i = 0; i < params->size; i++) {
						ConfigParam* param = &params->params[i];
						switch (param->utype) {
							case G_TYPE_STRING:
								if(!yaml_add_key_value_pair(param->name, param->val.c)) goto error;
								break;
							default:
								break;
						}
					}
				}
			}

			if (!b || b->is_container) {
				for (GList* l = actor->children;l;l=l->next) {
					if (!add_child(event, l->data)) goto error;
				}
			}

			end_map(event);
		}
		return true;
	  error:
		return false;
	}

	map_open(&event, "windows");

	add_child(&event, (AGlActor*)app->scene);
	end_map(&event);

	EMIT(yaml_mapping_end_event_initialize(&event));

	end_document;
	yaml_event_delete(&event);
	yaml_emitter_delete(&emitter);
	dbg(1, "yaml write finished ok.");

	return true;

  error:
	switch (emitter.error){
		case YAML_MEMORY_ERROR:
			fprintf(stderr, "Memory error: Not enough memory for emitting\n");
			break;
		case YAML_WRITER_ERROR:
			fprintf(stderr, "Writer error: %s\n", emitter.problem);
			break;
		case YAML_EMITTER_ERROR:
			fprintf(stderr, "yaml emitter error: %s\n", emitter.problem);
			break;
		default:
			fprintf(stderr, "Internal error\n");
			break;
	}
	yaml_event_delete(&event);
	yaml_emitter_delete(&emitter);

	return false;
}


static bool
ensure_config_dir ()
{
	static char* path = NULL;
	if (!path) path = g_strdup_printf("%s/.config/" PACKAGE, g_get_home_dir()); // path is static - don't free.

	return (!g_mkdir_with_parents(path, 488));
}


bool
save_settings ()
{
	PF;

	char* tmp = g_strdup_printf("%s.tmp.yaml", app->config_ctx.filename);

	bool ok = with_fp(tmp, _save_settings, NULL);

	if (ok) {
		ensure_config_dir();

		char* filename = g_strdup_printf("%s.yaml", app->config_ctx.filename);
		if (g_rename (tmp, filename)) {
			pwarn("failed to replace config (%s): %s", strerror(errno), filename);
			ok = false;
		}
		g_free(filename);
	}
	g_free(tmp);

	return ok;
}


