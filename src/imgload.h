/**
* +----------------------------------------------------------------------+
* | This file is part of Imgview.                                        |
* | copyright (C) 2018-2023 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#pragma once

#include "file_manager/diritem.h"
#include "file_manager/mimetype.h"
#include "tree/aglview.h"
#include "views/files.h"
#include "texture_cache/texture_cache.h"

typedef guint TextureId;

typedef struct {
    char*           path;
    DirectoryView*  dv;   // used to get path if path not set
    DirItem*        item;
    MIME_type*      mime_type;
    unsigned char*  data;
    AGliSize        size;
    Texture*        texture;
    gpointer        user_data;
} ImgLoadItem;

void      load_img_a (ImgLoadItem*);
Texture*  load_img_b (ImgLoadItem*);
