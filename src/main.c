/*
 +----------------------------------------------------------------------+
 | This file is part of Imgview.                                        |
 | copyright (C) 2018-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __main_c__

#include "config.h"
#include <libgen.h>
#include <getopt.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <debug/debug.h>
#include "agl/actor.h"
#include "agl/event.h"
#include "agl/ext.h"
#include "agl/behaviours/key.h"
#include "worker/worker.h"
#include "file_manager/mimetype.h"
#include "file_manager/pixmaps.h"
#include "file_manager/diritem.h"
#include "file_manager/file_deleter.h"
#include "icon/utils.h"
#include "tree/aglview.h"
#include "behaviours/style.h"
#include "views/files.h"
#include "views/img.h"
#include "views/statusbar.h"
#include "views/delete_dialogue.h"
#include "application.h"
#include "glx.h"
#include "keys.h"
#include "layout.h"
#include "imgload.h"

Application* app = NULL;

Worker worker = {0,};

static struct Actors {AGlActor *bg, *hdock, *files, *img, *debug, *statusbar, *statusbar2; } actors;

static ActorKeyHandler on_key_f, on_key_s, on_key_speed_down, on_key_speed_up, on_key_next, on_key_prev;

static ActorKey keys[] = {
	{XK_f,         on_key_f},
	{XK_v,         on_key_f},
	{XK_s,         on_key_s},
	{XK_less,      on_key_speed_down},
	{XK_comma,     on_key_speed_down},
	{XK_greater,   on_key_speed_up},
	{XK_period,    on_key_speed_up},
	{XK_Page_Up,   on_key_prev},
	{XK_Page_Down, on_key_next},
	{0,}
};

static void     add_content         ();
static void     scene_layout        (AGlActor*);
static void     on_actor_added      (Application*, AGlActor*, gpointer);
static void     files__on_selection (AyyiTreeSelection*, gpointer);
static void     on_cursor_change    (Observable*, int row, gpointer);
static void     on_delete_request   (AGlTreeView*, AMPromise*);
static void     on_fullscreen       (bool);
static void     remove_left         (bool);
static void     select_next         ();

static const struct option long_options[] = {
  { "dir",              1, NULL, 'd' },
  { "verbose",          1, NULL, 'v' },
  { "help",             0, NULL, 'h' },
  { "version",          0, NULL, 'V' },
};

static const char* const short_options = "v:d:hV";

static const char* const usage =
	"Usage: %s [OPTIONS]\n\n"
	PACKAGE_NAME" is an image viewer for quickly browsing large numbers of images.\n"
	"\n"
	"Options:\n"
	"  -d, --dir  show the contents of this directory (normally dir is taken from config file)\n"
	"  -v, --verbose  set logging verbosity\n";


int
main (int argc, char* argv[])
{
	_debug_ = 0;

	g_log_set_default_handler(log_handler, NULL);

	app = application_new();
	worker_init(&worker);
	type_init();
	pixmaps_init();
#if 0
	icon_theme_init();
#endif
	const char* theme = find_icon_theme();
	if(theme)
		set_icon_theme(theme);

	int opt;
	while ((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch(opt) {
			case 'd':
				app->config.dir = optarg;
				break;
			case 'v':
				_debug_ = 1;
				break;
			case 'h':
				printf(usage, basename(argv[0]));
				exit(EXIT_SUCCESS);
				break;
			case '?':
			default:
				printf(usage, basename(argv[0]));
				exit(EXIT_FAILURE);
		}
	}

	if (!(dpy = XOpenDisplay(NULL))) {
		printf("Error: couldn't open display %s\n", XDisplayName(NULL));
		return -1;
	}

	agl_scene_get_class()->behaviour_classes[0] = style_get_class();

	bool fullscreen = false;
	bool maximised = false;
	AGliPt size = get_window_size_from_settings(&fullscreen, &maximised);
	int screen = DefaultScreen(dpy);
	AGlWindowFlags flags = (fullscreen * AGL_FULLSCREEN) | (maximised * AGL_MAXIMISED);
	AGlWindow* window = agl_window("ImageView", (XDisplayWidth(dpy, screen) - size.x) / 2, (XDisplayHeight(dpy, screen) - size.y) / 2, size.x, size.y, flags);
	app->scene = window->scene;

	XClassHint* xclass = XAllocClassHint();
	*xclass = (XClassHint){
		.res_class = "imgview",
	};
	XSetClassHint(dpy, window->window, xclass);

	agl_window_set_icons(window->window,
		g_list_prepend(g_list_prepend(NULL,
			gdk_pixbuf_new_from_resource("/imgview/imgview-96.png", NULL)),
			gdk_pixbuf_new_from_resource("/imgview/imgview-16.png", NULL)
		)
	);

	#define KEYS(A) ((KeyBehaviour*)((AGlActor*)A)->behaviours[1])
	((AGlActor*)app->scene)->behaviours[1] = key_behaviour();
	KEYS(app->scene)->keys = &keys;
	agl_behaviour_init((AGlBehaviour*)KEYS(app->scene), (AGlActor*)app->scene);

	bool scene_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
	{
		switch (event->type) {
			case AGL_KEY_PRESS:
				if (agl_is_fullscreen(app->scene->gl.glx.window)) {
					;AGlEventKey* e = (AGlEventKey*)event;
					int keyval = e->keyval;
					if (keyval == 32 || keyval == 65293) { // Spacebar, Return
						select_next();
					}
				}

				// allow non-focussed nodes to handle key events

				;AGlBehaviourClass* K = key_get_class();
				AGlBehaviour* keys = agl_actor__find_behaviour(actors.img, K);
				if (keys) {
					if(K->event(keys, actors.img, event)) return AGL_HANDLED;
				}

				return actors.files->on_event(actors.files, event, xy);
			default:
				break;
		}
		return AGL_NOT_HANDLED;
	}

	((AGlActor*)app->scene)->on_event = scene_event;

	g_signal_connect(app, "actor-added", G_CALLBACK(on_actor_added), NULL);

	if (load_settings(false) || load_settings(true)) {
		dbg(1, "window setting loaded ok");
		add_content();
	}

	g_main_loop_run(agl_main_loop_new());

	if (agl_is_fullscreen(window->window)) {
		remove_left(false);
	}
	save_settings();

#ifdef WITH_VALGRIND
	glXMakeCurrent(dpy, 0, window->scene->gl.glx.context);
#endif

	agl_window_destroy(&window);
	XCloseDisplay(dpy);
	XFree(xclass);

	application_quit(app);

#ifdef WITH_VALGRIND
	if(worker.jobs){
		pwarn("jobs running. this may cause memory leaks to be reported");
		worker_cancel_jobs(&worker, (GObject*)app);
	}
#endif

	return 0;
}


static void
add_content ()
{
	if (!(actors.statusbar = agl_actor__find_by_name((AGlActor*)app->scene, "Statusbar"))) {
		if (_debug_) pwarn("config did not contain statusbar");
		actors.statusbar = statusbar(NULL);
		agl_actor__add_child((AGlActor*)app->scene, actors.statusbar);
	}
	actors.statusbar->region = (AGlfRegion){0}; // ignore size from config

	if (!(actors.statusbar2 = agl_actor__find_by_name((AGlActor*)app->scene, "Statusbar2"))) {
		agl_actor__add_child((AGlActor*)app->scene, actors.statusbar2 = statusbar(NULL));
	}

	if (actors.files && actors.img) {
		files_view_set_directory((FilesView*)actors.files, app->config.dir);
		g_signal_connect(((AGlTreeView*)actors.files)->selection, "changed", (GCallback)files__on_selection, NULL);

		void on_img_render (AGlObservable* _, AGlVal value, gpointer _files)
		{
			((AGlTreeView*)actors.files)->lock_selection = false;
		}
		agl_observable_subscribe(((ImgView*)actors.img)->rendered, on_img_render, NULL);
	}

	if (agl_is_fullscreen(app->scene->gl.glx.window)) {
		on_fullscreen(true);
	}

#ifdef SHOW_FBO_DEBUG
	agl_actor__add_child((AGlActor*)scene, actors.debug = wf_debug_actor(NULL));
	wf_debug_actor_set_actor((DebugActor*)actors.debug, actors.list);
#endif

	void on_delete_count (Observable* o, int value, gpointer _)
	{
		statusbar_set((Statusbar*)actors.statusbar2, value ? "%i" : "", value);
	}
	observable_subscribe (file_deleter(), on_delete_count, NULL);

	((AGlActor*)app->scene)->set_size = scene_layout;

#ifdef DEBUG
	if(_debug_ > 2) agl_actor__print_tree((AGlActor*)app->scene);
#endif
}


static void
on_actor_added (Application* app, AGlActor* actor, gpointer data)
{
	if (!strcmp(actor->name, "Files")) {
		actors.files = actor;
		dbg(1, "path=%s", app->config.dir);

		observable_subscribe(((AGlTreeView*)actors.files)->cursor, on_cursor_change, NULL);

		app->scene->selected = actors.files;

		((AGlTreeView*)actors.files)->on_delete_request = on_delete_request;
	}

	else if (actor->class == img_view_get_class()) {
		actors.img = actor;
	}

	else if (!strcmp(actor->name, "Dock H")) {
		actors.hdock = actor;
	}
}


static void
scene_layout (AGlActor* scene)
{
	g_return_if_fail(actors.statusbar);

	dbg(2, "%.0f x %.0f", scene->region.x2, agl_actor__height(scene));

	float width = agl_actor__width(scene);
	float height = agl_actor__height(scene);
	float statusbar_height = actors.statusbar->region.y2 > -1. ? 20. : 0;

	if(actors.statusbar->region.y2 > -1.){
		actors.hdock->region = (AGlfRegion){20., 20., width - 20., height - 10. - statusbar_height};
		actors.statusbar->region = (AGlfRegion){20., height - STATUSBAR_HEIGHT - 5., width - 120., height - 5.};
		actors.statusbar2->region = (AGlfRegion){width - 100. - 20., height - STATUSBAR_HEIGHT - 5., width - 20., height - 5.};
	}else{
		// fullscreen layout
		actors.hdock->region = (AGlfRegion){0., 0., width, height};
	}

#ifdef SHOW_FBO_DEBUG
	actors.debug->region = (AGlfRegion){scene->region.x2 / 2., 10., scene->region.x2 - 10., scene->region.x2 / 2.};
#endif

	agl_actor__invalidate((AGlActor*)app->scene);
}


#define PRECACHE_SIZE 20

static uint32_t precache_idle = 0;
#if 0
static int precache_count = 0;

static bool
precache (gpointer index)
{
	DirectoryView* dv = ((FilesView*)actors.files)->view;

	int i = GPOINTER_TO_INT(index) + precache_count + 1;
	if(i < dv->model->items->len){
		DirItem* item = ((ViewItem*)dv->model->items->pdata[i])->item;
		if(item && item->mime_type){
			char path[FILENAME_MAX] = {0,};
			snprintf(path, FILENAME_MAX-1, "%s/%s", vm_directory_get_path(dv->model), item->leafname);
			img_view_load_img((ImgView*)actors.img, path, item->mime_type, item);
		}
	}

	if(precache_count++ < PRECACHE_SIZE){
		return G_SOURCE_CONTINUE;
	}

	precache_timeout = 0;
	precache_count = 0;
	return G_SOURCE_REMOVE;
}
#endif


static gboolean
precache_img (gpointer index)
{
	DirectoryView* dv = ((FilesView*)actors.files)->view;
	int cursor = ((AGlTreeView*)actors.files)->cursor->value;

	// runs in worker thread
	void work (GObject* _, gpointer _c)
	{
		ImgLoadItem* witem = _c;

		// this will add to the texture_cache and copy to GL texture
		load_img_a(witem);
	}

	void work_done (GObject* _, GError* error, gpointer _c)
	{
		load_img_b((ImgLoadItem*)_c);
	}

	void free_job (gpointer _item)
	{
		ImgLoadItem* item = _item;
		if (item) g_clear_pointer(&item->data, g_free);
	}

	void push_item (DirItem* item)
	{
		if (item && item->mime_type) {
			if (!strcmp(item->mime_type->media_type, "image")) {
				worker_push_job(&worker, (GObject*)app, work, work_done, free_job, AGL_NEW(ImgLoadItem,
					.dv = dv,
					.item = item,
					.mime_type = item->mime_type
				));
			}
		}
	}

	int n_items_to_queue = 1;
	if (g_list_length(worker.jobs) < 10) n_items_to_queue = 10;

	for (int n=0;n<n_items_to_queue;n++) {
		int i = cursor + n + 1;
		if (i < dv->model->items->len) {
			DirItem* item = ((ViewItem*)dv->model->items->pdata[i])->item;
			push_item(item);
		}
	}

	precache_idle = 0;
	return G_SOURCE_REMOVE;
}


static void
files__on_selection (AyyiTreeSelection* selection, gpointer actor)
{
	DirectoryView* dv = ((FilesView*)actors.files)->view;

	if (selection->type == AYYI_SELECTION_SINGLE) {
		AyyiTreeIter iter;
		if (ayyi_tree_selection_get_selected (selection, NULL, &iter)) {
			dbg(1, "  selected=%i", GPOINTER_TO_INT(iter.user_data));
			DirItem* item = ((ViewItem*)dv->model->items->pdata[GPOINTER_TO_INT(iter.user_data)])->item;
			if (item && item->mime_type) {
				char path[FILENAME_MAX] = {0,};
				snprintf(path, FILENAME_MAX-1, "%s/%s", vm_directory_get_path(dv->model), item->leafname);
				img_view_show_img((ImgView*)actors.img, path, item->mime_type, item);
			}

			//if(!precache_timeout) precache_timeout = g_idle_add(precache, iter.user_data);
			g_idle_add(precache_img, iter.user_data);
		}
	} else {
#if 0
			// Using foreach instead, as below, is perhaps more efficient, dont want to allocate big list
			AyyiTreeModel* model;
			GList* selected = ayyi_tree_selection_get_selected_rows(selection, &model);
			if(selected){
				AyyiTreePath* path = g_list_last(selected)->data;
				AyyiTreeIter iter;
				if(ayyi_tree_model_get_iter(model, &iter, path)){
					show_iter(&iter);
				}
				g_list_foreach (selected, (GFunc)ayyi_tree_path_free, NULL);
				g_list_free (selected);
			}
#ifdef DEBUG
			else pwarn("failed to get selection");
#endif
#else
			#if 0 // following cursor change instead now
			void foreach (AyyiTreeModel* model, AyyiTreePath* path, AyyiTreeIter* iter, gpointer out)
			{
				AyyiTreeIter* i = out;
				*i = *iter;
			}

			AyyiTreeIter iter = {0,};
			ayyi_tree_selection_selected_foreach(selection, foreach, &iter);
			show_iter(&iter);
			#endif
#endif
	}

	if (actors.statusbar) {
		AGlTreeView* view = selection->tree_view;
		int n = ayyi_tree_model_iter_n_children(agl_tree_view_get_model(view), NULL);
		int n2 = ayyi_tree_selection_count_selected_rows(selection);
		statusbar_set((Statusbar*)actors.statusbar, "%i files. %i selected", n, n2);
	}
}


static void
on_cursor_change (Observable* o, int row, gpointer _)
{
	static DirItem* latest_item;

	void got_item (VMDirectory* vd, DirItem* item, gpointer data)
	{
		if (item == latest_item) {
			char path[FILENAME_MAX] = {0,};
			snprintf(path, FILENAME_MAX-1, "%s/%s", vm_directory_get_path(vd), item->leafname);
			img_view_show_img((ImgView*)actors.img, path, item->mime_type, item);
		}
	}

	void show_iter (AyyiTreeIter* iter)
	{
		DirectoryView* dv = ((FilesView*)actors.files)->view;
		DirItem* item = ((ViewItem*)dv->model->items->pdata[GPOINTER_TO_INT(iter->user_data)])->item;
		if (item) {
			latest_item = item;

			if (item->mime_type) {
				got_item(dv->model, item, NULL);
			} else {
				if (item->flags & ITEM_FLAG_NEED_RESCAN_QUEUE) {
					vm_directory_fetch_item (dv->model, item, got_item, NULL);
				} else {
					vm_directory_wait_item (dv->model, item, got_item, NULL);
				}
			}

			((AGlTreeView*)actors.files)->lock_selection = true;
		}
#ifdef DEBUG
		else pwarn("model data not found: row=%i %p flag=%i", GPOINTER_TO_INT(iter->user_data), item, item->flags & ITEM_FLAG_NEED_RESCAN_QUEUE);
#endif

#if 0
		if(!precache_timeout) precache_timeout = g_idle_add(precache, iter->user_data);
#else
		if (!precache_idle) precache_idle = g_idle_add(precache_img, iter->user_data);
#endif
	}

	dbg(1, "%i", row);
	AyyiTreeIter iter = {.user_data = GINT_TO_POINTER(row)};
	show_iter(&iter);
}


static void
show_statusbar ()
{
	actors.statusbar->region.y2 = actors.statusbar->region.y1 + 20.;
	actors.statusbar2->region.y2 = actors.statusbar2->region.y1 + 20.;

	agl_actor__invalidate(actors.statusbar);
	agl_actor__invalidate(actors.statusbar2);
}


static void
hide_statusbar ()
{
	actors.statusbar->region.y2 = -1.;
	actors.statusbar2->region.y2 = -1.;

	agl_actor__invalidate(actors.statusbar);
	agl_actor__invalidate(actors.statusbar2);
}


static void
remove_left (bool remove)
{
	static AGlActor* left = NULL;

	if (remove) {
		g_return_if_fail(!left);
		left = actors.hdock->children->data;
		g_return_if_fail(left && !strcmp(left->name, "Left"));
		actors.hdock->children = g_list_remove(actors.hdock->children, left);

		hide_statusbar();
	} else {
		if (left) {
			actors.hdock->children = g_list_prepend(actors.hdock->children, left);
			left = NULL;
		}

		show_statusbar();
	}
}


static void
on_fullscreen (bool fs)
{
	if (fs) {
		remove_left(true);
		agl_actor__set_size(actors.hdock);

		hide_statusbar();

		// TODO save this property to yaml
		((ImgView*)actors.img)->centered = true;

		app->scene->selected = actors.img;

		hide_cursor(app->scene->gl.glx.window);
	} else {
		remove_left(false);
		agl_actor__set_size(actors.hdock);

		show_statusbar();

		((ImgView*)actors.img)->centered = false;

		show_cursor(app->scene->gl.glx.window);
	}
}


static bool
on_key_f (AGlActor* actor, AGlModifierType modifier)
{
	bool fs = !agl_is_fullscreen(actor->root->gl.glx.window);

	agl_toggle_fullscreen(actor->root->gl.glx.window);

	on_fullscreen(fs);

	return AGL_HANDLED;
}


static void
select_next ()
{
	AGlTreeView* tree = (AGlTreeView*)actors.files;
	AyyiTreeModel* model = agl_tree_view_get_model(tree);

	AyyiTreeIter iter;
	AyyiTreePath* cursor;
	agl_tree_view_get_cursor (tree, &cursor, NULL);
	if(!cursor) cursor = ayyi_tree_path_new_first();

	ayyi_tree_path_next(cursor);
	if (ayyi_tree_model_get_iter(model, &iter, cursor)) {
		agl_tree_view_set_cursor(tree, cursor, NULL);
		ayyi_tree_selection_select_path(tree->selection, cursor);
	}

	ayyi_tree_path_free(cursor);
}


static void
select_prev ()
{
	AGlTreeView* tree = (AGlTreeView*)actors.files;

	AyyiTreePath* cursor;
	agl_tree_view_get_cursor (tree, &cursor, NULL);
	if (cursor) {
		ayyi_tree_path_prev(cursor);
		agl_tree_view_set_cursor(tree, cursor, NULL);
		if(ayyi_tree_selection_path_is_selected (tree->selection, cursor)){
			ayyi_tree_path_next(cursor);
			ayyi_tree_selection_unselect_path(tree->selection, cursor);
		}else{
			ayyi_tree_selection_select_path(tree->selection, cursor);
		}

		ayyi_tree_path_free(cursor);
	}
}


static struct {
	int   speed;
	struct {
		int   speed;
		guint id;
	}     timer;
	int   iter;
}
slideshow = {5000,};

static bool
on_key_s (AGlActor* actor, AGlModifierType modifier)
{
	PF;

	auto void start ();

	gboolean next (gpointer _)
	{
		PF;

		if(slideshow.timer.speed != slideshow.speed){
			start();
			return G_SOURCE_REMOVE;
		}

		if((++slideshow.iter) % 5)
			return G_SOURCE_CONTINUE;

		select_next();

		return G_SOURCE_CONTINUE;
	}

	void start ()
	{
		// the timeout is shorter than the slide time in order to handle speed changes
		slideshow.timer.id = g_timeout_add(slideshow.speed / 5, next, NULL);
		slideshow.timer.speed = slideshow.speed;
	}

	if(!slideshow.timer.id){
		start();
	}else{
		g_source_remove(slideshow.timer.id);
		slideshow.timer.id = 0;
	}

	return AGL_HANDLED;
}


static bool
on_key_speed_up (AGlActor* actor, AGlModifierType modifier)
{
	if(slideshow.speed > 10){
		int delta = slideshow.speed > 1000 ? 1000 : slideshow.speed > 100 ? 100 : 10;
		slideshow.speed -= delta;

		statusbar_set((Statusbar*)actors.statusbar2, "%i", slideshow.speed);
	}

	return AGL_HANDLED;
}


static bool
on_key_speed_down (AGlActor* actor, AGlModifierType modifier)
{
	if(slideshow.speed < 20000){
		int delta = slideshow.speed >= 1000 ? 1000 : slideshow.speed >= 100 ? 100 : 10;
		slideshow.speed += delta;

		statusbar_set((Statusbar*)actors.statusbar2, "%i", slideshow.speed);
	}

	return AGL_HANDLED;
}


static bool
on_key_next (AGlActor* actor, AGlModifierType modifier)
{
	if(agl_is_fullscreen(app->scene->gl.glx.window)){
		select_next();
		return AGL_HANDLED;
	}
	return AGL_NOT_HANDLED;
}


static bool
on_key_prev (AGlActor* actor, AGlModifierType modifier)
{
	if(agl_is_fullscreen(app->scene->gl.glx.window)){
		select_prev();
		return AGL_HANDLED;
	}
	return AGL_NOT_HANDLED;
}


static AGlWindow*
popup_window (AMPromise* promise)
{
	int screen = DefaultScreen(dpy);
	AGliPt size = {MAX(240, 10 * LINE_HEIGHT), 50 + 2 * LINE_HEIGHT};

	agl_scene_get_class()->behaviour_classes[0] = style_get_class();

	AGlWindow* window = agl_window("Confirm delete", (XDisplayWidth(dpy, screen) - size.x) / 2, (XDisplayHeight(dpy, screen) - size.y) / 2, size.x, size.y, AGL_FLAGS_NONE);

	g_free(((AGlActor*)window->scene)->name);
	((AGlActor*)window->scene)->name = g_strdup("Popup");
	agl_actor__add_child((AGlActor*)window->scene, window->scene->selected = delete_dialogue(promise));

	((DeleteDialogue*)window->scene->selected)->n_items = ayyi_tree_selection_count_selected_rows(((AGlTreeView*)actors.files)->selection);

	return window;
}


static AGlWindow* popup = NULL;

static gboolean
popup_destroy ()
{
	agl_window_destroy(&popup);

	return G_SOURCE_REMOVE;
}


static void
_on_delete_request_resolved (gpointer _view, gpointer _promise)
{
	g_idle_add(popup_destroy, NULL);
}


static void
on_delete_request (AGlTreeView* view, AMPromise* promise)
{
	PF;
	am_promise_add_callback(promise, _on_delete_request_resolved, promise);

	popup = popup_window(promise);
}
