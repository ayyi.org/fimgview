/**
* +----------------------------------------------------------------------+
* | This file is part of Imgview.                                        |
* | copyright (C) 2018-2023 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <stdio.h>
#include <GL/gl.h>
#include <jpeglib.h>
#include <setjmp.h>
#include "debug/debug.h"
#include "imgload.h"

struct my_error_mgr {
   struct jpeg_error_mgr pub; // "public" fields
   jmp_buf setjmp_buffer;     // for return to caller
};

typedef struct my_error_mgr* my_error_ptr;

static void buf_to_texture (TextureId, unsigned char* buf, int width, int height);
static void load_png_a     (ImgLoadItem*, const char*);
static void load_png_b     (ImgLoadItem*);
static void load_png_c     (ImgLoadItem*);


METHODDEF(void)
my_error_exit (j_common_ptr cinfo)
{
	// cinfo->err really points to a my_error_mgr struct, so coerce pointer
	my_error_ptr myerr = (my_error_ptr) cinfo->err;

	// Always display the message.
	// We could postpone this until after returning, if we chose.
	(*cinfo->err->output_message) (cinfo);

	// Return control to the setjmp point
	longjmp(myerr->setjmp_buffer, 1);
}


static void
load_jpg_a (ImgLoadItem* item, const char* path)
{
	g_return_if_fail(!item->texture);

	const int format = GL_RGB;

	FILE* fd;
	if (!(fd = fopen(path, "rb"))){
		dbg(0, "cannot open image");
		return;
	}

	struct jpeg_decompress_struct cinfo;
	struct my_error_mgr jerr;
	cinfo.err = jpeg_std_error ((struct jpeg_error_mgr*)&jerr);
	jerr.pub.error_exit = my_error_exit;
	// Establish the setjmp return context for my_error_exit to use.
	if (setjmp(jerr.setjmp_buffer)) {
		// The JPEG code has signaled an error.
		jpeg_destroy_decompress(&cinfo);
		fclose(fd);
		return;
	}

	jpeg_create_decompress (&cinfo);

	jpeg_stdio_src (&cinfo, fd);
	if (jpeg_read_header (&cinfo, true) != 1) {
		dbg(0, "cannot read jpg header: %s", path);
		return;
	}

	dbg(1, "size=%ix%i components=%i", cinfo.image_width, cinfo.image_height, cinfo.output_components);

	if (GL_RGB == format) {
		if (cinfo.out_color_space == JCS_GRAYSCALE)
			return;
	} else {
		if (cinfo.out_color_space != JCS_GRAYSCALE)
			return;
	}
	jpeg_start_decompress(&cinfo);

	int rowstride = cinfo.output_width * cinfo.output_components;
	if (rowstride % 4) rowstride += 4 - rowstride % 4;

	item->data = g_malloc(rowstride * cinfo.output_height);

	unsigned char* line;
	while (cinfo.output_scanline < cinfo.output_height) {
		line = item->data + rowstride * cinfo.output_scanline;
		jpeg_read_scanlines (&cinfo, &line, 1);
	}

	item->size = (AGliSize){cinfo.image_width, cinfo.image_height};

	jpeg_finish_decompress (&cinfo);
	jpeg_destroy_decompress (&cinfo);

	fclose(fd);
}


static void
load_jpg_b (ImgLoadItem* item)
{
	TextureId tid = item->texture->id;
	item->texture->size = item->size;

	buf_to_texture(tid, item->data, item->texture->size.w, item->texture->size.h);

	g_clear_pointer(&item->data, g_free);
}


void
load_img_a (ImgLoadItem* item)
{
	if (texture_cache_lookup_(item->item))
		return;

	char path[FILENAME_MAX] = {0,};
	if (item->path)
		g_strlcpy(path, item->path, FILENAME_MAX);
	else
		snprintf(path, FILENAME_MAX-1, "%s/%s", vm_directory_get_path(item->dv->model), item->item->leafname);

	if (!strcmp(item->mime_type->subtype, "jpeg")) {
		load_jpg_a(item, path);
	} else {
		load_png_a(item, path);
	}
}


Texture*
load_img_b (ImgLoadItem* item)
{
	if (!(item->texture = texture_cache_lookup_(item->item))) {
		item->texture = texture_cache_assign_new_(item->item);

		if (!strcmp(item->mime_type->subtype, "jpeg")) {
			load_jpg_b(item);
		} else {
			load_png_b(item);
		}
	}

	if (!strcmp(item->mime_type->subtype, "jpeg")) {
	} else {
		load_png_c(item);
	}

	return item->texture;
}


static void
buf_to_texture (TextureId tid, unsigned char* buf, int width, int height)
{
	agl_use_texture(tid);

	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, buf);
	gl_warn("texture bind");
}


static void
load_png_a (ImgLoadItem* item, const char* path)
{
	g_return_if_fail(path);

	GError* error = NULL;
	GdkPixbuf* pixbuf = gdk_pixbuf_new_from_file(path, &error);
	if (!pixbuf) {
		if (error) {
			g_warning("%s", error->message);
			g_error_free(error);
		} else {
			pwarn("pixbuf failed to load but not error was set");
		}
		return;
	}

	item->data = (void*)pixbuf;
	item->size = (AGliSize){gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf)};
}


static void
load_png_b (ImgLoadItem* item)
{
	g_return_if_fail(item->texture);

	if (item->data) {
		GdkPixbuf* pixbuf = (GdkPixbuf*)item->data;

		item->texture->size = item->size;
		buf_to_texture(item->texture->id, gdk_pixbuf_get_pixels(pixbuf), gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf));
	}
}


static void
load_png_c (ImgLoadItem* item)
{
	g_clear_pointer(&item->data, g_object_unref);
}
