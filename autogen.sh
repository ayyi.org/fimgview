#!/bin/sh

libtoolize --automake
aclocal
autoheader -Wall
automake --gnu --add-missing -Wall
autoconf

if [ ! -f lib/agl/autogen.sh ]; then
	${SETCOLOR_WARN}
	echo "libagl submodule not present"
	${SETCOLOR_NORMAL}
	if [ ! -d .git ]; then
		# building from tarball
		echo "fetching libagl ..."
		git --version || { echo "git not found"; exit; }
		url=`cat .gitmodules | grep url.*agl | sed -e "s/.*= \(.*\)/\1/"`
		dir=`pwd`
		cd lib/agl && git clone $url tmp || exit 1
		if [ ! -f tmp/autogen.sh ]; then
			${SETCOLOR_FAILURE}
			echo "failed to download libwaveform submodule"
			${SETCOLOR_NORMAL}
			exit 1
		fi
		mv tmp/* tmp/.git* . && rmdir tmp
		cd "$dir"
	else
		git submodule update --init --recursive
	fi
fi

cd lib/agl && \
	./autogen.sh

