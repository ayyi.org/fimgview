Fast Image Viewer
=================

[![Build status](https://gitlab.com/ayyi.org/fimgview/badges/master/pipeline.svg)](https://gitlab.com/ayyi.org/fimgview)

Fast Image Viewer is a minimalist image viewer originally
created to sort through large numbers of CCTV images.

![Screenshot](resources/screenshot.png "Screenshot")

### Requirements

 - X11
 - opengl >= 3.0
 - glib-2
 - graphene

### Build

```
./autogen.sh  # can be skipped if building from release tarball
./configure
make
```

### Usage

Keyboard shortcuts

 - f (or v) - toggle fullscreen
 - DEL - delete selected files
 - DOWN - next file
 - UP - previous file
 - SHIFT-DOWN - add next file to selection
 - SHIFT-UP - add previous file to selection
 - PAGE-DOWN - navigate down by 10 (or next image in fullscreen mode)
 - PAGE-UP - navigate up by 10 (or previous image in fullscreen mode)
 - RETURN / spacebar - next image in fullscreen mode
 - \+ - zoom in
 - \- - zoom out
 - s - start/stop slideshow
 - 1-10 - set step rate acceleration
 - ESC - exit
 - q - exit
