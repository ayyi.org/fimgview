/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib.h>
#include "agl/utils.h"
#include "observable.h"

typedef struct {
   ObservableFn fn;
   gpointer     user;
} Subscription;


Observable*
observable_new ()
{
	return g_new0(Observable, 1);
}


void
observable_free (Observable* observable)
{
	g_list_free_full(observable->subscriptions, g_free);
	g_free(observable);
}


void
observable_set (Observable* observable, int value)
{
	if(value == observable->value)
		return;

	if(observable->max){
		value = MIN(value, observable->max(observable->user_data));
	}

	observable->value = value;

	for(GList* l = observable->subscriptions; l; l = l->next){
		Subscription* subscription = l->data;
		subscription->fn(observable, value, subscription->user);
	}
}


void
observable_subscribe (Observable* observable, ObservableFn fn, gpointer user)
{
	observable->subscriptions = g_list_append(observable->subscriptions, AGL_NEW(Subscription,
		.fn = fn,
		.user = user
	));
}


/*
 *  Calls back imediately with the current value
 */
void
observable_subscribe_with_state (Observable* observable, ObservableFn fn, gpointer user)
{
	observable_subscribe(observable, fn, user);
	fn(observable, observable->value, user);
}
