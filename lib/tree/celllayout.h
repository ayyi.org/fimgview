/* gtkcelllayout.h
 * Copyright (C) 2003  Kristian Rietveld  <kris@gtk.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __ayyi_cell_layout_h__
#define __ayyi_cell_layout_h__

#include "tree/cellrenderer.h"
#include "tree/viewcolumn.h"

G_BEGIN_DECLS

#define AYYI_TYPE_CELL_LAYOUT           (ayyi_cell_layout_get_type ())
#define AYYI_CELL_LAYOUT(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_CELL_LAYOUT, AyyiCellLayout))
#define AYYI_IS_CELL_LAYOUT(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_CELL_LAYOUT))
#define AYYI_CELL_LAYOUT_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), AYYI_TYPE_CELL_LAYOUT, AyyiCellLayoutIface))

typedef struct _AyyiCellLayout           AyyiCellLayout; /* dummy typedef */
typedef struct _AyyiCellLayoutIface      AyyiCellLayoutIface;

/* keep in sync with GtkTreeCellDataFunc */
typedef void (* AyyiCellLayoutDataFunc) (AyyiCellLayout*, AyyiCellRenderer*, AyyiTreeModel*, AyyiTreeIter*, gpointer data);

struct _AyyiCellLayoutIface
{
   GTypeInterface g_iface;

   /* Virtual Table */
   void   (*pack_start)         (AyyiCellLayout*, AyyiCellRenderer*, gboolean expand);
   void   (*pack_end)           (AyyiCellLayout*, AyyiCellRenderer*, gboolean expand);
   void   (*clear)              (AyyiCellLayout*);
   void   (*add_attribute)      (AyyiCellLayout*, AyyiCellRenderer*, const gchar*attribute, gint column);
   void   (*set_cell_data_func) (AyyiCellLayout*, AyyiCellRenderer*, AyyiCellLayoutDataFunc, gpointer func_data, GDestroyNotify);
   void   (*clear_attributes)   (AyyiCellLayout*, AyyiCellRenderer*);
   void   (*reorder)            (AyyiCellLayout*, AyyiCellRenderer*, gint position);
   GList* (*get_cells)          (AyyiCellLayout*);
};

GType ayyi_cell_layout_get_type           (void) G_GNUC_CONST;
void  ayyi_cell_layout_pack_start         (AyyiCellLayout*, AyyiCellRenderer*, gboolean expand);
#if 0
void  ayyi_cell_layout_pack_end           (AyyiCellLayout*, AyyiCellRenderer*, gboolean expand);
GList *ayyi_cell_layout_get_cells         (AyyiCellLayout*);
void  ayyi_cell_layout_clear              (AyyiCellLayout*);
void  ayyi_cell_layout_set_attributes     (AyyiCellLayout*, AyyiCellRenderer*, ...) G_GNUC_NULL_TERMINATED;
void  ayyi_cell_layout_add_attribute      (AyyiCellLayout*, AyyiCellRenderer*, const gchar* attribute, gint column);
void  ayyi_cell_layout_set_cell_data_func (AyyiCellLayout*, AyyiCellRenderer*, AyyiCellLayoutDataFunc func, gpointer, GDestroyNotify);
void  ayyi_cell_layout_clear_attributes   (AyyiCellLayout*, AyyiCellRenderer*);
void  ayyi_cell_layout_reorder            (AyyiCellLayout*, AyyiCellRenderer*, gint position);
#endif

G_END_DECLS

#endif
