/* gtktreeviewcolumn.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __AYYI_TREE_VIEW_COLUMN_H__
#define __AYYI_TREE_VIEW_COLUMN_H__

#include "agl/actor.h"
#include "tree/cellrenderer.h"
#include "tree/model.h"

/* Not needed, retained for compatibility -Yosh */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkobject.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"


G_BEGIN_DECLS


#define AYYI_TYPE_TREE_VIEW_COLUMN            (ayyi_tree_view_column_get_type ())
#define AYYI_TREE_VIEW_COLUMN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_TREE_VIEW_COLUMN, AyyiTreeViewColumn))
#define AYYI_TREE_VIEW_COLUMN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_TREE_VIEW_COLUMN, AyyiTreeViewColumnClass))
#define AYYI_IS_TREE_VIEW_COLUMN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_TREE_VIEW_COLUMN))
#define AYYI_IS_TREE_VIEW_COLUMN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_TREE_VIEW_COLUMN))
#define AYYI_TREE_VIEW_COLUMN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_TREE_VIEW_COLUMN, AyyiTreeViewColumnClass))

typedef enum
{
  AYYI_TREE_VIEW_COLUMN_GROW_ONLY,
  AYYI_TREE_VIEW_COLUMN_AUTOSIZE,
  AYYI_TREE_VIEW_COLUMN_FIXED
} AyyiTreeViewColumnSizing;

typedef struct _AyyiTreeViewColumn      AyyiTreeViewColumn;
typedef struct _AyyiTreeViewColumnClass AyyiTreeViewColumnClass;

typedef void (* AyyiTreeCellDataFunc) (AyyiTreeViewColumn*, AyyiCellRenderer*, AyyiTreeModel*, AyyiTreeIter*, gpointer);


struct _AyyiTreeViewColumn
{
  GtkObject parent;

  AGlActor *GSEAL (tree_view);
  AGlActor *GSEAL (button);
  AGlActor *GSEAL (child);
  AGlActor *GSEAL (arrow);
  AGlActor *GSEAL (alignment);
  GdkWindow *GSEAL (window);
#if 0
  GtkCellEditable *GSEAL (editable_widget);
#endif
  gfloat GSEAL (xalign);
  guint GSEAL (property_changed_signal);
  gint GSEAL (spacing);

  /* Sizing fields */
  /* see gtk+/doc/tree-column-sizing.txt for more information on them */
  AyyiTreeViewColumnSizing GSEAL (column_type);
  gint GSEAL (requested_width);
  gint GSEAL (button_request);
  gint GSEAL (resized_width);
  gint GSEAL (width);
  gint GSEAL (fixed_width);
  gint GSEAL (min_width);
  gint GSEAL (max_width);

  /* dragging columns */
  gint GSEAL (drag_x);
  gint GSEAL (drag_y);

  gchar *GSEAL (title);
  GList *GSEAL (cell_list);

  /* Sorting */
  guint GSEAL (sort_clicked_signal);
  guint GSEAL (sort_column_changed_signal);
  gint GSEAL (sort_column_id);
  GtkSortType GSEAL (sort_order);

  /* Flags */
  guint GSEAL (visible)             : 1;
  guint GSEAL (resizable)           : 1;
  guint GSEAL (clickable)           : 1;
  guint GSEAL (dirty)               : 1;
  guint GSEAL (show_sort_indicator) : 1;
  guint GSEAL (maybe_reordered)     : 1;
  guint GSEAL (reorderable)         : 1;
  guint GSEAL (use_resized_width)   : 1;
  guint GSEAL (expand)              : 1;
};

struct _AyyiTreeViewColumnClass
{
  GtkObjectClass parent_class;

  void (*clicked) (AyyiTreeViewColumn*);
};

GType                    ayyi_tree_view_column_get_type            (void) G_GNUC_CONST;
AyyiTreeViewColumn*      ayyi_tree_view_column_new                 (void);
AyyiTreeViewColumn*      ayyi_tree_view_column_new_with_attributes (const gchar* title, AyyiCellRenderer*, ...) G_GNUC_NULL_TERMINATED;
void                     ayyi_tree_view_column_pack_start          (AyyiTreeViewColumn*, AyyiCellRenderer*, gboolean expand);
void                     ayyi_tree_view_column_pack_end            (AyyiTreeViewColumn*, AyyiCellRenderer*, gboolean expand);
void                     ayyi_tree_view_column_clear               (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_add_attribute       (AyyiTreeViewColumn*, AyyiCellRenderer*, const gchar* attribute, gint column);
void                     ayyi_tree_view_column_set_attributes      (AyyiTreeViewColumn*, AyyiCellRenderer*, ...) G_GNUC_NULL_TERMINATED;
void                     ayyi_tree_view_column_set_cell_data_func  (AyyiTreeViewColumn*, AyyiCellRenderer*, AyyiTreeCellDataFunc, gpointer func_data, GDestroyNotify);
void                     ayyi_tree_view_column_clear_attributes    (AyyiTreeViewColumn*, AyyiCellRenderer*);
void                     ayyi_tree_view_column_set_spacing         (AyyiTreeViewColumn*, gint spacing);
gint                     ayyi_tree_view_column_get_spacing         (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_visible         (AyyiTreeViewColumn*, gboolean visible);
gboolean                 ayyi_tree_view_column_get_visible         (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_resizable       (AyyiTreeViewColumn*, gboolean resizable);
gboolean                 ayyi_tree_view_column_get_resizable       (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_sizing          (AyyiTreeViewColumn*, AyyiTreeViewColumnSizing  type);
AyyiTreeViewColumnSizing ayyi_tree_view_column_get_sizing          (AyyiTreeViewColumn*);
gint                     ayyi_tree_view_column_get_width           (AyyiTreeViewColumn*);
gint                     ayyi_tree_view_column_get_fixed_width     (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_fixed_width     (AyyiTreeViewColumn*, gint fixed_width);
void                     ayyi_tree_view_column_set_min_width       (AyyiTreeViewColumn*, gint min_width);
gint                     ayyi_tree_view_column_get_min_width       (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_max_width       (AyyiTreeViewColumn*, gint max_width);
gint                     ayyi_tree_view_column_get_max_width       (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_clicked             (AyyiTreeViewColumn*);


/* Options for manipulating the column headers
 */
void                     ayyi_tree_view_column_set_title           (AyyiTreeViewColumn*, const gchar* title);
const gchar *            ayyi_tree_view_column_get_title           (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_expand          (AyyiTreeViewColumn*, gboolean expand);
gboolean                 ayyi_tree_view_column_get_expand          (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_clickable       (AyyiTreeViewColumn*, gboolean clickable);
gboolean                 ayyi_tree_view_column_get_clickable       (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_widget          (AyyiTreeViewColumn*, AGlActor*);
AGlActor*                ayyi_tree_view_column_get_widget          (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_alignment       (AyyiTreeViewColumn*, gfloat xalign);
gfloat                   ayyi_tree_view_column_get_alignment       (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_reorderable     (AyyiTreeViewColumn*, gboolean reorderable);
gboolean                 ayyi_tree_view_column_get_reorderable     (AyyiTreeViewColumn*);



/* You probably only want to use ayyi_tree_view_column_set_sort_column_id.  The
 * other sorting functions exist primarily to let others do their own custom sorting.
 */
void                     ayyi_tree_view_column_set_sort_column_id  (AyyiTreeViewColumn*, gint sort_column_id);
gint                     ayyi_tree_view_column_get_sort_column_id  (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_sort_indicator  (AyyiTreeViewColumn*, gboolean setting);
gboolean                 ayyi_tree_view_column_get_sort_indicator  (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_set_sort_order      (AyyiTreeViewColumn*, GtkSortType order);
GtkSortType              ayyi_tree_view_column_get_sort_order      (AyyiTreeViewColumn*);


/* These functions are meant primarily for interaction between the AyyiTreeView and thn
 */
void                     ayyi_tree_view_column_cell_set_cell_data  (AyyiTreeViewColumn*, AyyiTreeModel*, AyyiTreeIter*, gboolean is_expander, gboolean is_expanded);
void                     ayyi_tree_view_column_cell_get_size       (AyyiTreeViewColumn*, const GdkRectangle* cell_area, gint* x_offset, gint* y_offset, gint* width, gint *height);
gboolean                 ayyi_tree_view_column_cell_is_visible     (AyyiTreeViewColumn*);
void                     ayyi_tree_view_column_focus_cell          (AyyiTreeViewColumn*, AyyiCellRenderer*);
gboolean                 ayyi_tree_view_column_cell_get_position   (AyyiTreeViewColumn*, AyyiCellRenderer*, gint* start_pos, gint* width);
void                     ayyi_tree_view_column_queue_resize        (AyyiTreeViewColumn*);
AGlActor*                ayyi_tree_view_column_get_tree_view       (AyyiTreeViewColumn*);


G_END_DECLS


#endif
