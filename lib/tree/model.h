/* gtktreemodel.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __ayyi_tree_model_h__
#define __ayyi_tree_model_h__

#include <stdbool.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define AYYI_TYPE_TREE_MODEL           (ayyi_tree_model_get_type ())
#define AYYI_TREE_MODEL(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_TREE_MODEL, AyyiTreeModel))
#define AYYI_IS_TREE_MODEL(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_TREE_MODEL))
#define AYYI_TREE_MODEL_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), AYYI_TYPE_TREE_MODEL, AyyiTreeModelIface))

#define AYYI_TYPE_TREE_ITER            (ayyi_tree_iter_get_type ())
#define AYYI_TYPE_TREE_PATH            (ayyi_tree_path_get_type ())
#define AYYI_TYPE_TREE_ROW_REFERENCE   (ayyi_tree_row_reference_get_type ())

typedef struct _AyyiTreeIter         AyyiTreeIter;
typedef struct _AyyiTreePath         AyyiTreePath;
typedef struct _AyyiTreeRowReference AyyiTreeRowReference;
typedef struct _AyyiTreeModel        AyyiTreeModel; /* Dummy typedef */
typedef struct _AyyiTreeModelIface   AyyiTreeModelIface;

typedef gboolean (*AyyiTreeModelForeachFunc) (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*, gpointer);


typedef enum
{
   AYYI_TREE_MODEL_ITERS_PERSIST = 1 << 0,
   AYYI_TREE_MODEL_LIST_ONLY     = 1 << 1
} AyyiTreeModelFlags;

struct _AyyiTreeIter
{
   gint stamp;
   gpointer user_data;
   gpointer user_data2;
   gpointer user_data3;
};

struct _AyyiTreeModelIface
{
   GTypeInterface g_iface;

   /* Signals */
   void         (*row_changed)           (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*);
   void         (*row_inserted)          (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*);
   void         (*row_has_child_toggled) (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*);
   void         (*row_deleted)           (AyyiTreeModel*, AyyiTreePath*);
   void         (*rows_reordered)        (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*, gint* new_order);

   /* Virtual Table */
   AyyiTreeModelFlags (*get_flags)  (AyyiTreeModel*);

   gint         (*get_n_columns)   (AyyiTreeModel*);
   GType        (*get_column_type) (AyyiTreeModel*, gint index_);
   gboolean     (*get_iter)        (AyyiTreeModel*, AyyiTreeIter*, AyyiTreePath*);
   AyyiTreePath*(*get_path)        (AyyiTreeModel*, AyyiTreeIter*);
   void         (*get_value)       (AyyiTreeModel*, AyyiTreeIter*, gint column, GValue*);
   gboolean     (*iter_next)       (AyyiTreeModel*, AyyiTreeIter*);
   gboolean     (*iter_children)   (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* parent);
   gboolean     (*iter_has_child)  (AyyiTreeModel*, AyyiTreeIter*);
   gint         (*iter_n_children) (AyyiTreeModel*, AyyiTreeIter*);
   gboolean     (*iter_nth_child)  (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* parent, gint n);
   gboolean     (*iter_parent)     (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* child);
   void         (*ref_node)        (AyyiTreeModel*, AyyiTreeIter*);
   void         (*unref_node)      (AyyiTreeModel*, AyyiTreeIter*);

   /* Extra methods not part of the GtkTreeModel interface */
   void         (*delete_rows) (AyyiTreeModel*, void (*foreach)(AyyiTreeModelForeachFunc));
};


/* AyyiTreePath operations */
AyyiTreePath* ayyi_tree_path_new              ();
AyyiTreePath* ayyi_tree_path_new_from_string  (const gchar*);
AyyiTreePath* ayyi_tree_path_new_from_indices (gint first_index, ...);
gchar*        ayyi_tree_path_to_string        (AyyiTreePath*);
AyyiTreePath* ayyi_tree_path_new_first        ();
void          ayyi_tree_path_append_index     (AyyiTreePath*, gint index);
void          ayyi_tree_path_prepend_index    (AyyiTreePath*, gint index);
gint          ayyi_tree_path_get_depth        (AyyiTreePath*);
gint*         ayyi_tree_path_get_indices      (AyyiTreePath*);
gint*         ayyi_tree_path_get_indices_with_depth (AyyiTreePath*, gint* depth);
void          ayyi_tree_path_free             (AyyiTreePath* path);
AyyiTreePath* ayyi_tree_path_copy             (const AyyiTreePath*);
GType         ayyi_tree_path_get_type         (void) G_GNUC_CONST;
gint          ayyi_tree_path_compare          (const AyyiTreePath*, const AyyiTreePath*);
bool          ayyi_tree_path_next             (AyyiTreePath*);
bool          ayyi_tree_path_prev             (AyyiTreePath*);
bool          ayyi_tree_path_up               (AyyiTreePath*);
void          ayyi_tree_path_down             (AyyiTreePath*);

gboolean      ayyi_tree_path_is_ancestor      (AyyiTreePath*, AyyiTreePath* descendant);
gboolean      ayyi_tree_path_is_descendant    (AyyiTreePath*, AyyiTreePath* ancestor);

#define ayyi_tree_path_free0(var) (var = (ayyi_tree_path_free(var), NULL))

/* Row reference (an object that tracks model changes so it refers to the same
 * row always; a path refers to a position, not a fixed row).  You almost always
 * want to call ayyi_tree_row_reference_new.
 */

GType                 ayyi_tree_row_reference_get_type  (void) G_GNUC_CONST;
AyyiTreeRowReference* ayyi_tree_row_reference_new       (AyyiTreeModel*, AyyiTreePath*);
AyyiTreeRowReference* ayyi_tree_row_reference_new_proxy (GObject* proxy, AyyiTreeModel*, AyyiTreePath*);
AyyiTreePath*         ayyi_tree_row_reference_get_path  (AyyiTreeRowReference *reference);
AyyiTreeModel*        ayyi_tree_row_reference_get_model (AyyiTreeRowReference *reference);
gboolean              ayyi_tree_row_reference_valid     (AyyiTreeRowReference *reference);
AyyiTreeRowReference* ayyi_tree_row_reference_copy      (AyyiTreeRowReference *reference);
void                  ayyi_tree_row_reference_free      (AyyiTreeRowReference *reference);
/* These two functions are only needed if you created the row reference with a
 * proxy object */
void                  ayyi_tree_row_reference_inserted  (GObject*, AyyiTreePath*);
void                  ayyi_tree_row_reference_deleted   (GObject*, AyyiTreePath*);
void                  ayyi_tree_row_reference_reordered (GObject*, AyyiTreePath*, AyyiTreeIter*, gint* new_order);

#define ayyi_tree_row_reference_free0(var) (var = (ayyi_tree_row_reference_free(var), NULL))

/* AyyiTreeIter operations */
AyyiTreeIter*         ayyi_tree_iter_copy               (AyyiTreeIter*);
void                  ayyi_tree_iter_free               (AyyiTreeIter*);
GType                 ayyi_tree_iter_get_type           (void) G_GNUC_CONST;

GType                 ayyi_tree_model_get_type          (void) G_GNUC_CONST;
AyyiTreeModelFlags    ayyi_tree_model_get_flags         (AyyiTreeModel*);
gint                  ayyi_tree_model_get_n_columns     (AyyiTreeModel*);
GType                 ayyi_tree_model_get_column_type   (AyyiTreeModel*, gint index_);


/* Iterator movement */
gboolean           ayyi_tree_model_get_iter             (AyyiTreeModel*, AyyiTreeIter*, AyyiTreePath*);
gboolean           ayyi_tree_model_get_iter_from_string (AyyiTreeModel*, AyyiTreeIter*, const gchar* path_string);
gchar*             ayyi_tree_model_get_string_from_iter (AyyiTreeModel*, AyyiTreeIter*);
gboolean           ayyi_tree_model_get_iter_first       (AyyiTreeModel*, AyyiTreeIter*);
AyyiTreePath*      ayyi_tree_model_get_path             (AyyiTreeModel*, AyyiTreeIter*);
void               ayyi_tree_model_get_value            (AyyiTreeModel*, AyyiTreeIter*, gint column, GValue*);
gboolean           ayyi_tree_model_iter_next            (AyyiTreeModel*, AyyiTreeIter*);
gboolean           ayyi_tree_model_iter_children        (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* parent);
gboolean           ayyi_tree_model_iter_has_child       (AyyiTreeModel*, AyyiTreeIter*);
gint               ayyi_tree_model_iter_n_children      (AyyiTreeModel*, AyyiTreeIter*);
gboolean           ayyi_tree_model_iter_nth_child       (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* parent, gint);
gboolean           ayyi_tree_model_iter_parent          (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* child);
void               ayyi_tree_model_ref_node             (AyyiTreeModel*, AyyiTreeIter*);
void               ayyi_tree_model_unref_node           (AyyiTreeModel*, AyyiTreeIter*);
void               ayyi_tree_model_get                  (AyyiTreeModel*, AyyiTreeIter*, ...);
void               ayyi_tree_model_get_valist           (AyyiTreeModel*, AyyiTreeIter*, va_list);

void               ayyi_tree_model_foreach              (AyyiTreeModel* model, AyyiTreeModelForeachFunc func, gpointer user_data);


/* Signals */
void ayyi_tree_model_row_changed           (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*);
void ayyi_tree_model_row_inserted          (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*);
void ayyi_tree_model_row_has_child_toggled (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*);
void ayyi_tree_model_row_deleted           (AyyiTreeModel*, AyyiTreePath*);
void ayyi_tree_model_rows_reordered        (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*, gint* new_order);

G_END_DECLS

#endif
