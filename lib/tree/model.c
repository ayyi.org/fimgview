/* gtktreemodel.c
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <gobject/gvaluecollector.h>
#include "debug/debug.h"
#include "model.h"
#include "tree/aglview.h"
#include "marshalers.h"


#define INITIALIZE_TREE_ITER(Iter) \
    G_STMT_START{ \
      (Iter)->stamp = 0; \
      (Iter)->user_data  = NULL; \
      (Iter)->user_data2 = NULL; \
      (Iter)->user_data3 = NULL; \
    }G_STMT_END

#define ROW_REF_DATA_STRING "ayyi-tree-row-refs"

enum {
   ROW_CHANGED,
   ROW_INSERTED,
   ROW_HAS_CHILD_TOGGLED,
   ROW_DELETED,
   ROWS_REORDERED,
   LAST_SIGNAL
};

static guint tree_model_signals[LAST_SIGNAL] = { 0 };

struct _AyyiTreePath
{
   gint  depth;
   gint* indices;
};

typedef struct
{
   GSList* list;
} RowRefList;

static void   ayyi_tree_model_base_init   (gpointer g_class);

/* custom closs */
static void   row_inserted_marshal        (GClosure*, GValue* /* out */ return_value, guint n_param_value, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data);
static void   row_deleted_marshal         (GClosure*, GValue* /* out */ return_value, guint n_param_value, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data);
static void   rows_reordered_marshal      (GClosure*, GValue* /* out */ return_value, guint n_param_value, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data);

static void   ayyi_tree_row_ref_inserted  (RowRefList*, AyyiTreePath*, AyyiTreeIter*);
static void   ayyi_tree_row_ref_deleted   (RowRefList*, AyyiTreePath*);
static void   ayyi_tree_row_ref_reordered (RowRefList*, AyyiTreePath*, AyyiTreeIter*, gint* new_order);


GType
ayyi_tree_model_get_type ()
{
	static GType tree_model_type = 0;

	if (! tree_model_type) {
		const GTypeInfo tree_model_info = {
			sizeof (AyyiTreeModelIface), /* class_size */
			ayyi_tree_model_base_init,   /* base_init */
			NULL,                        /* base_finalize */
			NULL,
			NULL,                        /* class_finalize */
			NULL,                        /* class_data */
			0,
			0,                           /* n_preallocs */
			NULL
		};

		tree_model_type = g_type_register_static (G_TYPE_INTERFACE, "AyyiTreeModel", &tree_model_info, 0);

		g_type_interface_add_prerequisite (tree_model_type, G_TYPE_OBJECT);
	}

	return tree_model_type;
}


static void
ayyi_tree_model_base_init (gpointer g_class)
{
	static gboolean initialized = FALSE;
	GClosure* closure;

	if (! initialized) {
		GType row_inserted_params[2];
		GType row_deleted_params[1];
		GType rows_reordered_params[3];

		row_inserted_params[0] = AYYI_TYPE_TREE_PATH | G_SIGNAL_TYPE_STATIC_SCOPE;
		row_inserted_params[1] = AYYI_TYPE_TREE_ITER;

		row_deleted_params[0] = AYYI_TYPE_TREE_PATH | G_SIGNAL_TYPE_STATIC_SCOPE;

		rows_reordered_params[0] = AYYI_TYPE_TREE_PATH | G_SIGNAL_TYPE_STATIC_SCOPE;
		rows_reordered_params[1] = AYYI_TYPE_TREE_ITER;
		rows_reordered_params[2] = G_TYPE_POINTER;

		/**
		 * AyyiTreeModel::row-changed:
		 * @tree_model: the #AyyiTreeModel on which the signal is emitted
		 * @path: a #AyyiTreePath identifying the changed row
		 * @iter: a valid #AyyiTreeIter pointing to the changed row
		 *
		 * This signal is emitted when a row in the model has changed.
		 */
		tree_model_signals[ROW_CHANGED] = g_signal_new ("row-changed",
                      AYYI_TYPE_TREE_MODEL,
                      G_SIGNAL_RUN_LAST, 
                      G_STRUCT_OFFSET (AyyiTreeModelIface, row_changed),
                      NULL, NULL,
                      _gtk_marshal_VOID__BOXED_BOXED,
                      G_TYPE_NONE, 2,
                      AYYI_TYPE_TREE_PATH | G_SIGNAL_TYPE_STATIC_SCOPE,
                      AYYI_TYPE_TREE_ITER);

		/* We need to get notification about structure changes
		 * to update row references., so instead of using the
		 * standard g_signal_new() with an offset into our interface
		 * structure, we use a customs closures for the class
		 * closures (default handlers) that first update row references
		 * and then calls the function from the interface structure.
		 *
		 * The reason we don't simply update the row references from
		 * the wrapper functions (ayyi_tree_model_row_inserted(), etc.)
		 * is to keep proper ordering with respect to signal handlers
		 * connected normally and after.
		 */

		/**
		 * AyyiTreeModel::row-inserted:
		 * @tree_model: the #AyyiTreeModel on which the signal is emitted
		 * @path: a #AyyiTreePath identifying the new row
		 * @iter: a valid #AyyiTreeIter pointing to the new row
		 *
		 * This signal is emitted when a new row has been inserted in the model.
		 * 
		 * Note that the row may still be empty at this point, since
		 * it is a common pattern to first insert an empty row, and 
		 * then fill it with the desired values.
		 */
		closure = g_closure_new_simple (sizeof (GClosure), NULL);
		g_closure_set_marshal (closure, row_inserted_marshal);
		tree_model_signals[ROW_INSERTED] = g_signal_newv ("row-inserted",
                       AYYI_TYPE_TREE_MODEL,
                       G_SIGNAL_RUN_FIRST,
                       closure,
                       NULL, NULL,
                       _gtk_marshal_VOID__BOXED_BOXED,
                       G_TYPE_NONE, 2,
                       row_inserted_params);

      /**
       * AyyiTreeModel::row-has-child-toggled:
       * @tree_model: the #AyyiTreeModel on which the signal is emitted
       * @path: a #AyyiTreePath identifying the row
       * @iter: a valid #AyyiTreeIter pointing to the row
       *
       * This signal is emitted when a row has gotten the first child row or lost
       * its last child row.
       */
      tree_model_signals[ROW_HAS_CHILD_TOGGLED] =
        g_signal_new ("row-has-child-toggled",
                      AYYI_TYPE_TREE_MODEL,
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (AyyiTreeModelIface, row_has_child_toggled),
                      NULL, NULL,
                      _gtk_marshal_VOID__BOXED_BOXED,
                      G_TYPE_NONE, 2,
                      AYYI_TYPE_TREE_PATH | G_SIGNAL_TYPE_STATIC_SCOPE,
                      AYYI_TYPE_TREE_ITER);

      /**
       * AyyiTreeModel::row-deleted:
       * @tree_model: the #AyyiTreeModel on which the signal is emitted
       * @path: a #AyyiTreePath identifying the row
       *
       * This signal is emitted when a row has been deleted.
       *
       * Note that no iterator is passed to the signal handler,
       * since the row is already deleted.
       *
       * This should be called by models after a row has been removed.
       * The location pointed to by @path should be the location that
       * the row previously was at. It may not be a valid location anymore.
       */
      closure = g_closure_new_simple (sizeof (GClosure), NULL);
      g_closure_set_marshal (closure, row_deleted_marshal);
      tree_model_signals[ROW_DELETED] =
        g_signal_newv ("row-deleted",
                       AYYI_TYPE_TREE_MODEL,
                       G_SIGNAL_RUN_FIRST,
                       closure,
                       NULL, NULL,
                       _gtk_marshal_VOID__BOXED,
                       G_TYPE_NONE, 1,
                       row_deleted_params);

      /**
       * AyyiTreeModel::rows-reordered:
       * @tree_model: the #AyyiTreeModel on which the signal is emitted
       * @path: a #AyyiTreePath identifying the tree node whose children
       *        have been reordered
       * @iter: a valid #AyyiTreeIter pointing to the node whose 
       * @new_order: an array of integers mapping the current position of
       *             each child to its old position before the re-ordering,
       *             i.e. @new_order<literal>[newpos] = oldpos</literal>.
       *
       * This signal is emitted when the children of a node in the #AyyiTreeModel
       * have been reordered. 
       *
       * Note that this signal is <emphasis>not</emphasis> emitted
       * when rows are reordered by DND, since this is implemented
       * by removing and then reinserting the row.
       */
      closure = g_closure_new_simple (sizeof (GClosure), NULL);
      g_closure_set_marshal (closure, rows_reordered_marshal);
      tree_model_signals[ROWS_REORDERED] =
        g_signal_newv ("rows-reordered",
                       AYYI_TYPE_TREE_MODEL,
                       G_SIGNAL_RUN_FIRST,
                       closure,
                       NULL, NULL,
                       _gtk_marshal_VOID__BOXED_BOXED_POINTER,
                       G_TYPE_NONE, 3,
                       rows_reordered_params);
      initialized = TRUE;
    }
}


static void
row_inserted_marshal (GClosure* closure, GValue* return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	void (* row_inserted_callback) (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*) = NULL;
			
	GObject* model = g_value_get_object (param_values + 0);
	AyyiTreePath* path = (AyyiTreePath*)g_value_get_boxed (param_values + 1);
	AyyiTreeIter* iter = (AyyiTreeIter*)g_value_get_boxed (param_values + 2);

	/* first, update internal row references */
	ayyi_tree_row_ref_inserted ((RowRefList*)g_object_get_data (model, ROW_REF_DATA_STRING), path, iter);
							   
	/* fetch the interface ->row_inserted implementation */
	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (model);
	row_inserted_callback = G_STRUCT_MEMBER (gpointer, iface, G_STRUCT_OFFSET (AyyiTreeModelIface, row_inserted));

	/* Call that default signal handler, it if has been set */                                                         
	if (row_inserted_callback)
		row_inserted_callback (AYYI_TREE_MODEL (model), path, iter);
}


static void
row_deleted_marshal (GClosure* closure, GValue* /* out */ return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	void (* row_deleted_callback) (AyyiTreeModel*, AyyiTreePath*) = NULL;
	GObject* model = g_value_get_object (param_values + 0);
	AyyiTreePath* path = (AyyiTreePath*)g_value_get_boxed (param_values + 1);

	/* first, update internal row references */
	ayyi_tree_row_ref_deleted ((RowRefList*)g_object_get_data (model, ROW_REF_DATA_STRING), path);

	/* fetch the interface ->row_deleted implementation */
	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (model);
	row_deleted_callback = G_STRUCT_MEMBER (gpointer, iface, G_STRUCT_OFFSET (AyyiTreeModelIface, row_deleted));

	/* Call that default signal handler, it if has been set */
	if (row_deleted_callback)
		row_deleted_callback (AYYI_TREE_MODEL (model), path);
}


static void
rows_reordered_marshal (GClosure* closure, GValue /* out */  *return_value, guint n_param_values, const GValue* param_values, gpointer invocation_hint, gpointer marshal_data)
{
	void (* rows_reordered_callback) (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*, gint* new_order);
			
	GObject* model = g_value_get_object (param_values + 0);
	AyyiTreePath* path = (AyyiTreePath*)g_value_get_boxed (param_values + 1);
	AyyiTreeIter* iter = (AyyiTreeIter*)g_value_get_boxed (param_values + 2);
	gint* new_order = (gint*)g_value_get_pointer (param_values + 3);

	/* first, we need to update internal row references */
	ayyi_tree_row_ref_reordered ((RowRefList*)g_object_get_data (model, ROW_REF_DATA_STRING), path, iter, new_order);

	/* fetch the interface ->rows_reordered implementation */
	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (model);
	rows_reordered_callback = G_STRUCT_MEMBER (gpointer, iface, G_STRUCT_OFFSET (AyyiTreeModelIface, rows_reordered));

	/* Call that default signal handler, it if has been set */
	if (rows_reordered_callback)
		rows_reordered_callback (AYYI_TREE_MODEL (model), path, iter, new_order);
}


/**
 * ayyi_tree_path_new:
 *
 * Creates a new #AyyiTreePath.  This structure refers to a row.
 *
 * Return value: A newly created #AyyiTreePath.
 **/
/* AyyiTreePath Operations */
AyyiTreePath*
ayyi_tree_path_new (void)
{
	AyyiTreePath* retval = g_slice_new (AyyiTreePath);
	retval->depth = 0;
	retval->indices = NULL;

	return retval;
}


/**
 * ayyi_tree_path_new_from_string:
 * @path: The string representation of a path.
 *
 * Creates a new #AyyiTreePath initialized to @path.  @path is expected to be a
 * colon separated list of numbers.  For example, the string "10:4:0" would
 * create a path of depth 3 pointing to the 11th child of the root node, the 5th
 * child of that 11th child, and the 1st child of that 5th child.  If an invalid
 * path string is passed in, %NULL is returned.
 *
 * Return value: A newly-created #AyyiTreePath, or %NULL
 **/
AyyiTreePath*
ayyi_tree_path_new_from_string (const gchar* path)
{
	const gchar* orig_path = path;
	gchar* ptr;
	gint i;

	g_return_val_if_fail (path, NULL);
	g_return_val_if_fail (*path != '\000', NULL);

	AyyiTreePath* retval = ayyi_tree_path_new ();

	while (1) {
		i = strtol (path, &ptr, 10);
		if (i < 0) {
			g_warning (G_STRLOC ": Negative numbers in path %s passed to ayyi_tree_path_new_from_string", orig_path);
			ayyi_tree_path_free (retval);
			return NULL;
		}

		ayyi_tree_path_append_index (retval, i);

		if (*ptr == '\000')
			break;
		if (ptr == path || *ptr != ':') {
			g_warning (G_STRLOC ": Invalid path %s passed to ayyi_tree_path_new_from_string", orig_path);
			ayyi_tree_path_free (retval);
			return NULL;
		}
		path = ptr + 1;
	}

	return retval;
}


/**
 * ayyi_tree_path_new_from_indices:
 * @first_index: first integer
 * @varargs: list of integers terminated by -1
 *
 * Creates a new path with @first_index and @varargs as indices.
 *
 * Return value: A newly created #AyyiTreePath.
 *
 * Since: 2.2
 **/
AyyiTreePath*
ayyi_tree_path_new_from_indices (gint first_index, ...)
{
	va_list args;

	AyyiTreePath* path = ayyi_tree_path_new ();

	va_start (args, first_index);
	int arg = first_index;

	while (arg != -1) {
		ayyi_tree_path_append_index (path, arg);
		arg = va_arg (args, gint);
	}

	va_end (args);

	return path;
}


/**
 * ayyi_tree_path_to_string:
 * @path: A #AyyiTreePath
 *
 * Generates a string representation of the path.  This string is a ':'
 * separated list of numbers.  For example, "4:10:0:3" would be an acceptable return value for this string.
 *
 * Return value: A newly-allocated string.  Must be freed with g_free().
 **/
gchar *
ayyi_tree_path_to_string (AyyiTreePath *path)
{
	gchar *retval;

	g_return_val_if_fail (path != NULL, NULL);

	if (path->depth == 0)
		return NULL;

	gint n = path->depth * 12;
	gchar* ptr = retval = g_new0 (gchar, n);
	gchar* end = ptr + n;
	g_snprintf (retval, end - ptr, "%d", path->indices[0]);
	while (*ptr != '\000') 
		ptr++;

	int i; for (i = 1; i < path->depth; i++) {
		g_snprintf (ptr, end - ptr, ":%d", path->indices[i]);
		while (*ptr != '\000')
			ptr++;
	}

	return retval;
}


/**
 * ayyi_tree_path_new_first:
 *
 * Creates a new #AyyiTreePath.  The string representation of this path is "0"
 *
 * Return value: A new #AyyiTreePath.
 **/
AyyiTreePath *
ayyi_tree_path_new_first (void)
{
	AyyiTreePath* retval = ayyi_tree_path_new ();
	ayyi_tree_path_append_index (retval, 0);

	return retval;
}


/**
 * ayyi_tree_path_append_index:
 * @path: A #AyyiTreePath.
 * @index_: The index.
 *
 * Appends a new index to a path.  As a result, the depth of the path is
 * increased.
 **/
void
ayyi_tree_path_append_index (AyyiTreePath* path, gint index)
{
  g_return_if_fail (path != NULL);
  g_return_if_fail (index >= 0);

  path->depth += 1;
  path->indices = g_realloc (path->indices, path->depth * sizeof(gint));
  path->indices[path->depth - 1] = index;
}


/**
 * ayyi_tree_path_prepend_index:
 * @path: A #AyyiTreePath.
 * @index_: The index.
 *
 * Prepends a new index to a path.  As a result, the depth of the path is
 * increased.
 **/
void
ayyi_tree_path_prepend_index (AyyiTreePath* path, gint index)
{
  gint *new_indices;

  (path->depth)++;
  new_indices = g_new (gint, path->depth);

  if (path->indices == NULL)
    {
      path->indices = new_indices;
      path->indices[0] = index;
      return;
    }
  memcpy (new_indices + 1, path->indices, (path->depth - 1)*sizeof (gint));
  g_free (path->indices);
  path->indices = new_indices;
  path->indices[0] = index;
}


/**
 * ayyi_tree_path_get_depth:
 * @path: A #AyyiTreePath.
 *
 * Returns the current depth of @path.
 *
 * Return value: The depth of @path
 **/
gint
ayyi_tree_path_get_depth (AyyiTreePath* path)
{
	g_return_val_if_fail (path, 0);

	return path->depth;
}


/**
 * ayyi_tree_path_get_indices:
 * @path: A #AyyiTreePath.
 *
 * Returns the current indices of @path.  This is an array of integers, each
 * representing a node in a tree.  This value should not be freed.
 *
 * Return value: The current indices, or %NULL.
 **/
gint*
ayyi_tree_path_get_indices (AyyiTreePath* path)
{
	g_return_val_if_fail (path, NULL);

	return path->indices;
}


/**
 * ayyi_tree_path_get_indices_with_depth:
 * @path: A #AyyiTreePath.
 * @depth: Number of elements returned in the integer array
 *
 * Returns the current indices of @path.
 * This is an array of integers, each representing a node in a tree.
 * It also returns the number of elements in the array.
 * The array should not be freed.
 *
 * Return value: (array length=depth) (transfer none): The current indices, or %NULL.
 *
 * Since: 2.22
 **/
gint*
ayyi_tree_path_get_indices_with_depth (AyyiTreePath* path, gint* depth)
{
	g_return_val_if_fail (path, NULL);

	if (depth)
		*depth = path->depth;

	return path->indices;
}


/**
 * ayyi_tree_path_free:
 * @path: A #AyyiTreePath.
 *
 * Frees @path.
 **/
void
ayyi_tree_path_free (AyyiTreePath *path)
{
  if (!path)
    return;

  g_free (path->indices);
  g_slice_free (AyyiTreePath, path);
}


/**
 * ayyi_tree_path_copy:
 * @path: A #AyyiTreePath.
 *
 * Creates a new #AyyiTreePath as a copy of @path.
 *
 * Return value: A new #AyyiTreePath.
 **/
AyyiTreePath *
ayyi_tree_path_copy (const AyyiTreePath *path)
{
  AyyiTreePath *retval;

  g_return_val_if_fail (path != NULL, NULL);

  retval = g_slice_new (AyyiTreePath);
  retval->depth = path->depth;
  retval->indices = g_new (gint, path->depth);
  memcpy (retval->indices, path->indices, path->depth * sizeof (gint));
  return retval;
}


GType
ayyi_tree_path_get_type (void)
{
	static GType our_type = 0;
  
	if (our_type == 0)
		our_type = g_boxed_type_register_static ("AyyiTreePath", (GBoxedCopyFunc) ayyi_tree_path_copy, (GBoxedFreeFunc) ayyi_tree_path_free);

	return our_type;
}


/**
 * ayyi_tree_path_compare:
 * @a: A #AyyiTreePath.
 * @b: A #AyyiTreePath to compare with.
 *
 * Compares two paths.  If @a appears before @b in a tree, then -1 is returned.
 * If @b appears before @a, then 1 is returned.  If the two nodes are equal,
 * then 0 is returned.
 *
 * Return value: The relative positions of @a and @b
 **/
gint
ayyi_tree_path_compare (const AyyiTreePath* a, const AyyiTreePath* b)
{
	gint p = 0, q = 0;

	g_return_val_if_fail (a, 0);
	g_return_val_if_fail (b, 0);
	g_return_val_if_fail (a->depth > 0, 0);
	g_return_val_if_fail (b->depth > 0, 0);

	do {
		if (a->indices[p] == b->indices[q])
			continue;
		return (a->indices[p] < b->indices[q] ? -1 : 1);
	}
	while (++p < a->depth && ++q < b->depth);

	if (a->depth == b->depth)
		return 0;
	return (a->depth < b->depth?-1:1);
}


/**
 * ayyi_tree_path_is_ancestor:
 * @path: a #AyyiTreePath
 * @descendant: another #AyyiTreePath
 *
 * Returns %TRUE if @descendant is a descendant of @path.
 *
 * Return value: %TRUE if @descendant is contained inside @path
 **/
gboolean
ayyi_tree_path_is_ancestor (AyyiTreePath *path,
                           AyyiTreePath *descendant)
{
  gint i;

  g_return_val_if_fail (path != NULL, FALSE);
  g_return_val_if_fail (descendant != NULL, FALSE);

  /* can't be an ancestor if we're deeper */
  if (path->depth >= descendant->depth)
    return FALSE;

  i = 0;
  while (i < path->depth)
    {
      if (path->indices[i] != descendant->indices[i])
        return FALSE;
      ++i;
    }

  return TRUE;
}


/**
 * ayyi_tree_path_is_descendant:
 * @path: a #AyyiTreePath
 * @ancestor: another #AyyiTreePath
 *
 * Returns %TRUE if @path is a descendant of @ancestor.
 *
 * Return value: %TRUE if @ancestor contains @path somewhere below it
 **/
gboolean
ayyi_tree_path_is_descendant (AyyiTreePath* path, AyyiTreePath* ancestor)
{
	gint i;

	g_return_val_if_fail (path, FALSE);
	g_return_val_if_fail (ancestor, FALSE);

	/* can't be a descendant if we're shallower in the tree */
	if (path->depth <= ancestor->depth)
		return FALSE;

	i = 0;
	while (i < ancestor->depth) {
		if (path->indices[i] != ancestor->indices[i])
			return FALSE;
		++i;
	}

	return TRUE;
}


/**
 * ayyi_tree_path_next:
 * @path: A #AyyiTreePath.
 *
 * Moves the @path to point to the next node at the current depth.
 **/
bool
ayyi_tree_path_next (AyyiTreePath* path)
{
	g_return_val_if_fail (path, false);
	g_return_val_if_fail (path->depth > 0, false);

	path->indices[path->depth - 1] ++;

	return true;
}


/**
 * ayyi_tree_path_prev:
 * @path: A #AyyiTreePath.
 *
 * Moves the @path to point to the previous node at the current depth, 
 * if it exists.
 *
 * Return value: %TRUE if @path has a previous node, and the move was made.
 **/
bool
ayyi_tree_path_prev (AyyiTreePath* path)
{
	g_return_val_if_fail (path, FALSE);

	if (path->depth == 0)
		return FALSE;

	if (path->indices[path->depth - 1] == 0)
		return FALSE;

	path->indices[path->depth - 1] -= 1;

	return TRUE;
}


/**
 * ayyi_tree_path_up:
 * @path: A #AyyiTreePath.
 *
 * Moves the @path to point to its parent node, if it has a parent.
 *
 * Return value: %TRUE if @path has a parent, and the move was made.
 **/
bool
ayyi_tree_path_up (AyyiTreePath *path)
{
  g_return_val_if_fail (path != NULL, FALSE);

  if (path->depth == 0)
    return FALSE;

  path->depth--;

  return TRUE;
}


/**
 * ayyi_tree_path_down:
 * @path: A #AyyiTreePath.
 *
 * Moves @path to point to the first child of the current path.
 **/
void
ayyi_tree_path_down (AyyiTreePath *path)
{
  g_return_if_fail (path != NULL);

  ayyi_tree_path_append_index (path, 0);
}


/**
 * ayyi_tree_iter_copy:
 * @iter: A #AyyiTreeIter.
 *
 * Creates a dynamically allocated tree iterator as a copy of @iter.  
 * This function is not intended for use in applications, because you 
 * can just copy the structs by value 
 * (<literal>AyyiTreeIter new_iter = iter;</literal>).
 * You must free this iter with ayyi_tree_iter_free().
 *
 * Return value: a newly-allocated copy of @iter.
 **/
AyyiTreeIter *
ayyi_tree_iter_copy (AyyiTreeIter *iter)
{
  AyyiTreeIter *retval;

  g_return_val_if_fail (iter != NULL, NULL);

  retval = g_slice_new (AyyiTreeIter);
  *retval = *iter;

  return retval;
}


/**
 * ayyi_tree_iter_free:
 * @iter: A dynamically allocated tree iterator.
 *
 * Frees an iterator that has been allocated by ayyi_tree_iter_copy().
 * This function is mainly used for language bindings.
 **/
void
ayyi_tree_iter_free (AyyiTreeIter *iter)
{
  g_return_if_fail (iter != NULL);

  g_slice_free (AyyiTreeIter, iter);
}


GType
ayyi_tree_iter_get_type (void)
{
	static GType our_type = 0;

	if (our_type == 0)
		our_type = g_boxed_type_register_static ("AyyiTreeIter", (GBoxedCopyFunc) ayyi_tree_iter_copy, (GBoxedFreeFunc) ayyi_tree_iter_free);

	return our_type;
}


/**
 * ayyi_tree_model_get_flags:
 * @tree_model: A #AyyiTreeModel.
 *
 * Returns a set of flags supported by this interface.  The flags are a bitwise
 * combination of #AyyiTreeModelFlags.  The flags supported should not change
 * during the lifecycle of the @tree_model.
 *
 * Return value: The flags supported by this interface.
 **/
AyyiTreeModelFlags
ayyi_tree_model_get_flags (AyyiTreeModel *tree_model)
{
	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), 0);

	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
	if (iface->get_flags)
		return (* iface->get_flags) (tree_model);

	return 0;
}


/**
 * ayyi_tree_model_get_n_columns:
 * @tree_model: A #AyyiTreeModel.
 *
 * Returns the number of columns supported by @tree_model.
 *
 * Return value: The number of columns.
 **/
gint
ayyi_tree_model_get_n_columns (AyyiTreeModel *tree_model)
{
  AyyiTreeModelIface *iface;
  g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), 0);

  iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
  g_return_val_if_fail (iface->get_n_columns != NULL, 0);

  return (* iface->get_n_columns) (tree_model);
}


/**
 * ayyi_tree_model_get_column_type:
 * @tree_model: A #AyyiTreeModel.
 * @index_: The column index.
 *
 * Returns the type of the column.
 *
 * Return value: (transfer none): The type of the column.
 **/
GType
ayyi_tree_model_get_column_type (AyyiTreeModel* tree_model, gint index)
{

  g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), G_TYPE_INVALID);

  AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
  g_return_val_if_fail (iface->get_column_type, G_TYPE_INVALID);
  g_return_val_if_fail (index >= 0, G_TYPE_INVALID);

  return (* iface->get_column_type) (tree_model, index);
}


/**
 * ayyi_tree_model_get_iter:
 * @tree_model: A #AyyiTreeModel.
 * @iter: (out): The uninitialized #AyyiTreeIter.
 * @path: The #AyyiTreePath.
 *
 * Sets @iter to a valid iterator pointing to @path.
 *
 * Return value: %TRUE, if @iter was set.
 **/
gboolean
ayyi_tree_model_get_iter (AyyiTreeModel* tree_model, AyyiTreeIter* iter, AyyiTreePath* path)
{

	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
	g_return_val_if_fail (iter, FALSE);
	g_return_val_if_fail (path, FALSE);

	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
	g_return_val_if_fail (iface->get_iter, FALSE);
	g_return_val_if_fail (path->depth > 0, FALSE);

	INITIALIZE_TREE_ITER (iter);

	return (* iface->get_iter) (tree_model, iter, path);
}


/**
 * ayyi_tree_model_get_iter_from_string:
 * @tree_model: A #AyyiTreeModel.
 * @iter: (out): An uninitialized #AyyiTreeIter.
 * @path_string: A string representation of a #AyyiTreePath.
 *
 * Sets @iter to a valid iterator pointing to @path_string, if it
 * exists. Otherwise, @iter is left invalid and %FALSE is returned.
 *
 * Return value: %TRUE, if @iter was set.
 **/
gboolean
ayyi_tree_model_get_iter_from_string (AyyiTreeModel *tree_model,
				     AyyiTreeIter  *iter,
				     const gchar  *path_string)
{
  gboolean retval;
  AyyiTreePath *path;

  g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
  g_return_val_if_fail (iter != NULL, FALSE);
  g_return_val_if_fail (path_string != NULL, FALSE);
  
  path = ayyi_tree_path_new_from_string (path_string);
  
  g_return_val_if_fail (path != NULL, FALSE);

  retval = ayyi_tree_model_get_iter (tree_model, iter, path);
  ayyi_tree_path_free (path);
  
  return retval;
}


/**
 * ayyi_tree_model_get_string_from_iter:
 * @tree_model: A #AyyiTreeModel.
 * @iter: An #AyyiTreeIter.
 *
 * Generates a string representation of the iter. This string is a ':'
 * separated list of numbers. For example, "4:10:0:3" would be an
 * acceptable return value for this string.
 *
 * Return value: A newly-allocated string. Must be freed with g_free().
 *
 * Since: 2.2
 **/
gchar *
ayyi_tree_model_get_string_from_iter (AyyiTreeModel* tree_model, AyyiTreeIter* iter)
{
	gchar *ret;

	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), NULL);
	g_return_val_if_fail (iter != NULL, NULL);

	AyyiTreePath* path = ayyi_tree_model_get_path (tree_model, iter);

	g_return_val_if_fail (path != NULL, NULL);

	ret = ayyi_tree_path_to_string (path);
	ayyi_tree_path_free (path);

	return ret;
}


/**
 * ayyi_tree_model_get_iter_first:
 * @tree_model: A #AyyiTreeModel.
 * @iter: (out): The uninitialized #AyyiTreeIter.
 * 
 * Initializes @iter with the first iterator in the tree (the one at the path
 * "0") and returns %TRUE.  Returns %FALSE if the tree is empty.
 * 
 * Return value: %TRUE, if @iter was set.
 **/
gboolean
ayyi_tree_model_get_iter_first (AyyiTreeModel *tree_model,
			       AyyiTreeIter  *iter)
{
  AyyiTreePath *path;
  gboolean retval;

  g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
  g_return_val_if_fail (iter != NULL, FALSE);

  path = ayyi_tree_path_new_first ();
  retval = ayyi_tree_model_get_iter (tree_model, iter, path);
  ayyi_tree_path_free (path);

  return retval;
}


/**
 * ayyi_tree_model_get_path:
 * @tree_model: A #AyyiTreeModel.
 * @iter: The #AyyiTreeIter.
 *
 * Returns a newly-created #AyyiTreePath referenced by @iter.  This path should
 * be freed with ayyi_tree_path_free().
 *
 * Return value: a newly-created #AyyiTreePath.
 **/
AyyiTreePath*
ayyi_tree_model_get_path (AyyiTreeModel* tree_model, AyyiTreeIter* iter)
{
  g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), NULL);
  g_return_val_if_fail (iter != NULL, NULL);

  AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
  g_return_val_if_fail (iface->get_path, NULL);

  return (* iface->get_path) (tree_model, iter);
}


/**
 * ayyi_tree_model_get_value:
 * @tree_model: A #AyyiTreeModel.
 * @iter: The #AyyiTreeIter.
 * @column: The column to lookup the value at.
 * @value: (out) (transfer none): An empty #GValue to set.
 *
 * Initializes and sets @value to that at @column.
 * When done with @value, g_value_unset() needs to be called 
 * to free any allocated memory.
 */
void
ayyi_tree_model_get_value (AyyiTreeModel* tree_model, AyyiTreeIter* iter, gint column, GValue* value)
{

  g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
  g_return_if_fail (iter != NULL);
  g_return_if_fail (value != NULL);

  AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
  g_return_if_fail (iface->get_value != NULL);

  (* iface->get_value) (tree_model, iter, column, value);
}


/**
 * ayyi_tree_model_iter_next:
 * @tree_model: A #AyyiTreeModel.
 * @iter: (in): The #AyyiTreeIter.
 *
 * Sets @iter to point to the node following it at the current level.  If there
 * is no next @iter, %FALSE is returned and @iter is set to be invalid.
 *
 * Return value: %TRUE if @iter has been changed to the next node.
 **/
gboolean
ayyi_tree_model_iter_next (AyyiTreeModel* tree_model, AyyiTreeIter* iter)
{

	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
	g_return_val_if_fail (iter, FALSE);

	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
	g_return_val_if_fail (iface->iter_next != NULL, FALSE);

	return (* iface->iter_next) (tree_model, iter);
}


/**
 * ayyi_tree_model_iter_children:
 * @tree_model: A #AyyiTreeModel.
 * @iter: (out): The new #AyyiTreeIter to be set to the child.
 * @parent: (allow-none): The #AyyiTreeIter, or %NULL
 *
 * Sets @iter to point to the first child of @parent.  If @parent has no
 * children, %FALSE is returned and @iter is set to be invalid.  @parent
 * will remain a valid node after this function has been called.
 *
 * If @parent is %NULL returns the first node, equivalent to
 * <literal>ayyi_tree_model_get_iter_first (tree_model, iter);</literal>
 *
 * Return value: %TRUE, if @child has been set to the first child.
 **/
gboolean
ayyi_tree_model_iter_children (AyyiTreeModel* tree_model, AyyiTreeIter* iter, AyyiTreeIter* parent)
{
	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
	g_return_val_if_fail (iter, FALSE);

	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
	g_return_val_if_fail (iface->iter_children != NULL, FALSE);

	INITIALIZE_TREE_ITER (iter);

	return (* iface->iter_children) (tree_model, iter, parent);
}


/**
 * ayyi_tree_model_iter_has_child:
 * @tree_model: A #AyyiTreeModel.
 * @iter: The #AyyiTreeIter to test for children.
 *
 * Returns %TRUE if @iter has children, %FALSE otherwise.
 *
 * Return value: %TRUE if @iter has children.
 **/
gboolean
ayyi_tree_model_iter_has_child (AyyiTreeModel* tree_model, AyyiTreeIter* iter)
{
	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
	g_return_val_if_fail (iter != NULL, FALSE);

	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
	g_return_val_if_fail (iface->iter_has_child != NULL, FALSE);

	return (* iface->iter_has_child) (tree_model, iter);
}


/**
 * ayyi_tree_model_iter_n_children:
 * @tree_model: A #AyyiTreeModel.
 * @iter: (allow-none): The #AyyiTreeIter, or %NULL.
 *
 * Returns the number of children that @iter has.  As a special case, if @iter
 * is %NULL, then the number of toplevel nodes is returned.
 *
 * Return value: The number of children of @iter.
 **/
gint
ayyi_tree_model_iter_n_children (AyyiTreeModel* tree_model, AyyiTreeIter* iter)
{
	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), 0);

	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
	g_return_val_if_fail (iface->iter_n_children, 0);

	return (* iface->iter_n_children) (tree_model, iter);
}


/**
 * ayyi_tree_model_iter_nth_child:
 * @tree_model: A #AyyiTreeModel.
 * @iter: (out): The #AyyiTreeIter to set to the nth child.
 * @parent: (allow-none): The #AyyiTreeIter to get the child from, or %NULL.
 * @n: Then index of the desired child.
 *
 * Sets @iter to be the child of @parent, using the given index.  The first
 * index is 0.  If @n is too big, or @parent has no children, @iter is set
 * to an invalid iterator and %FALSE is returned.  @parent will remain a valid
 * node after this function has been called.  As a special case, if @parent is
 * %NULL, then the @n<!-- -->th root node is set.
 *
 * Return value: %TRUE, if @parent has an @n<!-- -->th child.
 **/
gboolean
ayyi_tree_model_iter_nth_child (AyyiTreeModel *tree_model,
			       AyyiTreeIter  *iter,
			       AyyiTreeIter  *parent,
			       gint          n)
{
  AyyiTreeModelIface *iface;

  g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
  g_return_val_if_fail (iter != NULL, FALSE);
  g_return_val_if_fail (n >= 0, FALSE);

  iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
  g_return_val_if_fail (iface->iter_nth_child != NULL, FALSE);

  INITIALIZE_TREE_ITER (iter);

  return (* iface->iter_nth_child) (tree_model, iter, parent, n);
}


/**
 * ayyi_tree_model_iter_parent:
 * @tree_model: A #AyyiTreeModel
 * @iter: (out): The new #AyyiTreeIter to set to the parent.
 * @child: The #AyyiTreeIter.
 *
 * Sets @iter to be the parent of @child.  If @child is at the toplevel, and
 * doesn't have a parent, then @iter is set to an invalid iterator and %FALSE
 * is returned.  @child will remain a valid node after this function has been
 * called.
 *
 * Return value: %TRUE, if @iter is set to the parent of @child.
 **/
gboolean
ayyi_tree_model_iter_parent (AyyiTreeModel* tree_model, AyyiTreeIter* iter, AyyiTreeIter* child)
{
	g_return_val_if_fail (AYYI_IS_TREE_MODEL (tree_model), FALSE);
	g_return_val_if_fail (iter != NULL, FALSE);
	g_return_val_if_fail (child != NULL, FALSE);

	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
	g_return_val_if_fail (iface->iter_parent != NULL, FALSE);

	INITIALIZE_TREE_ITER (iter);

	return (* iface->iter_parent) (tree_model, iter, child);
}


/**
 * ayyi_tree_model_ref_node:
 * @tree_model: A #AyyiTreeModel.
 * @iter: The #AyyiTreeIter.
 *
 * Lets the tree ref the node.  This is an optional method for models to
 * implement.  To be more specific, models may ignore this call as it exists
 * primarily for performance reasons.
 * 
 * This function is primarily meant as a way for views to let caching model 
 * know when nodes are being displayed (and hence, whether or not to cache that
 * node.)  For example, a file-system based model would not want to keep the
 * entire file-hierarchy in memory, just the sections that are currently being
 * displayed by every current view.
 *
 * A model should be expected to be able to get an iter independent of its
 * reffed state.
 **/
void
ayyi_tree_model_ref_node (AyyiTreeModel *tree_model,
			 AyyiTreeIter  *iter)
{
  AyyiTreeModelIface *iface;

  g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));

  iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
  if (iface->ref_node)
    (* iface->ref_node) (tree_model, iter);
}


/**
 * ayyi_tree_model_unref_node:
 * @tree_model: A #AyyiTreeModel.
 * @iter: The #AyyiTreeIter.
 *
 * Lets the tree unref the node.  This is an optional method for models to
 * implement.  To be more specific, models may ignore this call as it exists
 * primarily for performance reasons.
 *
 * For more information on what this means, see ayyi_tree_model_ref_node().
 * Please note that nodes that are deleted are not unreffed.
 **/
void
ayyi_tree_model_unref_node (AyyiTreeModel *tree_model,
			   AyyiTreeIter  *iter)
{
  AyyiTreeModelIface *iface;

  g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
  g_return_if_fail (iter != NULL);

  iface = AYYI_TREE_MODEL_GET_IFACE (tree_model);
  if (iface->unref_node)
    (* iface->unref_node) (tree_model, iter);
}


/**
 * ayyi_tree_model_get:
 * @tree_model: a #AyyiTreeModel
 * @iter: a row in @tree_model
 * @Varargs: pairs of column number and value return locations, terminated by -1
 *
 * Gets the value of one or more cells in the row referenced by @iter.
 * The variable argument list should contain integer column numbers,
 * each column number followed by a place to store the value being
 * retrieved.  The list is terminated by a -1. For example, to get a
 * value from column 0 with type %G_TYPE_STRING, you would
 * write: <literal>ayyi_tree_model_get (model, iter, 0, &amp;place_string_here, -1)</literal>,
 * where <literal>place_string_here</literal> is a <type>gchar*</type> to be 
 * filled with the string.
 *
 * Returned values with type %G_TYPE_OBJECT have to be unreferenced, values
 * with type %G_TYPE_STRING or %G_TYPE_BOXED have to be freed. Other values are
 * passed by value.
 **/
void
ayyi_tree_model_get (AyyiTreeModel *tree_model,
		    AyyiTreeIter  *iter,
		    ...)
{
  va_list var_args;

  g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
  g_return_if_fail (iter != NULL);

  va_start (var_args, iter);
  ayyi_tree_model_get_valist (tree_model, iter, var_args);
  va_end (var_args);
}

/**
 * ayyi_tree_model_get_valist:
 * @tree_model: a #AyyiTreeModel
 * @iter: a row in @tree_model
 * @var_args: <type>va_list</type> of column/return location pairs
 *
 * See ayyi_tree_model_get(), this version takes a <type>va_list</type> 
 * for language bindings to use.
 **/
void
ayyi_tree_model_get_valist (AyyiTreeModel* tree_model, AyyiTreeIter* iter, va_list var_args)
{
  g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
  g_return_if_fail (iter != NULL);

  gint column = va_arg (var_args, gint);

  while (column != -1) {
      GValue value = { 0, };
      gchar *error = NULL;

      if (column >= ayyi_tree_model_get_n_columns (tree_model))
	{
	  g_warning ("%s: Invalid column number %d accessed (remember to end your list of columns with a -1)", G_STRLOC, column);
	  break;
	}

      ayyi_tree_model_get_value (AYYI_TREE_MODEL (tree_model), iter, column, &value);

      G_VALUE_LCOPY (&value, var_args, 0, &error);
      if (error)
	{
	  g_warning ("%s: %s", G_STRLOC, error);
	  g_free (error);

 	  /* we purposely leak the value here, it might not be
	   * in a sane state if an error condition occoured
	   */
	  break;
	}

      g_value_unset (&value);

      column = va_arg (var_args, gint);
    }
}


/**
 * ayyi_tree_model_row_changed:
 * @tree_model: A #AyyiTreeModel
 * @path: A #AyyiTreePath pointing to the changed row
 * @iter: A valid #AyyiTreeIter pointing to the changed row
 * 
 * Emits the "row-changed" signal on @tree_model.
 **/
void
ayyi_tree_model_row_changed (AyyiTreeModel* tree_model, AyyiTreePath* path, AyyiTreeIter* iter)
{
	g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
	g_return_if_fail (path != NULL);
	g_return_if_fail (iter != NULL);

	g_signal_emit (tree_model, tree_model_signals[ROW_CHANGED], 0, path, iter);
}


/**
 * ayyi_tree_model_row_inserted:
 * @tree_model: A #AyyiTreeModel
 * @path: A #AyyiTreePath pointing to the inserted row
 * @iter: A valid #AyyiTreeIter pointing to the inserted row
 * 
 * Emits the "row-inserted" signal on @tree_model
 **/
void
ayyi_tree_model_row_inserted (AyyiTreeModel* tree_model, AyyiTreePath* path, AyyiTreeIter* iter)
{
	g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
	g_return_if_fail (path != NULL);
	g_return_if_fail (iter != NULL);

	g_signal_emit (tree_model, tree_model_signals[ROW_INSERTED], 0, path, iter);
}

/**
 * ayyi_tree_model_row_has_child_toggled:
 * @tree_model: A #AyyiTreeModel
 * @path: A #AyyiTreePath pointing to the changed row
 * @iter: A valid #AyyiTreeIter pointing to the changed row
 * 
 * Emits the "row-has-child-toggled" signal on @tree_model.  This should be
 * called by models after the child state of a node changes.
 **/
void
ayyi_tree_model_row_has_child_toggled (AyyiTreeModel* tree_model, AyyiTreePath* path, AyyiTreeIter* iter)
{
	g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
	g_return_if_fail (path != NULL);
	g_return_if_fail (iter != NULL);

	g_signal_emit (tree_model, tree_model_signals[ROW_HAS_CHILD_TOGGLED], 0, path, iter);
}


/**
 * ayyi_tree_model_row_deleted:
 * @tree_model: A #AyyiTreeModel
 * @path: A #AyyiTreePath pointing to the previous location of the deleted row.
 * 
 * Emits the "row-deleted" signal on @tree_model.  This should be called by
 * models after a row has been removed.  The location pointed to by @path 
 * should be the location that the row previously was at.  It may not be a 
 * valid location anymore.
 **/
void
ayyi_tree_model_row_deleted (AyyiTreeModel* tree_model, AyyiTreePath* path)
{
	g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
	g_return_if_fail (path);

	g_signal_emit (tree_model, tree_model_signals[ROW_DELETED], 0, path);
}


/**
 * ayyi_tree_model_rows_reordered:
 * @tree_model: A #AyyiTreeModel
 * @path: A #AyyiTreePath pointing to the tree node whose children have been 
 *      reordered
 * @iter: A valid #AyyiTreeIter pointing to the node whose children have been 
 *      reordered, or %NULL if the depth of @path is 0.
 * @new_order: an array of integers mapping the current position of each child
 *      to its old position before the re-ordering,
 *      i.e. @new_order<literal>[newpos] = oldpos</literal>.
 * 
 * Emits the "rows-reordered" signal on @tree_model.  This should be called by
 * models when their rows have been reordered.  
 **/
void
ayyi_tree_model_rows_reordered (AyyiTreeModel* tree_model, AyyiTreePath* path, AyyiTreeIter* iter, gint* new_order)
{
	g_return_if_fail (AYYI_IS_TREE_MODEL (tree_model));
	g_return_if_fail (new_order != NULL);

	g_signal_emit (tree_model, tree_model_signals[ROWS_REORDERED], 0, path, iter, new_order);
}


static gboolean
ayyi_tree_model_foreach_helper (AyyiTreeModel* model, AyyiTreeIter* iter, AyyiTreePath* path, AyyiTreeModelForeachFunc func, gpointer user_data)
{
  do
    {
      AyyiTreeIter child;

      if ((* func) (model, path, iter, user_data))
	return TRUE;

      if (ayyi_tree_model_iter_children (model, &child, iter))
	{
	  ayyi_tree_path_down (path);
	  if (ayyi_tree_model_foreach_helper (model, &child, path, func, user_data))
	    return TRUE;
	  ayyi_tree_path_up (path);
	}

      ayyi_tree_path_next (path);
    }
  while (ayyi_tree_model_iter_next (model, iter));

  return FALSE;
}

/**
 * ayyi_tree_model_foreach:
 * @model: A #AyyiTreeModel
 * @func: (scope call): A function to be called on each row
 * @user_data: User data to passed to func.
 *
 * Calls func on each node in model in a depth-first fashion.
 * If @func returns %TRUE, then the tree ceases to be walked, and
 * ayyi_tree_model_foreach() returns.
 **/
void
ayyi_tree_model_foreach (AyyiTreeModel* model, AyyiTreeModelForeachFunc func, gpointer user_data)
{
  AyyiTreePath *path;
  AyyiTreeIter iter;

  g_return_if_fail (AYYI_IS_TREE_MODEL (model));
  g_return_if_fail (func != NULL);

  path = ayyi_tree_path_new_first ();
  if (ayyi_tree_model_get_iter (model, &iter, path) == FALSE)
    {
      ayyi_tree_path_free (path);
      return;
    }

  ayyi_tree_model_foreach_helper (model, &iter, path, func, user_data);
  ayyi_tree_path_free (path);
}


/*
 * AyyiTreeRowReference
 */

static void ayyi_tree_row_reference_unref_path (AyyiTreePath* path, AyyiTreeModel* model, gint depth);


GType
ayyi_tree_row_reference_get_type (void)
{
	static GType our_type = 0;
  
	if (our_type == 0)
		our_type = g_boxed_type_register_static ("AyyiTreeRowReference", (GBoxedCopyFunc) ayyi_tree_row_reference_copy, (GBoxedFreeFunc) ayyi_tree_row_reference_free);

	return our_type;
}


struct _AyyiTreeRowReference
{
   GObject* proxy;
   AyyiTreeModel* model;
   AyyiTreePath* path;
};


static void
release_row_references (gpointer data)
{
	RowRefList* refs = data;

	GSList* l = refs->list;
	while (l) {
		AyyiTreeRowReference* reference = l->data;

		if (reference->proxy == (GObject*)reference->model)
			reference->model = NULL;
		reference->proxy = NULL;

		/* we don't free the reference, users are responsible for that. */

		l = g_slist_next (l);
	}

	g_slist_free (refs->list);
	g_free (refs);
}


static void
ayyi_tree_row_ref_inserted (RowRefList* refs, AyyiTreePath* path, AyyiTreeIter* iter)
{
	if (!refs)
		return;

	/* This function corrects the path stored in the reference to
	 * account for an insertion. Note that it's called _after_ the insertion
	 * with the path to the newly-inserted row. Which means that
	 * the inserted path is in a different "coordinate system" than
	 * the old path (e.g. if the inserted path was just before the old path,
	 * then inserted path and old path will be the same, and old path must be
	 * moved down one).
	 */

	GSList* l = refs->list;
	while (l) {
		AyyiTreeRowReference* reference = l->data;

		if (!reference->path)
			goto done;

		if (reference->path->depth >= path->depth) {
			gint i;
			gboolean ancestor = TRUE;

			for (i = 0; i < path->depth - 1; i ++) {
				if (path->indices[i] != reference->path->indices[i]) {
					ancestor = FALSE;
					break;
				}
			}
			if (ancestor == FALSE)
				goto done;

			if (path->indices[path->depth-1] <= reference->path->indices[path->depth-1])
				reference->path->indices[path->depth-1] += 1;
		}
  done:
		l = g_slist_next(l);
	}
}


static void
ayyi_tree_row_ref_deleted (RowRefList* refs, AyyiTreePath* path)
{
	if (!refs)
	    return;

	/* This function corrects the path stored in the reference to
	 * account for an deletion. Note that it's called _after_ the
	 * deletion with the old path of the just-deleted row. Which means
	 * that the deleted path is the same now-defunct "coordinate system"
	 * as the path saved in the reference, which is what we want to fix.
	 */

	GSList* l = refs->list;

	while (l) {
		AyyiTreeRowReference* reference = l->data;

		if (reference->path) {
			if (path->depth > reference->path->depth)
				goto next;

			gint i; for (i = 0; i < path->depth - 1; i++) {
				if (path->indices[i] != reference->path->indices[i])
					goto next;
			}

			/* We know it affects us. */
			if (path->indices[i] == reference->path->indices[i]) {
				if (reference->path->depth > path->depth)
					/* some parent was deleted, trying to unref any node
					 * between the deleted parent and the node the reference
					 * is pointing to is bad, as those nodes are already gone.
					 */
					ayyi_tree_row_reference_unref_path (reference->path, reference->model, path->depth - 1);
				else
					ayyi_tree_row_reference_unref_path (reference->path, reference->model, reference->path->depth - 1);

				ayyi_tree_path_free0 (reference->path);
			}
			else if (path->indices[i] < reference->path->indices[i]) {
				reference->path->indices[path->depth-1]-=1;
			}
		}

	  next:
		l = g_slist_next (l);
	}
}


static void
ayyi_tree_row_ref_reordered (RowRefList* refs, AyyiTreePath* path, AyyiTreeIter* iter, gint* new_order)
{
	if (!refs)
		return;

	GSList* l = refs->list;

	while (l) {
		AyyiTreeRowReference *reference = l->data;

		gint length = ayyi_tree_model_iter_n_children (AYYI_TREE_MODEL (reference->model), iter);

		if (length < 2)
			return;

		if ((reference->path) && (ayyi_tree_path_is_ancestor (path, reference->path))) {
			gint ref_depth = ayyi_tree_path_get_depth (reference->path);
			gint depth = ayyi_tree_path_get_depth (path);

			if (ref_depth > depth) {
				gint i;
				gint* indices = ayyi_tree_path_get_indices (reference->path);

				for (i = 0; i < length; i++) {
					if (new_order[i] == indices[depth]) {
						indices[depth] = i;
						break;
					}
				}
			}
		}

		l = g_slist_next (l);
	}
}


/* We do this recursively so that we can unref children nodes before their parent */
static void
ayyi_tree_row_reference_unref_path_helper (AyyiTreePath* path, AyyiTreeModel* model, AyyiTreeIter* parent_iter, gint depth, gint current_depth)
{
	AyyiTreeIter iter;

	if (depth == current_depth)
		return;

	ayyi_tree_model_iter_nth_child (model, &iter, parent_iter, path->indices[current_depth]);
	ayyi_tree_row_reference_unref_path_helper (path, model, &iter, depth, current_depth + 1);
	ayyi_tree_model_unref_node (model, &iter);
}


static void
ayyi_tree_row_reference_unref_path (AyyiTreePath* path, AyyiTreeModel* model, gint depth)
{
	AyyiTreeIter iter;

	if (depth <= 0)
		return;
  
	ayyi_tree_model_iter_nth_child (model, &iter, NULL, path->indices[0]);
	ayyi_tree_row_reference_unref_path_helper (path, model, &iter, depth, 1);
	ayyi_tree_model_unref_node (model, &iter);
}


/**
 * ayyi_tree_row_reference_new:
 * @model: A #AyyiTreeModel
 * @path: A valid #AyyiTreePath to monitor
 * 
 * Creates a row reference based on @path.  This reference will keep pointing 
 * to the node pointed to by @path, so long as it exists.  It listens to all
 * signals emitted by @model, and updates its path appropriately.  If @path
 * isn't a valid path in @model, then %NULL is returned.
 * 
 * Return value: A newly allocated #AyyiTreeRowReference, or %NULL
 **/
AyyiTreeRowReference*
ayyi_tree_row_reference_new (AyyiTreeModel* model, AyyiTreePath* path)
{
	g_return_val_if_fail (AYYI_IS_TREE_MODEL(model), NULL);
	g_return_val_if_fail (path, NULL);

	/* We use the model itself as the proxy object; and call
	 * ayyi_tree_row_reference_inserted(), etc, in the
	 * class closure (default handler) marshalers for the signal.
	 */  
	return ayyi_tree_row_reference_new_proxy (G_OBJECT (model), model, path);
}


/**
 * ayyi_tree_row_reference_new_proxy:
 * @proxy: A proxy #GObject
 * @model: A #AyyiTreeModel
 * @path: A valid #AyyiTreePath to monitor
 * 
 * You do not need to use this function.  Creates a row reference based on
 * @path.  This reference will keep pointing to the node pointed to by @path, 
 * so long as it exists.  If @path isn't a valid path in @model, then %NULL is
 * returned.  However, unlike references created with
 * ayyi_tree_row_reference_new(), it does not listen to the model for changes.
 * The creator of the row reference must do this explicitly using
 * ayyi_tree_row_reference_inserted(), ayyi_tree_row_reference_deleted(),
 * ayyi_tree_row_reference_reordered().
 * 
 * These functions must be called exactly once per proxy when the
 * corresponding signal on the model is emitted. This single call
 * updates all row references for that proxy. Since built-in GTK+
 * objects like #GtkTreeView already use this mechanism internally,
 * using them as the proxy object will produce unpredictable results.
 * Further more, passing the same object as @model and @proxy
 * doesn't work for reasons of internal implementation.
 *
 * This type of row reference is primarily meant by structures that need to
 * carefully monitor exactly when a row reference updates itself, and is not
 * generally needed by most applications.
 *
 * Return value: A newly allocated #AyyiTreeRowReference, or %NULL
 **/
AyyiTreeRowReference*
ayyi_tree_row_reference_new_proxy (GObject* proxy, AyyiTreeModel* model, AyyiTreePath* path)
{
	g_return_val_if_fail (G_IS_OBJECT (proxy), NULL);
	g_return_val_if_fail (AYYI_IS_TREE_MODEL(model), NULL);
	g_return_val_if_fail (path, NULL);
	g_return_val_if_fail (path->depth > 0, NULL);

	/* check that the path is valid */
	AyyiTreeIter parent_iter;
	if (!ayyi_tree_model_get_iter (model, &parent_iter, path))
		return NULL;

	/* Now we want to ref every node */
	ayyi_tree_model_iter_nth_child (model, &parent_iter, NULL, path->indices[0]);
	ayyi_tree_model_ref_node (model, &parent_iter);

	gint i;
	for (i = 1; i < path->depth; i++) {
		AyyiTreeIter iter;
		ayyi_tree_model_iter_nth_child (model, &iter, &parent_iter, path->indices[i]);
		ayyi_tree_model_ref_node (model, &iter);
		parent_iter = iter;
	}

	/* Make the row reference */
	AyyiTreeRowReference* reference = g_new(AyyiTreeRowReference, 1);

	g_object_ref (proxy);
	g_object_ref (model);
	reference->proxy = proxy;
	reference->model = model;
	reference->path = ayyi_tree_path_copy (path);

	RowRefList* refs = g_object_get_data (G_OBJECT(proxy), ROW_REF_DATA_STRING);

	if (!refs) {
		refs = g_new (RowRefList, 1);
		refs->list = NULL;

		g_object_set_data_full (G_OBJECT(proxy), ROW_REF_DATA_STRING, refs, release_row_references);
	}

	refs->list = g_slist_prepend (refs->list, reference);

	return reference;
}


/**
 * ayyi_tree_row_reference_get_path:
 * @reference: A #AyyiTreeRowReference
 * 
 * Returns a path that the row reference currently points to, or %NULL if the
 * path pointed to is no longer valid.
 * 
 * Return value: A current path, or %NULL.
 **/
AyyiTreePath*
ayyi_tree_row_reference_get_path (AyyiTreeRowReference* reference)
{
	g_return_val_if_fail (reference, NULL);

	if (!reference->proxy)
		return NULL;

	if (!reference->path)
		return NULL;

	return ayyi_tree_path_copy (reference->path);
}


/**
 * ayyi_tree_row_reference_get_model:
 * @reference: A #AyyiTreeRowReference
 *
 * Returns the model that the row reference is monitoring.
 *
 * Return value: (transfer none): the model
 *
 * Since: 2.8
 */
AyyiTreeModel *
ayyi_tree_row_reference_get_model (AyyiTreeRowReference* reference)
{
	g_return_val_if_fail (reference, NULL);

	return reference->model;
}


/**
 * ayyi_tree_row_reference_valid:
 * @reference: (allow-none): A #AyyiTreeRowReference, or %NULL
 * 
 * Returns %TRUE if the @reference is non-%NULL and refers to a current valid
 * path.
 * 
 * Return value: %TRUE if @reference points to a valid path.
 **/
gboolean
ayyi_tree_row_reference_valid (AyyiTreeRowReference* reference)
{
	if (reference == NULL || reference->path == NULL)
		return FALSE;

	return TRUE;
}


/**
 * ayyi_tree_row_reference_copy:
 * @reference: a #AyyiTreeRowReference
 * 
 * Copies a #AyyiTreeRowReference.
 * 
 * Return value: a copy of @reference.
 *
 * Since: 2.2
 **/
AyyiTreeRowReference*
ayyi_tree_row_reference_copy (AyyiTreeRowReference *reference)
{
	return ayyi_tree_row_reference_new_proxy (reference->proxy, reference->model, reference->path);
}


/**
 * ayyi_tree_row_reference_free:
 * @reference: (allow-none): A #AyyiTreeRowReference, or %NULL
 * 
 * Free's @reference. @reference may be %NULL.
 **/
void
ayyi_tree_row_reference_free (AyyiTreeRowReference *reference)
{
	if (!reference)
		return;

	RowRefList* refs = g_object_get_data (G_OBJECT (reference->proxy), ROW_REF_DATA_STRING);

	if (!refs) {
		g_warning (G_STRLOC": bad row reference, proxy has no outstanding row references");
		return;
	}

	refs->list = g_slist_remove (refs->list, reference);

	if (!refs->list) {
		g_object_set_data (G_OBJECT (reference->proxy), ROW_REF_DATA_STRING, NULL);
	}

	if (reference->path) {
		ayyi_tree_row_reference_unref_path (reference->path, reference->model, reference->path->depth);
		ayyi_tree_path_free (reference->path);
		reference->path = NULL;
	}

	g_object_unref (reference->proxy);
	g_object_unref (reference->model);
	g_free (reference);
}


/**
 * ayyi_tree_row_reference_inserted:
 * @proxy: A #GObject
 * @path: The row position that was inserted
 * 
 * Lets a set of row reference created by ayyi_tree_row_reference_new_proxy()
 * know that the model emitted the "row_inserted" signal.
 **/
void
ayyi_tree_row_reference_inserted (GObject* proxy, AyyiTreePath* path)
{
	g_return_if_fail (G_IS_OBJECT (proxy));

	ayyi_tree_row_ref_inserted ((RowRefList*)g_object_get_data (proxy, ROW_REF_DATA_STRING), path, NULL);
}

/**
 * ayyi_tree_row_reference_deleted:
 * @proxy: A #GObject
 * @path: The path position that was deleted
 * 
 * Lets a set of row reference created by ayyi_tree_row_reference_new_proxy()
 * know that the model emitted the "row_deleted" signal.
 **/
void
ayyi_tree_row_reference_deleted (GObject* proxy, AyyiTreePath* path)
{
	g_return_if_fail (G_IS_OBJECT (proxy));

	ayyi_tree_row_ref_deleted ((RowRefList*)g_object_get_data (proxy, ROW_REF_DATA_STRING), path);
}


/**
 * ayyi_tree_row_reference_reordered:
 * @proxy: A #GObject
 * @path: The parent path of the reordered signal
 * @iter: The iter pointing to the parent of the reordered
 * @new_order: The new order of rows
 * 
 * Lets a set of row reference created by ayyi_tree_row_reference_new_proxy()
 * know that the model emitted the "rows_reordered" signal.
 **/
void
ayyi_tree_row_reference_reordered (GObject* proxy, AyyiTreePath* path, AyyiTreeIter* iter, gint* new_order)
{
	g_return_if_fail (G_IS_OBJECT (proxy));

	ayyi_tree_row_ref_reordered ((RowRefList*)g_object_get_data (proxy, ROW_REF_DATA_STRING), path, iter, new_order);
}

#define __AYYI_TREE_MODEL_C__
