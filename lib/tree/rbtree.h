/* ayyirbtree.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* A Red-Black Tree implementation used specifically by AyyiTreeView.
 */
#ifndef __AYYI_RBTREE_H__
#define __AYYI_RBTREE_H__

#include <glib.h>


G_BEGIN_DECLS


typedef enum
{
  AYYI_RBNODE_BLACK = 1 << 0,
  AYYI_RBNODE_RED = 1 << 1,
  AYYI_RBNODE_IS_PARENT = 1 << 2,
  AYYI_RBNODE_IS_SELECTED = 1 << 3,
  AYYI_RBNODE_IS_PRELIT = 1 << 4,
  AYYI_RBNODE_IS_SEMI_COLLAPSED = 1 << 5,
  AYYI_RBNODE_IS_SEMI_EXPANDED = 1 << 6,
  AYYI_RBNODE_INVALID = 1 << 7,
  AYYI_RBNODE_COLUMN_INVALID = 1 << 8,
  AYYI_RBNODE_DESCENDANTS_INVALID = 1 << 9,
  AYYI_RBNODE_NON_COLORS = AYYI_RBNODE_IS_PARENT |
  			  AYYI_RBNODE_IS_SELECTED |
  			  AYYI_RBNODE_IS_PRELIT |
                          AYYI_RBNODE_IS_SEMI_COLLAPSED |
                          AYYI_RBNODE_IS_SEMI_EXPANDED |
                          AYYI_RBNODE_INVALID |
                          AYYI_RBNODE_COLUMN_INVALID |
                          AYYI_RBNODE_DESCENDANTS_INVALID
} AyyiRBNodeColor;

typedef struct _AyyiRBTree AyyiRBTree;
typedef struct _AyyiRBNode AyyiRBNode;
typedef struct _AyyiRBTreeView AyyiRBTreeView;

typedef void (*AyyiRBTreeTraverseFunc) (AyyiRBTree*, AyyiRBNode*, gpointer);

struct _AyyiRBTree
{
   AyyiRBNode* root;
   AyyiRBNode* nil;
   AyyiRBTree* parent_tree;
   AyyiRBNode* parent_node;
};

struct _AyyiRBNode
{
   guint flags : 14;

   /* We keep track of whether the aggregate count of children plus 1
    * for the node itself comes to an even number.  The parity flag is
    * the total count of children mod 2, where the total count of
    * children gets computed in the same way that the total offset gets
    * computed. i.e. not the same as the "count" field below which
    * doesn't include children. We could replace parity with a
    * full-size int field here, and then take % 2 to get the parity flag,
    * but that would use extra memory.
    */

   guint parity : 1;

   AyyiRBNode* left;
   AyyiRBNode* right;
   AyyiRBNode* parent;

   /* count is the number of nodes beneath us, plus 1 for ourselves.
    * i.e. node->left->count + node->right->count + 1
    */
   gint count;

   /* this is the total of sizes of
    * node->left, node->right, our own height, and the height
    * of all trees in ->children, if children exists because
    * the thing is expanded.
    */
   gint offset;

   /* Child trees */
   AyyiRBTree *children;
};


#define AYYI_RBNODE_GET_COLOR(node)         (node?(((node->flags&AYYI_RBNODE_RED)==AYYI_RBNODE_RED)?AYYI_RBNODE_RED:AYYI_RBNODE_BLACK):AYYI_RBNODE_BLACK)
#define AYYI_RBNODE_SET_COLOR(node,color)   if((node->flags&color)!=color)node->flags=node->flags^(AYYI_RBNODE_RED|AYYI_RBNODE_BLACK)
#define AYYI_RBNODE_GET_HEIGHT(node)        (node->offset-(node->left->offset+node->right->offset+(node->children?node->children->root->offset:0)))
#define AYYI_RBNODE_SET_FLAG(node, flag)    G_STMT_START{ (node->flags|=flag); }G_STMT_END
#define AYYI_RBNODE_UNSET_FLAG(node, flag)  G_STMT_START{ (node->flags&=~(flag)); }G_STMT_END
#define AYYI_RBNODE_FLAG_SET(node, flag)    (node?(((node->flags&flag)==flag)?TRUE:FALSE):FALSE)


AyyiRBTree* _ayyi_rbtree_new              (void);
void        _ayyi_rbtree_free             (AyyiRBTree*);
void        _ayyi_rbtree_remove           (AyyiRBTree*);
void        _ayyi_rbtree_destroy          (AyyiRBTree*);
AyyiRBNode* _ayyi_rbtree_insert_before    (AyyiRBTree*, AyyiRBNode*, gint height, gboolean valid);
AyyiRBNode* _ayyi_rbtree_insert_after     (AyyiRBTree*, AyyiRBNode*, gint height, gboolean valid);
void        _ayyi_rbtree_remove_node      (AyyiRBTree*, AyyiRBNode*);
void        _ayyi_rbtree_reorder          (AyyiRBTree*, gint* new_order, gint length);
AyyiRBNode* _ayyi_rbtree_find_count       (AyyiRBTree*, gint count);
void        _ayyi_rbtree_node_set_height  (AyyiRBTree*, AyyiRBNode*, gint height);
void        _ayyi_rbtree_node_mark_invalid(AyyiRBTree*, AyyiRBNode*);
void        _ayyi_rbtree_node_mark_valid  (AyyiRBTree*, AyyiRBNode*);
void        _ayyi_rbtree_column_invalid   (AyyiRBTree*);
void        _ayyi_rbtree_mark_invalid     (AyyiRBTree*);
void        _ayyi_rbtree_set_fixed_height (AyyiRBTree*, gint height, gboolean mark_valid);
gint        _ayyi_rbtree_node_find_offset (AyyiRBTree*, AyyiRBNode*);
gint        _ayyi_rbtree_node_find_parity (AyyiRBTree*, AyyiRBNode*);
gint        _ayyi_rbtree_find_offset      (AyyiRBTree*, gint offset, AyyiRBTree** new_tree, AyyiRBNode** new_node);
void        _ayyi_rbtree_traverse         (AyyiRBTree*, AyyiRBNode*, GTraverseType order, AyyiRBTreeTraverseFunc func, gpointer data);
AyyiRBNode* _ayyi_rbtree_next             (AyyiRBTree*, AyyiRBNode*);
AyyiRBNode* _ayyi_rbtree_prev             (AyyiRBTree*, AyyiRBNode*);
void        _ayyi_rbtree_next_full        (AyyiRBTree*, AyyiRBNode*, AyyiRBTree** new_tree, AyyiRBNode** new_node);
void        _ayyi_rbtree_prev_full        (AyyiRBTree*, AyyiRBNode*, AyyiRBTree** new_tree, AyyiRBNode** new_node);

gint        _ayyi_rbtree_get_depth        (AyyiRBTree*);

/* This func checks the integrity of the tree */
#ifdef G_ENABLE_DEBUG  
void        _ayyi_rbtree_test             (const gchar* where, AyyiRBTree*);
void        _ayyi_rbtree_debug_spew       (AyyiRBTree*);
#endif


G_END_DECLS


#endif /* __AYYI_RBTREE_H__ */
