/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __observable_h__
#define __observable_h__

typedef int (*ObservableMax) ();

typedef struct {
   int           value;
   ObservableMax max;
   GList*        subscriptions;
   gpointer      user_data;
} Observable;

typedef void (*ObservableFn) (Observable*, int value, gpointer);

Observable* observable_new       ();
void        observable_free      (Observable*);
void        observable_set       (Observable*, int);
void        observable_subscribe (Observable*, ObservableFn, gpointer);

#endif
