/* gtkcellrenderer.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __ayyi_cell_renderer_h__
#define __ayyi_cell_renderer_h__

#include  <glib-object.h>
#include "icon/gdk.h"
#include "agl/actor.h"

G_BEGIN_DECLS

typedef enum
{
   AYYI_CELL_RENDERER_SELECTED    = 1 << 0,
   AYYI_CELL_RENDERER_PRELIT      = 1 << 1,
   AYYI_CELL_RENDERER_INSENSITIVE = 1 << 2,
   /* this flag means the cell is in the sort column/row */
   AYYI_CELL_RENDERER_SORTED      = 1 << 3,
   AYYI_CELL_RENDERER_FOCUSED     = 1 << 4
} AyyiCellRendererState;

typedef enum
{
   AYYI_CELL_RENDERER_MODE_INERT,
   AYYI_CELL_RENDERER_MODE_ACTIVATABLE,
   AYYI_CELL_RENDERER_MODE_EDITABLE
} AyyiCellRendererMode;

#define AYYI_TYPE_CELL_RENDERER            (ayyi_cell_renderer_get_type ())
#define AYYI_CELL_RENDERER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_CELL_RENDERER, AyyiCellRenderer))
#define AYYI_CELL_RENDERER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_CELL_RENDERER, AyyiCellRendererClass))
#define AYYI_IS_CELL_RENDERER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_CELL_RENDERER))
#define AYYI_IS_CELL_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_CELL_RENDERER))
#define AYYI_CELL_RENDERER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_CELL_RENDERER, AyyiCellRendererClass))

GType ayyi_cell_renderer_mode_get_type (void) G_GNUC_CONST;
#define AYYI_TYPE_CELL_RENDERER_MODE (ayyi_cell_renderer_mode_get_type ())

typedef struct _AyyiCellRenderer AyyiCellRenderer;
typedef struct _AyyiCellRendererClass AyyiCellRendererClass;
typedef struct _AyyiCellRendererPrivate AyyiCellRendererPrivate;

struct _AyyiCellRenderer
{
   GObject parent;

   gfloat xalign;
   gfloat yalign;

   gint width;
   gint height;

   guint16 xpad;
   guint16 ypad;

   guint mode : 2;
   guint visible : 1;
   guint is_expander : 1;
   guint is_expanded : 1;
   guint cell_background_set : 1;
   guint sensitive : 1;
   guint editing : 1;

   AyyiCellRendererPrivate* priv;
};

struct _AyyiCellRendererClass
{
  GObjectClass parent_class;

  /* vtable - not signals */
  void             (* get_size)      (AyyiCellRenderer*, AGlActor*, GdkRectangle* cell_area, gint* x_offset, gint* y_offset, gint* width, gint* height);
  void             (* render)        (AyyiCellRenderer*, GdkDrawable*, AGlActor*, GdkRectangle* background_area, GdkRectangle* cell_area, GdkRectangle* expose_area, AyyiCellRendererState flags);
  gboolean         (* activate)      (AyyiCellRenderer*, GdkEvent*, AGlActor*, const gchar* path, GdkRectangle* background_area, GdkRectangle* cell_area, AyyiCellRendererState flags);
#if 0
  GtkCellEditable *(* start_editing) (AyyiCellRenderer* cell, GdkEvent*, AGlActor*, const gchar* path, GdkRectangle* background_area, GdkRectangle* cell_area, AyyiCellRendererState flags);

  /* Signals */
  void (* editing_canceled) (AyyiCellRenderer*);
  void (* editing_started)  (AyyiCellRenderer*, GtkCellEditable*, const gchar* path);
#endif

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
};

GType            ayyi_cell_renderer_get_type       (void) G_GNUC_CONST;

void             ayyi_cell_renderer_get_size       (AyyiCellRenderer*, AGlActor*, const GdkRectangle* cell_area, gint* x_offset, gint* y_offset, gint* width, gint* height);
void             ayyi_cell_renderer_render         (AyyiCellRenderer*, GdkWindow*, AGlActor*, const GdkRectangle* background_area, const GdkRectangle* cell_area, const GdkRectangle* expose_area, AyyiCellRendererState flags);
gboolean         ayyi_cell_renderer_activate       (AyyiCellRenderer*, GdkEvent*, AGlActor*, const gchar* path, const GdkRectangle* background_area, const GdkRectangle* cell_area, AyyiCellRendererState flags);
#if 0
GtkCellEditable *ayyi_cell_renderer_start_editing  (AyyiCellRenderer*, GdkEvent*, AGlActor*, const gchar* path, const GdkRectangle* background_area, const GdkRectangle* cell_area, AyyiCellRendererState flags);
#endif

void             ayyi_cell_renderer_set_fixed_size (AyyiCellRenderer*, gint width, gint height);
void             ayyi_cell_renderer_get_fixed_size (AyyiCellRenderer*, gint* width, gint* height);

void             ayyi_cell_renderer_set_alignment  (AyyiCellRenderer*, gfloat xalign, gfloat yalign);
void             ayyi_cell_renderer_get_alignment  (AyyiCellRenderer*, gfloat* xalign, gfloat* yalign);

void             ayyi_cell_renderer_set_padding    (AyyiCellRenderer*, gint xpad, gint ypad);
void             ayyi_cell_renderer_get_padding    (AyyiCellRenderer*, gint* xpad, gint* ypad);

void             ayyi_cell_renderer_set_visible    (AyyiCellRenderer*, gboolean visible);
gboolean         ayyi_cell_renderer_get_visible    (AyyiCellRenderer*);

void             ayyi_cell_renderer_set_sensitive  (AyyiCellRenderer*, gboolean sensitive);
gboolean         ayyi_cell_renderer_get_sensitive  (AyyiCellRenderer*);

/* For use by cell renderer implementations only */
void ayyi_cell_renderer_stop_editing     (AyyiCellRenderer*, gboolean canceled);


G_END_DECLS

#endif
