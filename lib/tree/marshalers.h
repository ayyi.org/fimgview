
#ifndef __ayyi_marshal_MARSHAL_H__
#define __ayyi_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:VOID (./gtkmarshalers.list:112) */
#define _gtk_marshal_VOID__VOID	g_cclosure_marshal_VOID__VOID

/* VOID:BOXED (./gtkmarshalers.list:56) */
#define _gtk_marshal_VOID__BOXED	g_cclosure_marshal_VOID__BOXED

/* VOID:BOXED,BOXED (./gtkmarshalers.list:57) */
extern void _gtk_marshal_VOID__BOXED_BOXED (GClosure     *closure,
                                            GValue       *return_value,
                                            guint         n_param_values,
                                            const GValue *param_values,
                                            gpointer      invocation_hint,
                                            gpointer      marshal_data);

/* VOID:BOXED,BOXED,POINTER (./gtkmarshalers.list:58) */
extern void _gtk_marshal_VOID__BOXED_BOXED_POINTER (GClosure     *closure,
                                                    GValue       *return_value,
                                                    guint         n_param_values,
                                                    const GValue *param_values,
                                                    gpointer      invocation_hint,
                                                    gpointer      marshal_data);

G_END_DECLS

#endif

