/* gtktreeselection.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __ayyi_tree_selection_h__
#define __ayyi_tree_selection_h__

typedef struct _AyyiTreeSelection      AyyiTreeSelection;
typedef struct _AyyiTreeSelectionClass AyyiTreeSelectionClass;

#include "tree/aglview.h"
#include "tree/model.h"
#include "tree/viewprivate.h"

G_BEGIN_DECLS


#define AYYI_TYPE_TREE_SELECTION            (ayyi_tree_selection_get_type ())
#define AYYI_TREE_SELECTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_TREE_SELECTION, AyyiTreeSelection))
#define AYYI_TREE_SELECTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_TREE_SELECTION, AyyiTreeSelectionClass))
#define AYYI_IS_TREE_SELECTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_TREE_SELECTION))
#define AYYI_IS_TREE_SELECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_TREE_SELECTION))
#define AYYI_TREE_SELECTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_TREE_SELECTION, AyyiTreeSelectionClass))

typedef gboolean (* AyyiTreeSelectionFunc)    (AyyiTreeSelection*, AyyiTreeModel*, AyyiTreePath*, gboolean path_currently_selected, gpointer);
typedef void (* AyyiTreeSelectionForeachFunc) (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*, gpointer);

/* list selection modes */
typedef enum
{
   AYYI_SELECTION_NONE,                             /* Nothing can be selected */
   AYYI_SELECTION_SINGLE,
   AYYI_SELECTION_BROWSE,
   AYYI_SELECTION_MULTIPLE,
} AyyiSelectionMode;

struct _AyyiTreeSelection
{
   GObject parent;

   /*< private >*/

   AGlTreeView*          tree_view;
   AyyiSelectionMode     type;
   AyyiTreeSelectionFunc user_func;
   gpointer              user_data;
   GDestroyNotify        destroy;
};

struct _AyyiTreeSelectionClass
{
   GObjectClass parent_class;

   void (* changed) (AyyiTreeSelection *selection);

   /* Padding for future expansion */
   void (*_gtk_reserved1) (void);
   void (*_gtk_reserved2) (void);
   void (*_gtk_reserved3) (void);
   void (*_gtk_reserved4) (void);
};


GType             ayyi_tree_selection_get_type            (void) G_GNUC_CONST;

void              ayyi_tree_selection_set_mode            (AyyiTreeSelection*, AyyiSelectionMode);
AyyiSelectionMode ayyi_tree_selection_get_mode            (AyyiTreeSelection*);
void              ayyi_tree_selection_set_select_function (AyyiTreeSelection*, AyyiTreeSelectionFunc, gpointer, GDestroyNotify);
gpointer          ayyi_tree_selection_get_user_data       (AyyiTreeSelection*);
AGlTreeView*      ayyi_tree_selection_get_tree_view       (AyyiTreeSelection*);

AyyiTreeSelectionFunc ayyi_tree_selection_get_select_function (AyyiTreeSelection*);

/* Only meaningful if GTK_SELECTION_SINGLE or GTK_SELECTION_BROWSE is set */
/* Use selected_foreach or get_selected_rows for GTK_SELECTION_MULTIPLE */
gboolean         ayyi_tree_selection_get_selected        (AyyiTreeSelection*, AyyiTreeModel*, AyyiTreeIter*);
GList*           ayyi_tree_selection_get_selected_rows   (AyyiTreeSelection*, AyyiTreeModel**);
gint             ayyi_tree_selection_count_selected_rows (AyyiTreeSelection*);
void             ayyi_tree_selection_selected_foreach    (AyyiTreeSelection*, AyyiTreeSelectionForeachFunc, gpointer);
void             ayyi_tree_selection_select_path         (AyyiTreeSelection*, AyyiTreePath*);
void             ayyi_tree_selection_unselect_path       (AyyiTreeSelection*, AyyiTreePath*);
void             ayyi_tree_selection_select_iter         (AyyiTreeSelection*, AyyiTreeIter*);
void             ayyi_tree_selection_unselect_iter       (AyyiTreeSelection*, AyyiTreeIter*);
gboolean         ayyi_tree_selection_path_is_selected    (AyyiTreeSelection*, AyyiTreePath*);
gboolean         ayyi_tree_selection_iter_is_selected    (AyyiTreeSelection*, AyyiTreeIter*);
void             ayyi_tree_selection_select_all          (AyyiTreeSelection*);
void             ayyi_tree_selection_unselect_all        (AyyiTreeSelection*);
void             ayyi_tree_selection_select_range        (AyyiTreeSelection*, AyyiTreePath* start_path, AyyiTreePath* end_path);
void             ayyi_tree_selection_unselect_range      (AyyiTreeSelection*, AyyiTreePath* start_path, AyyiTreePath* end_path);


G_END_DECLS

#endif
