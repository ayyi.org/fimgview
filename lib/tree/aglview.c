/*
 +----------------------------------------------------------------------+
 | This file is part of Samplecat. https://ayyi.github.io/samplecat/    |
 | copyright (C) 2016-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 +----------------------------------------------------------------------+
 | AGlTreeView                                                          |
 | Intended to be a base class that works with a AyyiTreeModel          |
 | Introduced initially to support AyyiTreeSelection                    |
 +----------------------------------------------------------------------+
 |
*/

#include "config.h"
#include "debug/debug.h"
#include "agl/ext.h"
#include "agl/actor.h"
#include "agl/event.h"
#include "tree/model.h"
#include "tree/selection.h"
#include "views/typedefs.h"
#include "views/scrollbar.h"
#include "tree/aglview.h"
#include "tree/viewprivate.h"

#define _g_free0(var) (var = (g_free (var), NULL))

#define row_height tree_row_height
#define N_ROWS_VISIBLE(A) (agl_actor__height(((AGlActor*)A)) / row_height - 1)
#define scrollable_height (ayyi_tree_model_iter_n_children (model, NULL))
#define max_scroll_offset (MAX(0, scrollable_height - N_ROWS_VISIBLE(actor) + 2))

#if 0
#define ROW_HEIGHT(tree_view, height) \
  ((height > 0) ? (height) : (tree_view)->priv->expander_size)
#else
#define ROW_HEIGHT(tree_view, height) \
  (height)
#endif

/* The "background" areas of all rows/cells add up to cover the entire tree.
 * The background includes all inter-row and inter-cell spacing.
 * The "cell" areas are the cell_area passed in to gtk_cell_renderer_render(),
 * i.e. just the cells, no spacing.
 */
#define BACKGROUND_HEIGHT(node) (AYYI_RBNODE_GET_HEIGHT (node))

/* This is in bin_window coordinates */
#define BACKGROUND_FIRST_PIXEL(tree_view,tree,node) (RBTREE_Y_TO_TREE_WINDOW_Y (tree_view, _ayyi_rbtree_node_find_offset ((tree), (node))))

/* Translate from bin_window coordinates to rbtree (tree coordinates) and
 * vice versa.
 */
#define TREE_WINDOW_Y_TO_RBTREE_Y(tree_view,y) ((y) + tree_view->priv->dy)
#define RBTREE_Y_TO_TREE_WINDOW_Y(tree_view,y) ((y) - tree_view->priv->dy)

static AGlActor* agl_tree_view                        (AGlActor*);
static void      ayyi_tree_view_build_tree            (AGlTreeView*, AyyiRBTree*, AyyiTreeIter*, gint depth, gboolean recurse);
static void      ayyi_tree_view_row_inserted          (AyyiTreeModel*, AyyiTreePath*, AyyiTreeIter*, gpointer);
static void      ayyi_tree_view_row_deleted           (AyyiTreeModel*, AyyiTreePath*, gpointer);
static void      ayyi_tree_view_top_row_to_dy         (AGlTreeView*);
static void      ayyi_tree_view_dy_to_top_row         (AGlTreeView*);
static void      agl_tree_view_free_rbtree            (AGlTreeView*);
static void      initialize_fixed_height_mode         (AGlTreeView*);
static void      agl_tree_view_real_set_cursor        (AGlTreeView*, AyyiTreePath*, gboolean clear_and_select, gboolean clamp_node);
static bool      agl_tree_view_real_select_cursor_row (AGlTreeView*, bool start_editing);
static bool      agl_tree_view_real_toggle_cursor_row (AGlTreeView*);
static void      ayyi_tree_view_clamp_node_visible    (AGlTreeView*, AyyiRBTree*, AyyiRBNode*);

void             ayyi_tree_view_scroll_to_cell        (AGlTreeView*, AyyiTreePath*, AyyiTreeViewColumn*, gboolean use_align, gfloat row_align, gfloat col_align);
void             ayyi_tree_view_get_background_area   (AGlTreeView*, AyyiTreePath*, AyyiTreeViewColumn*, GdkRectangle*);

static AGl* agl = NULL;
static int instance_count = 0;
static AGlActorClass actor_class = {0, "AGlTreeView", (AGlActorNew*)agl_tree_view};


AGlActorClass*
agl_tree_view_get_class ()
{
	return &actor_class;
}


static void
_init ()
{
	if (!agl) {
		agl = agl_get_instance();
	}
}


#if 0
static iRange
row_range(AGlTreeView* view)
{
	int n_rows = N_ROWS_VISIBLE(view);
#if 0
	return (iRange){view->scroll_offset, MIN(items->len, view->scroll_offset + n_rows)};
#else
	return (iRange){view->scroll_offset, view->scroll_offset + n_rows};
#endif
}
#endif


#if 0
static int
n_visible_columns(AGlTreeView* view, int col[], int n)
{
	int aw = agl_actor__width((AGlActor*)view);

	int c; for(c=0;c<n-1;c++){
		if(col[c + 1] > aw) return c;
	}
	return n - 1;
}
#endif


void
agl_tree_view_init (AGlActor* a)
{
	AGlTreeView* view = (AGlTreeView*)a;

#ifdef XXAGL_ACTOR_RENDER_CACHE
	a->fbo = agl_fbo_new(agl_actor__width(a), agl_actor__height(a), 0, AGL_FBO_HAS_STENCIL);
	a->cache.enabled = true;
#endif

	view->priv->proxy = g_object_new(G_TYPE_OBJECT, NULL);

	view->selection = _ayyi_tree_selection_new_with_tree_view (view);
#if 0
	ayyi_tree_selection_set_mode(view->selection, AYYI_SELECTION_SINGLE);
#else
	ayyi_tree_selection_set_mode(view->selection, AYYI_SELECTION_MULTIPLE);
#endif

	agl_actor__add_child (a->parent, view->priv->scrollbar = scrollbar_view ((AGlActor*)view, AGL_ORIENTATION_VERTICAL, view->scroll));
}


void
agl_tree_view_set_size (AGlActor* actor)
{
	AGlTreeView* view = (AGlTreeView*)actor;
	AyyiTreeModel* model = view->priv->model;
	if (!model) return;

	observable_set (view->scroll, CLAMP(view->scroll->value, 0, max_scroll_offset));

	/*
	if (tree_view->priv->height <= tree_view->priv->vadjustment->page_size)
		gtk_adjustment_set_value (GTK_ADJUSTMENT (tree_view->priv->vadjustment), 0);
	else if (tree_view->priv->vadjustment->value + tree_view->priv->vadjustment->page_size > tree_view->priv->height)
		gtk_adjustment_set_value (GTK_ADJUSTMENT (tree_view->priv->vadjustment), tree_view->priv->height - tree_view->priv->vadjustment->page_size);
	else*/ if (ayyi_tree_row_reference_valid (view->priv->top_row))
		ayyi_tree_view_top_row_to_dy (view);
	else
		ayyi_tree_view_dy_to_top_row (view);
}


typedef struct {
	AGlTreeView* view;
} DeleteRowsContext;

static DeleteRowsContext delete_context = {0,};

/*
 *  Rows are deleted in reverse order so that indexing
 *  does not change.
 *
 *  It would be safer to delete rows by reference
 *  to their value rather than their index. Using
 *  AyyiRowReference would be one method, or using
 *  a custom reference managed by the model implementation.
 *  But when deleting a very large number of rows, there
 *  may be some performance implications for this.
 */
static void
delete_rows_foreach(AyyiTreeModelForeachFunc foreach)
{
	AyyiTreeModel* model;
	AGlTreeView* view = delete_context.view;
#ifdef DEBUG
	AyyiTreeIter prev = {.user_data = GINT_TO_POINTER(INT_MAX)};
#endif
	GList* list = ayyi_tree_selection_get_selected_rows(view->selection, &model);

	// select row above before deleting
	AyyiTreeRowReference* sel_ref = NULL;
	{
		if(view->selection->type == AYYI_SELECTION_MULTIPLE){
			ayyi_tree_selection_unselect_all(view->selection);
		}

		AyyiTreePath* sel = ayyi_tree_path_copy(list->data);
		gint* indices = ayyi_tree_path_get_indices (sel);
		if(indices[0] != 0){
			if(ayyi_tree_path_prev(sel)){
				ayyi_tree_selection_select_path(view->selection, sel);
				agl_tree_view_set_cursor(delete_context.view, sel, NULL);
			}
		}else{
			// 1st row is selected. need to set selection after end of current selection.
			ayyi_tree_path_free(sel);
			GList* l = g_list_last(list);
			sel = ayyi_tree_path_copy(l->data);
			ayyi_tree_path_next(sel);
			ayyi_tree_selection_select_path(view->selection, sel);
			agl_tree_view_set_cursor(delete_context.view, sel, NULL);
		}

		sel_ref = ayyi_tree_row_reference_new(model, sel);
		ayyi_tree_path_free(sel);
	}

	GList* l = g_list_last(list);
	for(;l;l=l->prev){
		AyyiTreePath* path = l->data;
		AyyiTreeIter iter;
		ayyi_tree_model_get_iter(model, &iter, path);
#ifdef DEBUG
		g_assert(iter.user_data < prev.user_data); // data loss can occur if rows deleted out of order
#endif
		foreach(model, path, &iter, NULL);
#ifdef DEBUG
		prev = iter;
#endif
	}
	g_list_foreach (list, (GFunc)ayyi_tree_path_free, NULL);
	g_list_free (list);

	if(sel_ref){
		AyyiTreePath* sel = ayyi_tree_row_reference_get_path(sel_ref);
		AyyiRBTree* tree = NULL;
		AyyiRBNode* node = NULL;
		_ayyi_tree_view_find_node (view, sel, &tree, &node);

		ayyi_tree_view_clamp_node_visible (view, tree, node);
		ayyi_tree_row_reference_free(sel_ref);
		ayyi_tree_path_free(sel);
	}
}


static void
_on_delete_request_resolved (gpointer _view, gpointer _promise)
{
	AGlTreeView* view = _view;
	AMPromise* promise = _promise;
	AyyiTreeModel* model = view->priv->model;

	dbg(1, "----> value=%i", promise->value.i);

	if(promise->value.i){
		delete_context.view = view;
		AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE(model);
						// a simpler alternative would be to call iface->delete_row multiple times
		(*iface->delete_rows)(model, delete_rows_foreach);
	}

	am_promise_unref(promise);
}


static int cursor_step = 1;

bool
agl_tree_view_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
{
	AGlTreeView* view = (AGlTreeView*)actor;
	AGlTreeViewPriv* v = view->priv;
	AyyiTreeModel* model = view->priv->model;
	if (!model) return AGL_NOT_HANDLED;
	AyyiTreeModelIface* iface = AYYI_TREE_MODEL_GET_IFACE(model);

	switch(event->type){
		case AGL_BUTTON_PRESS:
			switch (event->button.button) {
				case 4:
					dbg(1, "! scroll up");
					observable_set (view->scroll, MAX(view->scroll->value - 1, 0));
					agl_actor__invalidate (actor);
					break;
				case 5:
					dbg(1, "! scroll down");
					if (scrollable_height > N_ROWS_VISIBLE(actor)) {
						if (view->scroll->value < scrollable_height - N_ROWS_VISIBLE(actor) + 1) {
							observable_set (view->scroll, view->scroll->value + 1);
							agl_actor__invalidate (actor);
						}
					}
					break;
			}
			break;
		case AGL_BUTTON_RELEASE:
			;AyyiTreePath* row = agl_tree_view_path_at_coord (view, 0, xy.y - actor->region.y1 - actor->scrollable.y1);
			dbg(1, "RELEASE button=%i y=%.0f row=%p", event->button.button, xy.y - actor->region.y1, row);
			if(row){
				switch(event->button.button){
					case 1:
						if(event->button.state & GDK_CONTROL_MASK){
							/*
							v->modify_selection_pressed = true;
							agl_tree_view_set_cursor(view, row, NULL);
							v->modify_selection_pressed = false;
							*/

#if 0
							ayyi_tree_selection_select_path(view->selection, row);
#else
							agl_tree_view_real_set_cursor (view, row, FALSE, TRUE);
							agl_tree_view_real_toggle_cursor_row (view);
#endif
						}else if(event->button.state & GDK_SHIFT_MASK){
							v->extend_selection_pressed = true;
#if 0
							agl_tree_view_set_cursor(view, row, NULL);
#else
							agl_tree_view_real_set_cursor (view, row, FALSE, TRUE);
							agl_tree_view_real_select_cursor_row (view, FALSE);
#endif
							v->extend_selection_pressed = false;
						}else{
							// normal button press
							if(view->selection->type == AYYI_SELECTION_MULTIPLE){
								ayyi_tree_selection_unselect_all(view->selection);
							}

							agl_tree_view_set_cursor(view, row, NULL);
						}
						break;
				}
				ayyi_tree_path_free(row);
			}
			break;
		case AGL_KEY_PRESS:
			switch (((AGlEventKey*)event)->keyval) {
				case XK_1 ... XK_9:
					cursor_step = ((GdkEventKey*)event)->keyval - XK_0;
					break;
				case XK_Up:
					dbg(1, "KEY_PRESS Up");
					AyyiTreeIter iter;
					if (!view->lock_selection) {
						if (view->selection->type == AYYI_SELECTION_MULTIPLE) {
							AyyiTreePath* cursor;
							agl_tree_view_get_cursor (view, &cursor, NULL);
							if (cursor) {
								bool shift = ((AGlEventKey*)event)->state & ShiftMask;
#if 0
								v->extend_selection_pressed = !!shift;
								v->modify_selection_pressed = false;
#else
								v->extend_selection_pressed = false;
								v->modify_selection_pressed = !!shift;
#endif

								ayyi_tree_path_prev(cursor);
								agl_tree_view_set_cursor(view, cursor, NULL);
								if (ayyi_tree_selection_path_is_selected (view->selection, cursor)) {
									ayyi_tree_path_next(cursor);
									ayyi_tree_selection_unselect_path(view->selection, cursor);
								} else {
									ayyi_tree_selection_select_path(view->selection, cursor);
								}

								ayyi_tree_path_free(cursor);

//								agl_actor__invalidate(actor); // TODO should not be needed
							}
						} else {
							if (ayyi_tree_selection_get_selected (view->selection, v->model, &iter)) {
#if 0
								if(GPOINTER_TO_INT(iter.user_data) > 0){
									iter.user_data = GINT_TO_POINTER(GPOINTER_TO_INT(iter.user_data) - 1);
									ayyi_tree_selection_select_iter(view->selection, &iter);
								}
#else
								AyyiTreePath* path = ayyi_tree_model_get_path(v->model, &iter);
								if (ayyi_tree_path_prev(path)) {
									ayyi_tree_selection_select_path(view->selection, path);
								}
								ayyi_tree_path_free(path);
#endif
							}
						}
					}
					break;
				case XK_Down:
					dbg(1, "KEY_PRESS Down");
					if (!view->lock_selection) {
						if (view->selection->type == AYYI_SELECTION_MULTIPLE) {
							AyyiTreePath* cursor;
							agl_tree_view_get_cursor (view, &cursor, NULL);
							if (!cursor) cursor = ayyi_tree_path_new_first();

							bool shift = ((AGlEventKey*)event)->state & ShiftMask;
							//v->extend_selection_pressed = !!shift;
							v->extend_selection_pressed = false;
							v->modify_selection_pressed = !!shift;

							for (int i=0;i<cursor_step;i++) {
								ayyi_tree_path_next(cursor);
								if ((*iface->get_iter)(model, NULL, cursor)) { // check the cursor is valid
									agl_tree_view_set_cursor(view, cursor, NULL);
									ayyi_tree_selection_select_path(view->selection, cursor);
								}
							}

							ayyi_tree_path_free(cursor);
						} else {
							if (ayyi_tree_selection_get_selected (view->selection, view->priv->model, &iter)) {
								if (ayyi_tree_model_iter_next (view->priv->model, &iter)) {
									ayyi_tree_selection_select_iter(view->selection, &iter);
								}
							}
#ifdef DEBUG
							else pwarn("  failed to get selection iter");
#endif
						}
					}
					break;
				case XK_Page_Up:
				case XK_Page_Down:
					if (!view->lock_selection) {
						typedef bool (*NextFn) (AyyiTreePath*);
						NextFn next = NULL;
						if (((AGlEventKey*)event)->keyval == XK_Page_Up) {
							dbg(1, "Page Up");
							next = ayyi_tree_path_prev;
						} else {
							dbg(1, "Page Down");
							next = ayyi_tree_path_next;
						}
						if (view->selection->type == AYYI_SELECTION_MULTIPLE) {
							AyyiTreePath* cursor;
							agl_tree_view_get_cursor (view, &cursor, NULL);
							if (!cursor) cursor = ayyi_tree_path_new_first();

							bool shift = ((AGlEventKey*)event)->state & ShiftMask;
							v->extend_selection_pressed = !!shift;
							v->modify_selection_pressed = false;

							AyyiTreePath* new_cursor = NULL;
							for (int i=0;i<10;i++) {
								AyyiTreePath* orig = ayyi_tree_path_copy(cursor);
								next(cursor);
								if ((*iface->get_iter)(model, NULL, cursor)) { // check the cursor is valid
									if (new_cursor) ayyi_tree_path_free(new_cursor);
									new_cursor = ayyi_tree_path_copy(cursor);
									ayyi_tree_path_free(orig);
								} else {
									ayyi_tree_path_free(cursor);
									cursor = orig;
									break;
								}
							}
							if (new_cursor) {
								agl_tree_view_set_cursor(view, cursor, NULL);
								ayyi_tree_selection_select_path(view->selection, cursor);
								ayyi_tree_path_free(new_cursor);
							}

							ayyi_tree_path_free(cursor);
						}
					}
					break;
				case XK_Home:
					dbg(1, "home");
					AyyiTreePath* cursor = ayyi_tree_path_new_first();
					agl_tree_view_set_cursor(view, cursor, NULL);
					ayyi_tree_selection_select_path(view->selection, cursor);
					ayyi_tree_path_free(cursor);
					break;
				case XK_End:
					dbg(1, "end");
					int last = ayyi_tree_model_iter_n_children (model, NULL) - 1;
					cursor = ayyi_tree_path_new_from_indices(last, -1);
					agl_tree_view_set_cursor(view, cursor, NULL);
					ayyi_tree_selection_select_path(view->selection, cursor);
					ayyi_tree_path_free(cursor);
					break;
				case XK_Delete:
					dbg(1, "delete");
					if (view->on_delete_request) {
						AMPromise* promise = am_promise_new(view);
						am_promise_add_callback(promise, _on_delete_request_resolved, promise);
						view->on_delete_request(view, promise);
					}
					break;
				default:
					return AGL_NOT_HANDLED;
			}
			break;
#if 0
		case GDK_KEY_RELEASE:
			dbg(0, "KEY_RELEASE");
			break;
#endif
		default:
			break;
	}
	return AGL_HANDLED;
}


void
agl_tree_view_free (AGlActor* actor)
{
	AGlTreeView* treeview = (AGlTreeView*)actor;

	observable_free (treeview->scroll);

	if(!--instance_count){
	}
}


/*
 *  This is an abstract class, so the 'new' function is never called.
 *
 *  GtkTreeView delegates drawing to cell renderers.
 *  Here, drawing is currently done by the child class.
 */
static AGlActor*
agl_tree_view (AGlActor* _)
{
	return NULL;
}


/*
 *  Value is in rows
 *
 *  This uses the total number of items in the model, so if
 *  the implementing class uses a view filter, it will have to
 *  do its own scroll-window management.
 */
static void
agl_tree_view_on_scroll (Observable* observable, int row, gpointer _view)
{
	AGlActor* actor = _view;
	AGlTreeView* treeview = (AGlTreeView*)actor;

	row = MAX(row, 0);
#if 0
	gint dy = tree_view->priv->dy - row * row_height;
	if (dy) {
		update_prelight (tree_view, tree_view->priv->event_last_x, tree_view->priv->event_last_y - dy);
	}
#endif

	if (treeview->priv->dy != row * row_height) {
		// Update our dy and top_row
		treeview->priv->dy = row * row_height;

#if 0
		if (!tree_view->priv->in_top_row_to_dy)
#else
		if (true)
#endif
			ayyi_tree_view_dy_to_top_row (treeview);
	}
}


void
agl_tree_view_construct (AGlTreeView* view, AyyiTreeModel* model)
{
	_init();

	int max_scroll_position (gpointer _view)
	{
		AGlTreeView* view = _view;
		int n_items = ayyi_tree_model_iter_n_children (view->priv->model, NULL);
		return MAX(0, n_items - agl_actor__height((AGlActor*)view) / row_height + 2);
	}

	view->priv = AGL_NEW(AGlTreeViewPriv,
		.model = model
	);

	view->cursor = observable_new();
	view->cursor->value = -1;

	view->scroll = observable_new ();
	view->scroll->max = max_scroll_position;
	view->scroll->user_data = view;
	observable_subscribe(view->scroll, agl_tree_view_on_scroll, view);

	if(model){
		AyyiTreePath* path = ayyi_tree_path_new_first();
		AyyiTreeIter iter;
		if (ayyi_tree_model_get_iter(view->priv->model, &iter, path)) {
			view->priv->tree = _ayyi_rbtree_new();
			ayyi_tree_view_build_tree (view, view->priv->tree, &iter, 1, FALSE);
		}
		ayyi_tree_path_free (path);

		g_signal_connect (view->priv->model, "row-inserted", G_CALLBACK(ayyi_tree_view_row_inserted), view);
	}
}


/**
 * agl_tree_view_get_model:
 * @tree_view: a #AyyiTreeView
 *
 * Returns the model the #AyyiTreeView is based on.  Returns %NULL if the
 * model is unset.
 *
 * Return value: (transfer none): A #GtkTreeModel, or %NULL if none is currently being used.
 **/
AyyiTreeModel*
agl_tree_view_get_model (AGlTreeView* view)
{
	g_return_val_if_fail (view, NULL);

	return view->priv->model;
}


/**
 * agl_tree_view_set_model:
 * @tree_view: A #GtkTreeNode.
 * @model: (allow-none): The model.
 *
 * Sets the model for a #AyyiTreeView.  If the @tree_view already has a model
 * set, it will remove it before setting the new model.  If @model is %NULL,
 * then it will unset the old model.
 **/
void
agl_tree_view_set_model (AGlTreeView* tree_view, AyyiTreeModel* model)
{
	g_return_if_fail (tree_view);
	g_return_if_fail (AYYI_IS_TREE_MODEL (model));

	if (model == tree_view->priv->model)
		return;

#if 0
	if (tree_view->priv->scroll_to_path) {
		ayyi_tree_row_reference_free0 (tree_view->priv->scroll_to_path);
	}
#endif

	if (tree_view->priv->model) {
#if 0
		GList* tmplist = tree_view->priv->columns;

		ayyi_tree_view_unref_and_check_selection_tree (tree_view, tree_view->priv->tree);

		remove_expand_collapse_timeout (tree_view);

		g_signal_handlers_disconnect_by_func (tree_view->priv->model, ayyi_tree_view_row_changed, tree_view);
#endif
		g_signal_handlers_disconnect_by_func (tree_view->priv->model, ayyi_tree_view_row_inserted, tree_view);
#if 0
		g_signal_handlers_disconnect_by_func (tree_view->priv->model, ayyi_tree_view_row_has_child_toggled, tree_view);
#endif
		g_signal_handlers_disconnect_by_func (tree_view->priv->model, ayyi_tree_view_row_deleted, tree_view);
#if 0
		g_signal_handlers_disconnect_by_func (tree_view->priv->model, ayyi_tree_view_rows_reordered, tree_view);

		for (; tmplist; tmplist = tmplist->next)
			_ayyi_tree_view_column_unset_model (tmplist->data, tree_view->priv->model);
#endif

		if (tree_view->priv->tree)
			agl_tree_view_free_rbtree (tree_view);

#if 0
		ayyi_tree_row_reference_free0 (tree_view->priv->drag_dest_row);
#endif
		ayyi_tree_row_reference_free0 (tree_view->priv->cursor);
		ayyi_tree_row_reference_free0 (tree_view->priv->anchor);
		ayyi_tree_row_reference_free0 (tree_view->priv->top_row);
#if 0
		ayyi_tree_row_reference_free0 (tree_view->priv->scroll_to_path);

		tree_view->priv->scroll_to_column = NULL;
#endif

		g_object_unref (tree_view->priv->model);

#if 0
		tree_view->priv->search_column = -1;
		tree_view->priv->fixed_height_check = 0;
		tree_view->priv->fixed_height = -1;
		tree_view->priv->dy = tree_view->priv->top_row_dy = 0;
		tree_view->priv->last_button_x = -1;
		tree_view->priv->last_button_y = -1;
#endif
	}

	tree_view->priv->model = model;

	if (tree_view->priv->model) {

#if 0
		if (tree_view->priv->search_column == -1) {
			gint i;
			for (i = 0; i < ayyi_tree_model_get_n_columns (model); i++) {
				GType type = ayyi_tree_model_get_column_type (model, i);

				if (g_value_type_transformable (type, G_TYPE_STRING)) {
					tree_view->priv->search_column = i;
					break;
				}
			}
		}
#endif

		g_object_ref (tree_view->priv->model);

#if 0
		g_signal_connect (tree_view->priv->model, "row-changed", G_CALLBACK (ayyi_tree_view_row_changed), tree_view);
#endif
		g_signal_connect (tree_view->priv->model, "row-inserted", G_CALLBACK (ayyi_tree_view_row_inserted), tree_view);
#if 0
		g_signal_connect (tree_view->priv->model, "row-has-child-toggled", G_CALLBACK (ayyi_tree_view_row_has_child_toggled), tree_view);
#endif
		g_signal_connect (tree_view->priv->model, "row-deleted", G_CALLBACK (ayyi_tree_view_row_deleted), tree_view);
#if 0
		g_signal_connect (tree_view->priv->model, "rows-reordered", G_CALLBACK (ayyi_tree_view_rows_reordered), tree_view);
#endif

		AyyiTreeModelFlags flags = ayyi_tree_model_get_flags (tree_view->priv->model);
		if ((flags & AYYI_TREE_MODEL_LIST_ONLY) == AYYI_TREE_MODEL_LIST_ONLY)
			AYYI_TREE_VIEW_SET_FLAG (tree_view, AYYI_TREE_VIEW_IS_LIST);
		else
			AYYI_TREE_VIEW_UNSET_FLAG (tree_view, AYYI_TREE_VIEW_IS_LIST);

		AyyiTreePath* path = ayyi_tree_path_new_first ();
		AyyiTreeIter iter;
		if (ayyi_tree_model_get_iter (tree_view->priv->model, &iter, path)) {
			tree_view->priv->tree = _ayyi_rbtree_new ();
			ayyi_tree_view_build_tree (tree_view, tree_view->priv->tree, &iter, 1, FALSE);
		}
		ayyi_tree_path_free (path);

		/*  FIXME: do I need to do this? ayyi_tree_view_create_buttons (tree_view); */
#if 0
		install_presize_handler (tree_view);
#endif
	}

#if 0 // TODO
	g_object_notify (G_OBJECT (tree_view->priv->proxy), "model");
#endif

	if (!tree_view->selection) {
		AyyiTreeIter iter;
		if(ayyi_tree_model_get_iter_first(model, &iter)){
			ayyi_tree_selection_select_iter(tree_view->selection, &iter);
		}
	}

	if (tree_view->selection)
		_ayyi_tree_selection_emit_changed (tree_view->selection);

	AyyiTreePath* path = ayyi_tree_path_new_first();
	agl_tree_view_set_cursor(tree_view, path, 0);
	ayyi_tree_path_free(path);

	agl_actor__invalidate((AGlActor*)tree_view);
}


static void
agl_tree_view_free_rbtree (AGlTreeView* tree_view)
{
	_ayyi_rbtree_free (tree_view->priv->tree);

	tree_view->priv->tree = NULL;
#if 0
	tree_view->priv->button_pressed_node = NULL;
	tree_view->priv->button_pressed_tree = NULL;
	tree_view->priv->prelight_tree = NULL;
	tree_view->priv->prelight_node = NULL;
	tree_view->priv->expanded_collapsed_node = NULL;
	tree_view->priv->expanded_collapsed_tree = NULL;
#endif
}


/*
 *  Result must be freed
 */
AyyiTreePath*
agl_tree_view_path_at_coord (AGlTreeView* view, int x, int y)
{
	AyyiRBTree* tree;
	AyyiRBNode* node;
	/*int offset = */_ayyi_rbtree_find_offset (view->priv->tree, y, &tree, &node);
	if(node){
		AyyiTreePath* path = _ayyi_tree_view_find_path(view, tree, node);
		return path;
	}
	else pwarn("couldnt find row at y=%i", y);
	return NULL;
}


/*
 *  Returned cursor path must be freed
 */
void
agl_tree_view_get_cursor (AGlTreeView* tree_view, AyyiTreePath** path, AyyiTreeViewColumn** focus_column)
{
	g_return_if_fail (tree_view);

	if (path) {
		if (ayyi_tree_row_reference_valid (tree_view->priv->cursor))
			*path = ayyi_tree_row_reference_get_path (tree_view->priv->cursor);
		else
			*path = NULL;
	}

	if (focus_column) {
		*focus_column = tree_view->priv->focus_column;
	}
}


static void
agl_tree_view_real_set_cursor (AGlTreeView* tree_view, AyyiTreePath* path, gboolean clear_and_select, gboolean clamp_node)
{
	AGlTreeViewPriv* v = tree_view->priv;

	AyyiRBTree* tree = NULL;
	AyyiRBNode* node = NULL;

#if 0
	if (ayyi_tree_row_reference_valid (tree_view->priv->cursor)) {
		AyyiTreePath* cursor_path = ayyi_tree_row_reference_get_path (tree_view->priv->cursor);
//		ayyi_tree_view_queue_draw_path (tree_view, cursor_path, NULL);
		ayyi_tree_path_free (cursor_path);
	}
#endif

	ayyi_tree_row_reference_free0 (v->cursor);

	/*  One cannot set the cursor on a separator. Also, if
	 *  _ayyi_tree_view_find_node returns TRUE, it ran out of tree
	 *  before finding the tree and node belonging to path. The
	 *  path maps to a non-existing path and we will silently bail out.
	 *  We unset tree and node to avoid further processing.
	 */
	if (/*!row_is_separator (tree_view, NULL, path) && */!_ayyi_tree_view_find_node (tree_view, path, &tree, &node)) {
		v->cursor = ayyi_tree_row_reference_new_proxy (v->proxy, v->model, path);
	} else {
		tree = NULL;
		node = NULL;
	}

	if (tree) {
		AyyiRBTree* new_tree = NULL;
		AyyiRBNode* new_node = NULL;

		if (clear_and_select && !v->modify_selection_pressed) {
			AyyiTreeSelectMode mode = 0;

			if (v->modify_selection_pressed)
				mode |= AYYI_TREE_SELECT_MODE_TOGGLE;
			if (tree_view->priv->extend_selection_pressed)
				mode |= AYYI_TREE_SELECT_MODE_EXTEND;

			_ayyi_tree_selection_internal_select_node (tree_view->selection, node, tree, path, mode, FALSE);
		}

		/* We have to re-find tree and node here again, somebody might have
		* cleared the node or the whole tree in the GtkTreeSelection::changed
		* callback. If the nodes differ we bail out here.
		*/
		_ayyi_tree_view_find_node (tree_view, path, &new_tree, &new_node);

		if (tree != new_tree || node != new_node)
			return;

		if (clamp_node) {
			ayyi_tree_view_clamp_node_visible (tree_view, tree, node);
			//_ayyi_tree_view_queue_draw_node (tree_view, tree, node, NULL);
		}
	}

	AyyiTreeIter iter;
	if(ayyi_tree_model_get_iter(v->model, &iter, path)){
		observable_set(tree_view->cursor, GPOINTER_TO_INT(iter.user_data));
	}
}


static void
agl_tree_view_set_cursor_on_cell (AGlTreeView* tree_view, AyyiTreePath* path, AyyiTreeViewColumn* focus_column, AyyiCellRenderer* focus_cell)
{
	g_return_if_fail (path);
#if 0
	g_return_if_fail (focus_column == NULL);
#endif

	if (!tree_view->priv->model)
		return;

	if (focus_cell) {
		g_return_if_fail (focus_column);
#if 0
		g_return_if_fail (AYYI_IS_CELL_RENDERER (focus_cell));
#endif
	}

	/* cancel the current editing, if it exists */
#if 0
	if (tree_view->priv->edited_column && tree_view->priv->edited_column->editable_widget)
		ayyi_tree_view_stop_editing (tree_view, TRUE);
#endif

	agl_tree_view_real_set_cursor (tree_view, path, TRUE, TRUE);

#if 0
	if (focus_column && focus_column->visible) {
		gboolean column_in_tree = FALSE;

		GList* list;
		for (list = tree_view->priv->columns; list; list = list->next)
			if (list->data == focus_column) {
				column_in_tree = TRUE;
				break;
			}
		g_return_if_fail (column_in_tree);
		tree_view->priv->focus_column = focus_column;
		if (focus_cell)
			ayyi_tree_view_column_focus_cell (focus_column, focus_cell);
		if (start_editing)
			ayyi_tree_view_start_editing (tree_view, path);
	}
#endif
}


void
agl_tree_view_set_cursor (AGlTreeView* tree_view, AyyiTreePath* path, AyyiTreeViewColumn* focus_column)
{
	agl_tree_view_set_cursor_on_cell (tree_view, path, focus_column, NULL);
}


AyyiTreePath*
_ayyi_tree_view_find_path (AGlTreeView* tree_view, AyyiRBTree* tree, AyyiRBNode* node)
{
	AyyiTreePath* path = ayyi_tree_path_new ();

	g_return_val_if_fail (node, path);
	g_return_val_if_fail (node != tree->nil, path);

	gint count = 1 + node->left->count;

	AyyiRBNode* last = node;
	AyyiRBNode* tmp_node = node->parent;
	AyyiRBTree* tmp_tree = tree;
	while (tmp_tree) {
		while (tmp_node != tmp_tree->nil) {
			if (tmp_node->right == last)
				count += 1 + tmp_node->left->count;
			last = tmp_node;
			tmp_node = tmp_node->parent;
		}
		ayyi_tree_path_prepend_index (path, count - 1);
		last = tmp_tree->parent_node;
		tmp_tree = tmp_tree->parent_tree;
		if (last) {
			count = 1 + last->left->count;
			tmp_node = last->parent;
		}
	}
	return path;
}


void
_ayyi_tree_view_queue_draw_node (AGlTreeView* tree_view, AyyiRBTree* tree, AyyiRBNode* node, const GdkRectangle* clip_rect)
{
	agl_actor__invalidate((AGlActor*)tree_view);
}


/*  Returns TRUE if we ran out of tree before finding the path. If the path is
 *  invalid (ie. points to a node that's not in the tree), *tree and *node are
 *  both set to NULL.
 */
bool
_ayyi_tree_view_find_node (AGlTreeView* tree_view, AyyiTreePath* path, AyyiRBTree** tree, AyyiRBNode** node)
{
	AyyiRBNode* tmpnode = NULL;
	AyyiRBTree* tmptree = tree_view->priv->tree;
	gint* indices = ayyi_tree_path_get_indices (path);
	gint depth = ayyi_tree_path_get_depth (path);

	*node = NULL;
	*tree = NULL;

	if (depth == 0 || tmptree == NULL)
		return FALSE;

	gint i = 0;
	do {
		tmpnode = _ayyi_rbtree_find_count (tmptree, indices[i] + 1);
		++i;
		if (tmpnode == NULL) {
#ifdef DEBUG
			dbg(0, "node not found: i=%i", indices[0]);
#endif
			*tree = NULL;
			*node = NULL;
			return FALSE;
		}
		if (i >= depth) {
			*tree = tmptree;
			*node = tmpnode;
			return FALSE;
		}
		*tree = tmptree;
		*node = tmpnode;
		tmptree = tmpnode->children;
		if (tmptree == NULL)
			return TRUE;

	} while (1);
}


static void
ayyi_tree_view_build_tree (AGlTreeView* tree_view, AyyiRBTree* tree, AyyiTreeIter* iter, gint depth, gboolean recurse)
{
	AyyiTreeModel* model = tree_view->priv->model;
	AGlActor* actor = (AGlActor*)tree_view;
	AyyiRBNode* temp = NULL;
	AyyiTreePath* path = NULL;
	gboolean is_list = AYYI_TREE_VIEW_FLAG_SET (tree_view, AYYI_TREE_VIEW_IS_LIST);

	do {
		ayyi_tree_model_ref_node (tree_view->priv->model, iter);
#if 0
		temp = _ayyi_rbtree_insert_after (tree, temp, 0, FALSE); // note temp is re-assigned
#else
		temp = _ayyi_rbtree_insert_after (tree, temp, 20, FALSE); // note temp is re-assigned
#endif

		if (tree_view->priv->fixed_height > 0) {
			if (AYYI_RBNODE_FLAG_SET (temp, AYYI_RBNODE_INVALID)) {
				_ayyi_rbtree_node_set_height (tree, temp, tree_view->priv->fixed_height);
				_ayyi_rbtree_node_mark_valid (tree, temp);
			}
		}

		if (is_list)
			continue;

		if (recurse) {
			AyyiTreeIter child;

			if (!path)
				path = ayyi_tree_model_get_path (tree_view->priv->model, iter);
			else
				ayyi_tree_path_next (path);

			if (ayyi_tree_model_iter_children (tree_view->priv->model, &child, iter)) {
				gboolean expand = false;

#if 0
				g_signal_emit (tree_view, tree_view_signals[TEST_EXPAND_ROW], 0, iter, path, &expand);
#endif

				if (ayyi_tree_model_iter_has_child (tree_view->priv->model, iter) && !expand) {
					temp->children = _ayyi_rbtree_new ();
					temp->children->parent_tree = tree;
					temp->children->parent_node = temp;
					ayyi_tree_view_build_tree (tree_view, temp->children, &child, depth + 1, recurse);
				}
			}
		}

		if (ayyi_tree_model_iter_has_child (tree_view->priv->model, iter)) {
			if ((temp->flags & AYYI_RBNODE_IS_PARENT) != AYYI_RBNODE_IS_PARENT)
				temp->flags ^= AYYI_RBNODE_IS_PARENT;
		}
	} while (ayyi_tree_model_iter_next (tree_view->priv->model, iter));

	if (path)
		ayyi_tree_path_free (path);

	int n_items = ayyi_tree_model_iter_n_children (model, NULL);
	actor->scrollable.y2 = actor->scrollable.y1 + (n_items + 1) * row_height;
}


static bool
node_is_visible (AGlTreeView* tree_view, AyyiRBTree* tree, AyyiRBNode* node)
{
	AGlActor* actor = (AGlActor*)tree_view;

	int y = _ayyi_rbtree_node_find_offset (tree, node);
	int height = ROW_HEIGHT (tree_view, AYYI_RBNODE_GET_HEIGHT (node));

	if (y >= -actor->scrollable.y1 && y + height <= -actor->scrollable.y1 + agl_actor__height(actor))
		return TRUE;

	return FALSE;
}


static void
ayyi_tree_view_row_inserted (AyyiTreeModel* model, AyyiTreePath* path, AyyiTreeIter* iter, gpointer data)
{
	AGlTreeView* tree_view = (AGlTreeView*)data;
	AGlActor* actor = (AGlActor*)tree_view;
	AyyiRBNode* tmpnode = NULL;
	gboolean free_path = FALSE;
	gboolean node_visible = TRUE;

	g_return_if_fail (path || iter);

	gint height;
	if (tree_view->priv->fixed_height_mode && tree_view->priv->fixed_height >= 0)
		height = tree_view->priv->fixed_height;
	else
		height = 0;

	if (!path) {
		path = ayyi_tree_model_get_path (model, iter);
		free_path = TRUE;
	}
	else if (!iter)
		ayyi_tree_model_get_iter (model, iter, path);

	if (!tree_view->priv->tree)
		tree_view->priv->tree = _ayyi_rbtree_new ();

	AyyiRBTree* tree;
	AyyiRBTree* tmptree = tree = tree_view->priv->tree;

	/* Update all row-references */
	ayyi_tree_row_reference_inserted (tree_view->priv->proxy, path);
	gint depth = ayyi_tree_path_get_depth (path);
	gint* indices = ayyi_tree_path_get_indices (path);

	/* First, find the parent tree */
	gint i = 0;
	while (i < depth - 1) {
		if (!tmptree) {
			/* We aren't showing the node */
			node_visible = FALSE;
			goto done;
		}

		tmpnode = _ayyi_rbtree_find_count (tmptree, indices[i] + 1);
		if (!tmpnode) {
			g_warning ("A node was inserted with a parent that's not in the tree.\n" \
				"This possibly means that a GtkTreeModel inserted a child node\n" \
				"before the parent was inserted.");
			goto done;
		} else if (!AYYI_RBNODE_FLAG_SET (tmpnode, AYYI_RBNODE_IS_PARENT)) {
			/* FIXME enforce correct behavior on model, probably */
			/* In theory, the model should have emitted has_child_toggled here.  We
			 * try to catch it anyway, just to be safe, in case the model hasn't.
			 */
#if 0
			AyyiTreePath* tmppath = _ayyi_tree_view_find_path (tree_view, tree, tmpnode);
			ayyi_tree_view_row_has_child_toggled (model, tmppath, NULL, data);
			ayyi_tree_path_free (tmppath);
#endif
			goto done;
		}

		tmptree = tmpnode->children;
		tree = tmptree;
		i++;
	}

	if (!tree) {
		node_visible = FALSE;
		goto done;
	}

	/* ref the node */
	ayyi_tree_model_ref_node (tree_view->priv->model, iter);
	if (indices[depth - 1] == 0) {
		tmpnode = _ayyi_rbtree_find_count (tree, 1);
		tmpnode = _ayyi_rbtree_insert_before (tree, tmpnode, height, FALSE);
	} else {
		tmpnode = _ayyi_rbtree_find_count (tree, indices[depth - 1]);
		tmpnode = _ayyi_rbtree_insert_after (tree, tmpnode, height, FALSE);
	}

  done:;
	int n_items = ayyi_tree_model_iter_n_children (model, NULL);
	actor->scrollable.y2 = actor->scrollable.y1 + (n_items + 1) * row_height;

	if (height > 0) {
		if (tree)
			_ayyi_rbtree_node_mark_valid (tree, tmpnode);

		if (node_visible && node_is_visible (tree_view, tree, tmpnode)){
			// not sure if setting size is needed
			agl_actor__set_size((AGlActor*)tree_view); // TODO queue it
			agl_actor__invalidate((AGlActor*)tree_view);
		}
#if 0
		else
			gtk_widget_queue_resize_no_redraw (GTK_WIDGET (tree_view));
#endif
	}
#if 0
	else
		install_presize_handler (tree_view);
#endif

	if (free_path)
		ayyi_tree_path_free (path);
}


static void
check_selection_helper (AyyiRBTree* tree, AyyiRBNode* node, gpointer data)
{
	gint* value = (gint *)data;

	*value = AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED);

	if (node->children && !*value)
		_ayyi_rbtree_traverse (node->children, node->children->root, G_POST_ORDER, check_selection_helper, data);
}


static void
ayyi_tree_view_row_deleted (AyyiTreeModel* model, AyyiTreePath* path, gpointer data)
{
	AGlTreeView* tree_view = data;
	AGlActor* actor = (AGlActor*)tree_view;

	g_return_if_fail (path);

	ayyi_tree_row_reference_deleted (tree_view->priv->proxy, path);

	AyyiRBTree* tree;
	AyyiRBNode* node;
	if (_ayyi_tree_view_find_node (tree_view, path, &tree, &node))
		return;

	if (!tree)
		return;

	/* check if the selection has been changed */
	gint selection_changed = FALSE;
	_ayyi_rbtree_traverse (tree, node, G_POST_ORDER, check_selection_helper, &selection_changed);

#if 0
	GList* list;
	for (list = tree_view->priv->columns; list; list = list->next)
		if (((AyyiTreeViewColumn *)list->data)->visible && ((AyyiTreeViewColumn*)list->data)->column_type == AYYI_TREE_VIEW_COLUMN_AUTOSIZE)
			_ayyi_tree_view_column_cell_set_dirty ((AyyiTreeViewColumn *)list->data, TRUE);

	/* Ensure we don't have a dangling pointer to a dead node */
	ensure_unprelighted (tree_view);

	/* Cancel editting if we've started */
	ayyi_tree_view_stop_editing (tree_view, TRUE);

	/* If we have a node expanded/collapsed timeout, remove it */
	remove_expand_collapse_timeout (tree_view);

	if (tree_view->priv->destroy_count_func) {
		gint child_count = 0;
		if (node->children)
			_ayyi_rbtree_traverse (node->children, node->children->root, G_POST_ORDER, count_children_helper, &child_count);
		tree_view->priv->destroy_count_func (tree_view, path, child_count, tree_view->priv->destroy_count_data);
	}
#endif

	if (tree->root->count == 1) {
		if (tree_view->priv->tree == tree)
		tree_view->priv->tree = NULL;

		_ayyi_rbtree_remove (tree);
	} else {
		_ayyi_rbtree_remove_node (tree, node);
	}

	if (! ayyi_tree_row_reference_valid (tree_view->priv->top_row)) {
		ayyi_tree_row_reference_free0 (tree_view->priv->top_row);
	}

#if 0
	install_scroll_sync_handler(tree_view);
#endif

	int n_items = ayyi_tree_model_iter_n_children (model, NULL);
	actor->scrollable.y2 = actor->scrollable.y1 + (n_items + 1) * row_height;

	agl_actor__invalidate((AGlActor*)tree_view);

	if (selection_changed)
		g_signal_emit_by_name (tree_view->selection, "changed");
}


/* Returns TRUE if it updated the size
 */
static bool
validate_row (AGlTreeView* tree_view, AyyiRBTree* tree, AyyiRBNode* node, AyyiTreeIter* iter, AyyiTreePath* path)
{
	gint height = row_height;
	bool retval = FALSE;
#if 0
	GList* list;
	gint focus_line_width;
	gint depth = ayyi_tree_path_get_depth (path);
#endif

	/* double check the row needs validating */
	if (! AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_INVALID) && ! AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_COLUMN_INVALID))
		return FALSE;

#if 0
	gboolean is_separator = row_is_separator (tree_view, iter, NULL);
#endif

#if 0
	gint horizontal_separator;
	gint vertical_separator;
	gint focus_pad;
	gint grid_line_width;
	gboolean wide_separators;
	gint separator_height;
	gtk_widget_style_get (GTK_WIDGET (tree_view),
		"focus-padding", &focus_pad,
		"focus-line-width", &focus_line_width,
		"horizontal-separator", &horizontal_separator,
		"vertical-separator", &vertical_separator,
		"grid-line-width", &grid_line_width,
		"wide-separators",  &wide_separators,
		"separator-height", &separator_height,
		NULL);
#endif

#if 0
	bool draw_vgrid_lines = tree_view->priv->grid_lines == AYYI_TREE_VIEW_GRID_LINES_VERTICAL || tree_view->priv->grid_lines == AYYI_TREE_VIEW_GRID_LINES_BOTH;
	bool draw_hgrid_lines = tree_view->priv->grid_lines == AYYI_TREE_VIEW_GRID_LINES_HORIZONTAL || tree_view->priv->grid_lines == AYYI_TREE_VIEW_GRID_LINES_BOTH;
#endif

#if 0
	GList *first_column, *last_column;
	for (last_column = g_list_last (tree_view->priv->columns);
		last_column && !(GTK_TREE_VIEW_COLUMN (last_column->data)->visible);
		last_column = last_column->prev)
		;

	for (first_column = g_list_first (tree_view->priv->columns);
		first_column && !(GTK_TREE_VIEW_COLUMN (first_column->data)->visible);
		first_column = first_column->next)
		;

	for (list = tree_view->priv->columns; list; list = list->next) {
		gint tmp_width;
		gint tmp_height;

		AyyiTreeViewColumn* column = list->data;

		if (! column->visible)
			continue;

		if (AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_COLUMN_INVALID) && !column->dirty)
			continue;

		ayyi_tree_view_column_cell_set_cell_data (column, tree_view->priv->model, iter, GTK_RBNODE_FLAG_SET (node, GTK_RBNODE_IS_PARENT), node->children?TRUE:FALSE);
		ayyi_tree_view_column_cell_get_size (column, NULL, NULL, NULL, &tmp_width, &tmp_height);

		if (!is_separator) {
			tmp_height += vertical_separator;
			height = MAX (height, tmp_height);
			height = MAX (height, tree_view->priv->expander_size);
		} else {
			if (wide_separators)
				height = separator_height + 2 * focus_pad;
			else
				height = 2 + 2 * focus_pad;
		}

		if (ayyi_tree_view_is_expander_column (tree_view, column)) {
			tmp_width = tmp_width + horizontal_separator + (depth - 1) * tree_view->priv->level_indentation;

			if (TREE_VIEW_DRAW_EXPANDERS (tree_view))
				tmp_width += depth * tree_view->priv->expander_size;
			}
			else
				tmp_width = tmp_width + horizontal_separator;

			if (draw_vgrid_lines) {
			if (list->data == first_column || list->data == last_column)
				tmp_width += grid_line_width / 2.0;
			else
				tmp_width += grid_line_width;
		}

		if (tmp_width > column->requested_width) {
			retval = TRUE;
			column->requested_width = tmp_width;
		}
	}

	if (draw_hgrid_lines)
		height += grid_line_width;
#endif

	if (height != AYYI_RBNODE_GET_HEIGHT (node)) {
		retval = TRUE;
		_ayyi_rbtree_node_set_height (tree, node, height);
	}
	_ayyi_rbtree_node_mark_valid (tree, node);
	tree_view->priv->post_validation_flag = TRUE;

	return retval;
}


static void
initialize_fixed_height_mode (AGlTreeView* tree_view)
{
	if (!tree_view->priv->tree)
		return;

	if (tree_view->priv->fixed_height < 0) {
		AyyiTreeIter iter;

		AyyiRBTree* tree = tree_view->priv->tree;
		AyyiRBNode* node = tree->root;

		AyyiTreePath* path = _ayyi_tree_view_find_path (tree_view, tree, node);
		ayyi_tree_model_get_iter (tree_view->priv->model, &iter, path);

		validate_row (tree_view, tree, node, &iter, path);

		ayyi_tree_path_free (path);

		tree_view->priv->fixed_height = ROW_HEIGHT (tree_view, AYYI_RBNODE_GET_HEIGHT (node));
	}

	_ayyi_rbtree_set_fixed_height (tree_view->priv->tree, tree_view->priv->fixed_height, TRUE);
}


/**
 * ayyi_tree_view_set_fixed_height_mode:
 * @tree_view: a #AyyiTreeView 
 * @enable: %TRUE to enable fixed height mode
 * 
 * Enables or disables the fixed height mode of @tree_view. 
 * Fixed height mode speeds up #AyyiTreeView by assuming that all 
 * rows have the same height. 
 * Only enable this option if all rows are the same height and all
 * columns are of type %GTK_TREE_VIEW_COLUMN_FIXED.
 *
 * Since: 2.6 
 **/
void
agl_tree_view_set_fixed_height_mode (AGlTreeView* tree_view, gboolean enable)
{
	enable = enable != FALSE;

	if (enable == tree_view->priv->fixed_height_mode)
		return;

	if (!enable) {
		tree_view->priv->fixed_height_mode = 0;
		tree_view->priv->fixed_height = -1;

		/* force a revalidation */
#if 0
		install_presize_handler (tree_view);
#endif
	} else {
		/* make sure all columns are of type FIXED */
#if 0
		GList* l = tree_view->priv->columns;
		for (;l;l=l->next) {
			AyyiTreeViewColumn *c = l->data;

			g_return_if_fail (ayyi_tree_view_column_get_sizing (c) == AYYI_TREE_VIEW_COLUMN_FIXED);
		}

		// Yes, we really have to do this is in a separate loop */
		for (l = tree_view->priv->columns; l; l = l->next)
			g_signal_connect (l->data, "notify::sizing", G_CALLBACK (column_sizing_notify), tree_view);
#endif

		tree_view->priv->fixed_height_mode = 1;
		tree_view->priv->fixed_height = -1;

		if (tree_view->priv->tree)
			initialize_fixed_height_mode (tree_view);
																else pwarn("no tree yet, not initialising fixed height");
	}

#if 0
	if(tree_view->priv->proxy) g_object_notify (G_OBJECT (tree_view->priv->proxy), "fixed-height-mode");
#endif
}


static void
ayyi_tree_view_set_top_row (AGlTreeView* tree_view, AyyiTreePath* path, gint offset)
{
	ayyi_tree_row_reference_free (tree_view->priv->top_row);

	if (!path) {
		tree_view->priv->top_row = NULL;
		tree_view->priv->top_row_dy = 0;
	} else {
		tree_view->priv->top_row = ayyi_tree_row_reference_new_proxy (tree_view->priv->proxy, tree_view->priv->model, path);
		tree_view->priv->top_row_dy = offset;
	}
}


/* Always call this if dy is in the visible range. If the tree is empty, then
 * it's set to be NULL, and top_row_dy is 0;
 */
static void
ayyi_tree_view_dy_to_top_row (AGlTreeView* tree_view)
{
	if (!tree_view->priv->tree) {
		ayyi_tree_view_set_top_row (tree_view, NULL, 0);
	} else {
		AyyiRBTree* tree;
		AyyiRBNode* node;
		gint offset = _ayyi_rbtree_find_offset (tree_view->priv->tree, tree_view->priv->dy, &tree, &node);

		if (tree == NULL) {
			ayyi_tree_view_set_top_row (tree_view, NULL, 0);
		} else {
			AyyiTreePath* path = _ayyi_tree_view_find_path (tree_view, tree, node);
			ayyi_tree_view_set_top_row (tree_view, path, offset);
			ayyi_tree_path_free (path);
		}
	}
}


static void
ayyi_tree_view_top_row_to_dy (AGlTreeView* tree_view)
{
	AyyiTreePath *path;
	AyyiRBTree *tree;
	AyyiRBNode *node;
	int new_dy;

	/* Avoid recursive calls */
#if 0
	if (tree_view->priv->in_top_row_to_dy)
		return;
#endif

	if (tree_view->priv->top_row)
		path = ayyi_tree_row_reference_get_path (tree_view->priv->top_row);
	else
		path = NULL;

	if (!path)
		tree = NULL;
	else
		_ayyi_tree_view_find_node (tree_view, path, &tree, &node);

	if (path)
		ayyi_tree_path_free (path);

	if (!tree) {
		/* keep dy and set new toprow */
		ayyi_tree_row_reference_free0 (tree_view->priv->top_row);
		tree_view->priv->top_row_dy = 0;
		/* DO NOT install the idle handler */
		ayyi_tree_view_dy_to_top_row (tree_view);
		return;
	}

	if (ROW_HEIGHT (tree_view, BACKGROUND_HEIGHT (node)) < tree_view->priv->top_row_dy) {
		/* new top row -- do NOT install the idle handler */
		ayyi_tree_view_dy_to_top_row (tree_view);
		return;
	}

	new_dy = _ayyi_rbtree_node_find_offset (tree, node);
	new_dy += tree_view->priv->top_row_dy;

#if 0
	if (new_dy + tree_view->priv->vadjustment->page_size > tree_view->priv->height)
		new_dy = tree_view->priv->height - tree_view->priv->vadjustment->page_size;
#endif

	new_dy = MAX (0, new_dy);

#if 0
	tree_view->priv->in_top_row_to_dy = TRUE;
	gtk_adjustment_set_value (tree_view->priv->vadjustment, (gdouble)new_dy);
	tree_view->priv->in_top_row_to_dy = FALSE;
#endif
}


static bool
agl_tree_view_real_select_cursor_row (AGlTreeView* tree_view, bool start_editing)
{
	AyyiRBTree* new_tree = NULL;
	AyyiRBNode* new_node = NULL;
	AyyiRBTree* cursor_tree = NULL;
	AyyiRBNode* cursor_node = NULL;
	AyyiTreePath* cursor_path = NULL;
	AyyiTreeSelectMode mode = 0;

	//if (!gtk_widget_has_focus (GTK_WIDGET (tree_view))) return FALSE;

	if (tree_view->priv->cursor)
		cursor_path = ayyi_tree_row_reference_get_path (tree_view->priv->cursor);

	if (!cursor_path)
		return FALSE;

	_ayyi_tree_view_find_node (tree_view, cursor_path, &cursor_tree, &cursor_node);

	if (!cursor_tree) {
		ayyi_tree_path_free (cursor_path);
		return FALSE;
	}

#if 0
	if (!tree_view->priv->extend_selection_pressed && start_editing && tree_view->priv->focus_column) {
		if (ayyi_tree_view_start_editing (tree_view, cursor_path)) {
			ayyi_tree_path_free (cursor_path);
			return TRUE;
		}
	}
#endif

	if (tree_view->priv->modify_selection_pressed)
		mode |= AYYI_TREE_SELECT_MODE_TOGGLE;
	if (tree_view->priv->extend_selection_pressed)
		mode |= AYYI_TREE_SELECT_MODE_EXTEND;

	_ayyi_tree_selection_internal_select_node (tree_view->selection, cursor_node, cursor_tree, cursor_path, mode, FALSE);

	/* We bail out if the original (tree, node) don't exist anymore after
	 * handling the selection-changed callback.  We do return TRUE because
	 * the key press has been handled at this point.
	 */
	_ayyi_tree_view_find_node (tree_view, cursor_path, &new_tree, &new_node);

	if (cursor_tree != new_tree || cursor_node != new_node)
		return FALSE;

	ayyi_tree_view_clamp_node_visible (tree_view, cursor_tree, cursor_node);

	//gtk_widget_grab_focus (GTK_WIDGET (tree_view));
	//_ayyi_tree_view_queue_draw_node (tree_view, cursor_tree, cursor_node, NULL);

	if (!tree_view->priv->extend_selection_pressed)
#if 0 // TODO
	ayyi_tree_view_row_activated (tree_view, cursor_path, tree_view->priv->focus_column);
#endif

	ayyi_tree_path_free (cursor_path);

	return TRUE;
}


static bool
agl_tree_view_real_toggle_cursor_row (AGlTreeView* tree_view)
{
	AyyiRBTree* new_tree = NULL;
	AyyiRBNode* new_node = NULL;
	AyyiRBTree* cursor_tree = NULL;
	AyyiRBNode* cursor_node = NULL;
	AyyiTreePath* cursor_path = NULL;

#if 0
	if (!gtk_widget_has_focus (GTK_WIDGET (tree_view)))
		return FALSE;
#endif

	cursor_path = NULL;
	if (tree_view->priv->cursor)
		cursor_path = ayyi_tree_row_reference_get_path (tree_view->priv->cursor);

	if (cursor_path == NULL)
		return FALSE;

	_ayyi_tree_view_find_node (tree_view, cursor_path, &cursor_tree, &cursor_node);
	if (cursor_tree == NULL) {
		ayyi_tree_path_free (cursor_path);
		return FALSE;
	}

	_ayyi_tree_selection_internal_select_node (tree_view->selection, cursor_node, cursor_tree, cursor_path, AYYI_TREE_SELECT_MODE_TOGGLE, FALSE);

	/* We bail out if the original (tree, node) don't exist anymore after
	* handling the selection-changed callback.  We do return TRUE because
	* the key press has been handled at this point.
	*/
	_ayyi_tree_view_find_node (tree_view, cursor_path, &new_tree, &new_node);

	if (cursor_tree != new_tree || cursor_node != new_node)
		return FALSE;

	ayyi_tree_view_clamp_node_visible (tree_view, cursor_tree, cursor_node);

#if 0
	gtk_widget_grab_focus (GTK_WIDGET (tree_view));
	ayyi_tree_view_queue_draw_path (tree_view, cursor_path, NULL);
#endif
	ayyi_tree_path_free (cursor_path);

	return TRUE;
}


/* Make sure the node is visible vertically */
static void
ayyi_tree_view_clamp_node_visible (AGlTreeView* tree_view, AyyiRBTree* tree, AyyiRBNode* node)
{
	// this will fail if scroll_offset is not set correctly

	/* just return if the node is visible, avoiding a costly expose */
	gint node_dy = _ayyi_rbtree_node_find_offset (tree, node);
	gint height = ROW_HEIGHT (tree_view, AYYI_RBNODE_GET_HEIGHT (node));
	if (
			!AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_INVALID)
			&& node_dy >= tree_view->scroll->value * row_height
			&& node_dy + height <= (tree_view->scroll->value * row_height + agl_actor__height((AGlActor*)tree_view)) - 20
	)
		return;

	AyyiTreePath* path = _ayyi_tree_view_find_path (tree_view, tree, node);
	if (path) {
		ayyi_tree_view_scroll_to_cell (tree_view, path, NULL, FALSE, 0.0, 0.0);
		ayyi_tree_path_free (path);
	}
}


/**
 * ayyi_tree_view_scroll_to_point:
 * @tree_view: a #AyyiTreeView
 * @tree_x: X coordinate of new top-left pixel of visible area, or -1
 * @tree_y: Y coordinate of new top-left pixel of visible area, or -1
 *
 * Scrolls the tree view such that the top-left corner of the visible
 * area is @tree_x, @tree_y, where @tree_x and @tree_y are specified
 * in tree coordinates.  The @tree_view must be realized before
 * this function is called.  If it isn't, you probably want to be
 * using ayyi_tree_view_scroll_to_cell().
 *
 * If either @tree_x or @tree_y are -1, then that direction isn't scrolled.
 **/
void
ayyi_tree_view_scroll_to_point (AGlTreeView* tree_view, gint tree_x, gint tree_y)
{
	g_return_if_fail (tree_view);

	if (tree_y != -1){
		agl_actor__invalidate((AGlActor*)tree_view);
		observable_set(tree_view->scroll, tree_y / row_height);
	}
}


/**
 * ayyi_tree_view_scroll_to_cell:
 * @tree_view: A #AyyiTreeView.
 * @path: (allow-none): The path of the row to move to, or %NULL.
 * @column: (allow-none): The #AyyiTreeViewColumn to move horizontally to, or %NULL.
 * @use_align: whether to use alignment arguments, or %FALSE.
 * @row_align: The vertical alignment of the row specified by @path.
 * @col_align: The horizontal alignment of the column specified by @column.
 *
 * Moves the alignments of @tree_view to the position specified by @column and
 * @path.  If @column is %NULL, then no horizontal scrolling occurs.  Likewise,
 * if @path is %NULL no vertical scrolling occurs.  At a minimum, one of @column
 * or @path need to be non-%NULL.  @row_align determines where the row is
 * placed, and @col_align determines where @column is placed.  Both are expected
 * to be between 0.0 and 1.0. 0.0 means left/top alignment, 1.0 means
 * right/bottom alignment, 0.5 means center.
 *
 * If @use_align is %FALSE, then the alignment arguments are ignored, and the
 * tree does the minimum amount of work to scroll the cell onto the screen.
 * This means that the cell will be scrolled to the edge closest to its current
 * position.  If the cell is currently visible on the screen, nothing is done.
 *
 * This function only works if the model is set, and @path is a valid row on the
 * model.  If the model changes before the @tree_view is realized, the centered
 * path will be modified to reflect this change.
 **/
void
ayyi_tree_view_scroll_to_cell (AGlTreeView* tree_view, AyyiTreePath* path, AyyiTreeViewColumn* column, gboolean use_align, gfloat row_align, gfloat col_align)
{
	g_return_if_fail (tree_view);
	g_return_if_fail (tree_view->priv->model);
	g_return_if_fail (tree_view->priv->tree);
	g_return_if_fail (row_align >= 0.0 && row_align <= 1.0);
	g_return_if_fail (col_align >= 0.0 && col_align <= 1.0);
	g_return_if_fail (path || column);

#if 0 // leaks
	dbg(1, "path=%s use_align: %d row_align: %f", ayyi_tree_path_to_string (path), use_align, row_align);
#endif

	row_align = CLAMP (row_align, 0.0, 1.0);
#if 0
	col_align = CLAMP (col_align, 0.0, 1.0);


	/* Note: Despite the benefits that come from having one code path for the
	 * scrolling code, we short-circuit validate_visible_area's immplementation as
	 * it is much slower than just going to the point.
	 */
	if (/*!gtk_widget_get_visible (GTK_WIDGET (tree_view)) || !gtk_widget_get_realized (GTK_WIDGET (tree_view)) || GTK_WIDGET_ALLOC_NEEDED (tree_view) || */AYYI_RBNODE_FLAG_SET (tree_view->priv->tree->root, AYYI_RBNODE_DESCENDANTS_INVALID)) {
		if (tree_view->priv->scroll_to_path)
			ayyi_tree_row_reference_free (tree_view->priv->scroll_to_path);

		tree_view->priv->scroll_to_path = NULL;
		tree_view->priv->scroll_to_column = NULL;

		if (path)
			tree_view->priv->scroll_to_path = ayyi_tree_row_reference_new_proxy (G_OBJECT (tree_view), tree_view->priv->model, path);
		if (column)
			tree_view->priv->scroll_to_column = column;
		tree_view->priv->scroll_to_use_align = use_align;
		tree_view->priv->scroll_to_row_align = row_align;
		tree_view->priv->scroll_to_col_align = col_align;

		install_presize_handler (tree_view);
	} else {
#endif
		GdkRectangle cell_rect;
		ayyi_tree_view_get_background_area (tree_view, path, column, &cell_rect);

#if 0
		GdkRectangle vis_rect;
		ayyi_tree_view_get_visible_rect (tree_view, &vis_rect);

		cell_rect.y = TREE_WINDOW_Y_TO_RBTREE_Y (tree_view, cell_rect.y);

		gint dest_x = vis_rect.x;
		gint dest_y = vis_rect.y;

		if (column) {
			if (use_align) {
				dest_x = cell_rect.x - ((vis_rect.width - cell_rect.width) * col_align);
			} else {
				if (cell_rect.x < vis_rect.x)
					dest_x = cell_rect.x;
				if (cell_rect.x + cell_rect.width > vis_rect.x + vis_rect.width)
					dest_x = cell_rect.x + cell_rect.width - vis_rect.width;
			}
		}

		if (path) {
			if (use_align) {
				dest_y = cell_rect.y - ((vis_rect.height - cell_rect.height) * row_align);
				dest_y = MAX (dest_y, 0);
			} else {
				if (cell_rect.y < vis_rect.y)
					dest_y = cell_rect.y;
				if (cell_rect.y + cell_rect.height > vis_rect.y + vis_rect.height)
					dest_y = cell_rect.y + cell_rect.height - vis_rect.height;
			}
		}
#endif
		gint dest_x = 0;
		gint dest_y = TREE_WINDOW_Y_TO_RBTREE_Y (tree_view, cell_rect.y);

		ayyi_tree_view_scroll_to_point (tree_view, dest_x, dest_y);
#if 0
	}
#endif
}


/**
 * ayyi_tree_view_get_background_area:
 * @tree_view: a #AyyiTreeView
 * @path: (allow-none): a #GtkTreePath for the row, or %NULL to get only horizontal coordinates
 * @column: (allow-none): a #AyyiTreeViewColumn for the column, or %NULL to get only vertical coordiantes
 * @rect: (out): rectangle to fill with cell background rect
 *
 * Fills the bounding rectangle in bin_window coordinates for the cell at the
 * row specified by @path and the column specified by @column.  If @path is
 * %NULL, or points to a node not found in the tree, the @y and @height fields of
 * the rectangle will be filled with 0. If @column is %NULL, the @x and @width
 * fields will be filled with 0.  The returned rectangle is equivalent to the
 * @background_area passed to gtk_cell_renderer_render().  These background
 * areas tile to cover the entire bin window.  Contrast with the @cell_area,
 * returned by ayyi_tree_view_get_cell_area(), which returns only the cell
 * itself, excluding surrounding borders and the tree expander area.
 *
 **/
void
ayyi_tree_view_get_background_area (AGlTreeView* tree_view, AyyiTreePath* path, AyyiTreeViewColumn* column, GdkRectangle* rect)
{
	AyyiRBTree* tree = NULL;
	AyyiRBNode* node = NULL;

	g_return_if_fail (tree_view);
#if 0
	g_return_if_fail (column == NULL || AYYI_IS_TREE_VIEW_COLUMN (column));
#endif
	g_return_if_fail (rect);

	rect->x = 0;
	rect->y = 0;
	rect->width = 0;
	rect->height = 0;

	if (path) {
		/* Get vertical coords */

		if (!_ayyi_tree_view_find_node (tree_view, path, &tree, &node) && tree == NULL)
			return;

		rect->y = BACKGROUND_FIRST_PIXEL (tree_view, tree, node);

		rect->height = ROW_HEIGHT (tree_view, BACKGROUND_HEIGHT (node));
	}

#if 0
	if (column) {
		gint x2 = 0;

		ayyi_tree_view_get_background_xrange (tree_view, tree, column, &rect->x, &x2);
		rect->width = x2 - rect->x;
	}
#endif
}

