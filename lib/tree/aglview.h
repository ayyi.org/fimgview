/*
 +----------------------------------------------------------------------+
 | This file is part of Samplecat. https://ayyi.github.io/samplecat/    |
 | copyright (C) 2016-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 +----------------------------------------------------------------------+
 | AGlTreeView                                                          |
 | AGlTreeView is an abstract class                                     |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

typedef struct _AGlTreeView AGlTreeView;
typedef struct _AGlTreeViewPrivate AGlTreeViewPriv;

#include "agl/actor.h"
#include "promise/promise.h"
#include "tree/model.h"
typedef bool (*AyyiTreeViewRowSeparatorFunc) (AyyiTreeModel*, AyyiTreeIter*, gpointer);
#include "tree/selection.h"
#include "tree/rbtree.h"
#include "tree/viewcolumn.h"
#include "tree/observable.h"
#include "behaviours/style.h"

#define tree_row_height LINE_HEIGHT

typedef void (*AGlTreeViewDeleteRequest) (AGlTreeView*, AMPromise*);


struct _AGlTreeView {
   AGlActor           actor;
   Observable*        cursor;               // TODO combine with v->cursor (make the observable a RowReference, instead of int)
   AyyiTreeSelection* selection;
   Observable*        scroll;               // uses row-number. If you need pixels, use actor->scrollable.y1
                                            // see also tree_view->priv->top_row and tree_view->priv->dy
   AGlTreeViewDeleteRequest
                      on_delete_request;
   bool               lock_selection;       // disable selection/cursor changes
   AGlTreeViewPriv*   priv;
};

AGlActorClass*
        agl_tree_view_get_class             ();

void    agl_tree_view_construct             (AGlTreeView*, AyyiTreeModel*);

// AGlActor class methods
void    agl_tree_view_init                  (AGlActor*);
void    agl_tree_view_free                  (AGlActor*);
void    agl_tree_view_set_size              (AGlActor*);
bool    agl_tree_view_event                 (AGlActor*, AGlEvent*, AGliPt);

AyyiTreeModel*
        agl_tree_view_get_model             (AGlTreeView*);
void    agl_tree_view_set_model             (AGlTreeView*, AyyiTreeModel*);
AyyiTreePath*
        agl_tree_view_path_at_coord         (AGlTreeView*, int x, int y);
void    agl_tree_view_get_cursor            (AGlTreeView*, AyyiTreePath**, AyyiTreeViewColumn** focus_column);
void    agl_tree_view_set_cursor            (AGlTreeView*, AyyiTreePath*, AyyiTreeViewColumn* focus_column);

void    agl_tree_view_set_fixed_height_mode (AGlTreeView*, gboolean enable);
