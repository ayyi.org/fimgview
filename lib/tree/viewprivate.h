/* treeprivate.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __ayyi_tree_private_h__
#define __ayyi_tree_private_h__

G_BEGIN_DECLS

#include "tree/aglview.h"
#include "tree/rbtree.h"
#include "tree/viewcolumn.h"

#define TREE_VIEW_DRAG_WIDTH 6

typedef enum
{
  AYYI_TREE_VIEW_IS_LIST = 1 << 0,
  AYYI_TREE_VIEW_SHOW_EXPANDERS = 1 << 1,
  AYYI_TREE_VIEW_IN_COLUMN_RESIZE = 1 << 2,
  AYYI_TREE_VIEW_ARROW_PRELIT = 1 << 3,
  AYYI_TREE_VIEW_HEADERS_VISIBLE = 1 << 4,
  AYYI_TREE_VIEW_DRAW_KEYFOCUS = 1 << 5,
  AYYI_TREE_VIEW_MODEL_SETUP = 1 << 6,
  AYYI_TREE_VIEW_IN_COLUMN_DRAG = 1 << 7
} AyyiTreeViewFlags;

typedef enum
{
  AYYI_TREE_SELECT_MODE_TOGGLE = 1 << 0,
  AYYI_TREE_SELECT_MODE_EXTEND = 1 << 1
}
AyyiTreeSelectMode;

enum
{
  DRAG_COLUMN_WINDOW_STATE_UNSET = 0,
  DRAG_COLUMN_WINDOW_STATE_ORIGINAL = 1,
  DRAG_COLUMN_WINDOW_STATE_ARROW = 2,
  DRAG_COLUMN_WINDOW_STATE_ARROW_LEFT = 3,
  DRAG_COLUMN_WINDOW_STATE_ARROW_RIGHT = 4
};

enum
{
  RUBBER_BAND_OFF = 0,
  RUBBER_BAND_MAYBE_START = 1,
  RUBBER_BAND_ACTIVE = 2
};

#define AYYI_TREE_VIEW_SET_FLAG(tree_view, flag)   G_STMT_START{ (tree_view->priv->flags|=flag); }G_STMT_END
#define AYYI_TREE_VIEW_UNSET_FLAG(tree_view, flag) G_STMT_START{ (tree_view->priv->flags&=~(flag)); }G_STMT_END
#define AYYI_TREE_VIEW_FLAG_SET(tree_view, flag)   ((tree_view->priv->flags&flag)==flag)
#define TREE_VIEW_HEADER_HEIGHT(tree_view)        (AYYI_TREE_VIEW_FLAG_SET (tree_view, AYYI_TREE_VIEW_HEADERS_VISIBLE)?tree_view->priv->header_height:0)
#define TREE_VIEW_COLUMN_REQUESTED_WIDTH(column)  (CLAMP (column->requested_width, (column->min_width!=-1)?column->min_width:column->requested_width, (column->max_width!=-1)?column->max_width:column->requested_width))
#define TREE_VIEW_DRAW_EXPANDERS(tree_view)       (!AYYI_TREE_VIEW_FLAG_SET (tree_view, AYYI_TREE_VIEW_IS_LIST)&&AYYI_TREE_VIEW_FLAG_SET (tree_view, AYYI_TREE_VIEW_SHOW_EXPANDERS))

 /* This lovely little value is used to determine how far away from the title bar
  * you can move the mouse and still have a column drag work.
  */
#define TREE_VIEW_COLUMN_DRAG_DEAD_MULTIPLIER(tree_view) (10*TREE_VIEW_HEADER_HEIGHT(tree_view))

typedef struct _AyyiTreeViewColumnReorder AyyiTreeViewColumnReorder;
struct _AyyiTreeViewColumnReorder
{
  gint left_align;
  gint right_align;
  AyyiTreeViewColumn *left_column;
  AyyiTreeViewColumn *right_column;
};

struct _AGlTreeViewPrivate
{
   AyyiTreeModel* model;

   guint flags;
   /* tree information */
   AyyiRBTree* tree;

#if 0
   /* Adjustments */
   AyyiAdjustment *hadjustment;
   AyyiAdjustment *vadjustment;

   /* Sub windows */
   GdkWindow *bin_window;
   GdkWindow *header_window;

#endif
   /* Scroll position state keeping */
   AyyiTreeRowReference* top_row;
   gint top_row_dy;
   /* dy == y pos of top_row + top_row_dy */
   /* we cache it for simplicity of the code */
   gint dy;

#if 0
   guint presize_handler_timer;
   guint validate_rows_timer;
   guint scroll_sync_timer;

   /* Indentation and expander layout */
   gint expander_size;
   AyyiTreeViewColumn *expander_column;

   gint level_indentation;

   /* Key navigation (focus), selection */
   gint cursor_offset;
#endif

   AyyiTreeRowReference* anchor;
   AyyiTreeRowReference* cursor;

   AyyiTreeViewColumn* focus_column;

#if 0
   /* Current pressed node, previously pressed, prelight */
   AyyiRBNode *button_pressed_node;
   AyyiRBTree *button_pressed_tree;

   gint pressed_button;
   gint press_start_x;
   gint press_start_y;

   gint event_last_x;
   gint event_last_y;

   guint last_button_time;
   gint last_button_x;
   gint last_button_y;

   AyyiRBNode *prelight_node;
   AyyiRBTree *prelight_tree;

   /* Cell Editing */
   AyyiTreeViewColumn *edited_column;

   /* The node that's currently being collapsed or expanded */
   AyyiRBNode *expanded_collapsed_node;
   AyyiRBTree *expanded_collapsed_tree;
   guint expand_collapse_timeout;

   /* Auto expand/collapse timeout in hover mode */
   guint auto_expand_timeout;
#endif

#if 0
   /* Header information */
   gint n_columns;
   GList *columns;
   gint header_height;

   AyyiTreeViewColumnDropFunc column_drop_func;
   gpointer column_drop_func_data;
   GDestroyNotify column_drop_func_data_destroy;
   GList *column_drag_info;
   AyyiTreeViewColumnReorder *cur_reorder;

   gint prev_width_before_expander;

   /* Interactive Header reordering */
   GdkWindow *drag_window;
   GdkWindow *drag_highlight_window;
   AyyiTreeViewColumn *drag_column;
   gint drag_column_x;

   /* Interactive Header Resizing */
   gint drag_pos;
   gint x_drag;

   /* Non-interactive Header Resizing, expand flag support */
   gint prev_width;

   gint last_extra_space;
   gint last_extra_space_per_column;
   gint last_number_of_expand_columns;

   /* ATK Hack */
   AyyiTreeDestroyCountFunc destroy_count_func;
   gpointer destroy_count_data;
   GDestroyNotify destroy_count_destroy;

   /* Scroll timeout (e.g. during dnd, rubber banding) */
   guint scroll_timeout;

   /* Row drag-and-drop */
   AyyiTreeRowReference *drag_dest_row;
   AyyiTreeViewDropPosition drag_dest_pos;
   guint open_dest_timeout;

   /* Rubber banding */
   gint rubber_band_status;
   gint rubber_band_x;
   gint rubber_band_y;
   gint rubber_band_extend;
   gint rubber_band_modify;

   AyyiRBNode *rubber_band_start_node;
   AyyiRBTree *rubber_band_start_tree;

   AyyiRBNode *rubber_band_end_node;
   AyyiRBTree *rubber_band_end_tree;
#endif

   /* fixed height */
   gint fixed_height;

   /* Scroll-to functionality when unrealized */
#if 0
   AyyiTreeRowReference *scroll_to_path;
   AyyiTreeViewColumn *scroll_to_column;
   gfloat scroll_to_row_align;
   gfloat scroll_to_col_align;

   /* Interactive search */
   gint selected_iter;
   gint search_column;
   AyyiTreeViewSearchPositionFunc search_position_func;
   AyyiTreeViewSearchEqualFunc search_equal_func;
   gpointer search_user_data;
   GDestroyNotify search_destroy;
   gpointer search_position_user_data;
   GDestroyNotify search_position_destroy;
   AyyiWidget *search_window;
   AyyiWidget *search_entry;
   guint search_entry_changed_id;
   guint typeselect_flush_timeout;

   /* Grid and tree lines */
   AyyiTreeViewGridLines grid_lines;
   double grid_line_dashes[2];
   int grid_line_width;

   gboolean tree_lines_enabled;
   double tree_line_dashes[2];
   int tree_line_width;

   /* Row separators */
#endif
   AyyiTreeViewRowSeparatorFunc row_separator_func;
   gpointer row_separator_data;
#if 0
   GDestroyNotify row_separator_destroy;

   /* Tooltip support */
   gint tooltip_column;

   /* Here comes the bitfield */
   guint scroll_to_use_align : 1;
#endif

   guint fixed_height_mode : 1;
   guint fixed_height_check : 1;

#if 0
   guint reorderable : 1;
   guint header_has_focus : 1;
   guint drag_column_window_state : 3;
   /* hint to display rows in alternating colors */
   guint has_rules : 1;
   guint mark_rows_col_dirty : 1;

   /* for DnD */
   guint empty_view_drop : 1;
#endif

   // these flags are set transiently during selection operations to determine behaviour
   guint modify_selection_pressed : 1;
   guint extend_selection_pressed : 1;

#if 0
   guint init_hadjust_value : 1;

   guint in_top_row_to_dy : 1;

   /* interactive search */
   guint enable_search : 1;
   guint disable_popdown : 1;
   guint search_custom_entry_set : 1;
  
   guint hover_selection : 1;
   guint hover_expand : 1;
   guint imcontext_changed : 1;

   guint rubber_banding_enable : 1;

   guint in_grab : 1;
#endif

   guint post_validation_flag : 1;

#if 0
   /* Whether our key press handler is to avoid sending an unhandled binding to the search entry */
   guint search_entry_avoid_unhandled_binding : 1;
#endif

   GObject* proxy;

   AGlActor* scrollbar;
};

#if 0
#ifdef __GNUC__

#define TREE_VIEW_INTERNAL_ASSERT(expr, ret)     G_STMT_START{          \
     if (!(expr))                                                       \
       {                                                                \
         g_log (G_LOG_DOMAIN,                                           \
                G_LOG_LEVEL_CRITICAL,                                   \
		"%s (%s): assertion `%s' failed.\n"                     \
	        "There is a disparity between the internal view of the AyyiTreeView,\n"    \
		"and the AyyiTreeModel.  This generally means that the model has changed\n"\
		"without letting the view know.  Any display from now on is likely to\n"  \
		"be incorrect.\n",                                                        \
                G_STRLOC,                                               \
                G_STRFUNC,                                              \
                #expr);                                                 \
         return ret;                                                    \
       };                               }G_STMT_END

#define TREE_VIEW_INTERNAL_ASSERT_VOID(expr)     G_STMT_START{          \
     if (!(expr))                                                       \
       {                                                                \
         g_log (G_LOG_DOMAIN,                                           \
                G_LOG_LEVEL_CRITICAL,                                   \
		"%s (%s): assertion `%s' failed.\n"                     \
	        "There is a disparity between the internal view of the AyyiTreeView,\n"    \
		"and the AyyiTreeModel.  This generally means that the model has changed\n"\
		"without letting the view know.  Any display from now on is likely to\n"  \
		"be incorrect.\n",                                                        \
                G_STRLOC,                                               \
                G_STRFUNC,                                              \
                #expr);                                                 \
         return;                                                        \
       };                               }G_STMT_END

#else

#define TREE_VIEW_INTERNAL_ASSERT(expr, ret)     G_STMT_START{          \
     if (!(expr))                                                       \
       {                                                                \
         g_log (G_LOG_DOMAIN,                                           \
                G_LOG_LEVEL_CRITICAL,                                   \
		"file %s: line %d: assertion `%s' failed.\n"       \
	        "There is a disparity between the internal view of the AyyiTreeView,\n"    \
		"and the AyyiTreeModel.  This generally means that the model has changed\n"\
		"without letting the view know.  Any display from now on is likely to\n"  \
		"be incorrect.\n",                                                        \
                __FILE__,                                               \
                __LINE__,                                               \
                #expr);                                                 \
         return ret;                                                    \
       };                               }G_STMT_END

#define TREE_VIEW_INTERNAL_ASSERT_VOID(expr)     G_STMT_START{          \
     if (!(expr))                                                       \
       {                                                                \
         g_log (G_LOG_DOMAIN,                                           \
                G_LOG_LEVEL_CRITICAL,                                   \
		"file %s: line %d: assertion '%s' failed.\n"            \
	        "There is a disparity between the internal view of the AyyiTreeView,\n"    \
		"and the AyyiTreeModel.  This generally means that the model has changed\n"\
		"without letting the view know.  Any display from now on is likely to\n"  \
		"be incorrect.\n",                                                        \
                __FILE__,                                               \
                __LINE__,                                               \
                #expr);                                                 \
         return;                                                        \
       };                               }G_STMT_END
#endif
#endif


/* functions that shouldn't be exported */
void          _ayyi_tree_selection_internal_select_node (AyyiTreeSelection*, AyyiRBNode*, AyyiRBTree*, AyyiTreePath*, AyyiTreeSelectMode, gboolean override_browse_mode);
void          _ayyi_tree_selection_emit_changed         (AyyiTreeSelection*);
bool          _ayyi_tree_view_find_node                 (AGlTreeView*, AyyiTreePath*, AyyiRBTree**, AyyiRBNode**);
AyyiTreePath* _ayyi_tree_view_find_path                 (AGlTreeView*, AyyiRBTree*, AyyiRBNode*);
#if 0
void          _ayyi_tree_view_child_move_resize         (AGlTreeView*, AyyiWidget*, gint x, gint y, gint width, gint height);
#endif
void          _ayyi_tree_view_queue_draw_node           (AGlTreeView*, AyyiRBTree*, AyyiRBNode*, const GdkRectangle* clip_rect);

#if 0
void _gtk_tree_view_column_realize_button   (AyyiTreeViewColumn*);
void _gtk_tree_view_column_unrealize_button (AyyiTreeViewColumn*);
void _gtk_tree_view_column_set_tree_view    (AyyiTreeViewColumn*, AyyiTreeView*);
void _gtk_tree_view_column_unset_model      (AyyiTreeViewColumn*, AyyiTreeModel*);
void _gtk_tree_view_column_unset_tree_view  (AyyiTreeViewColumn*);
void _gtk_tree_view_column_set_width        (AyyiTreeViewColumn*, gint width);
void _gtk_tree_view_column_start_drag       (AyyiTreeView*, AyyiTreeViewColumn *column);
bool _gtk_tree_view_column_cell_event       (AyyiTreeViewColumn*, AyyiCellEditable**, GdkEvent*, gchar* path_string, const GdkRectangle* background_area, const GdkRectangle *cell_area, guint flags);
void _gtk_tree_view_column_start_editing    (AyyiTreeViewColumn*, AyyiCellEditable*);
void _gtk_tree_view_column_stop_editing     (AyyiTreeViewColumn*);
void _gtk_tree_view_install_mark_rows_col_dirty (AyyiTreeView*);
void             _gtk_tree_view_column_autosize          (AyyiTreeView*, AyyiTreeViewColumn*);

gboolean          _gtk_tree_view_column_has_editable_cell   (AyyiTreeViewColumn*);
AyyiCellRenderer* _gtk_tree_view_column_get_edited_cell     (AyyiTreeViewColumn*);
gint              _gtk_tree_view_column_count_special_cells (AyyiTreeViewColumn*);
AyyiCellRenderer* _gtk_tree_view_column_get_cell_at_pos     (AyyiTreeViewColumn*, gint x);

#endif
AyyiTreeSelection* _ayyi_tree_selection_new_with_tree_view (AGlTreeView*);
void               _ayyi_tree_selection_set_tree_view      (AyyiTreeSelection*, AGlTreeView*);
gboolean           _ayyi_tree_selection_row_is_selectable  (AyyiTreeSelection*, AyyiRBNode*, AyyiTreePath*);

#if 0
void		  _gtk_tree_view_column_cell_render        (AyyiTreeViewColumn*, GdkWindow*, const GdkRectangle*, const GdkRectangle*, const GdkRectangle*, guint flags);
void		  _gtk_tree_view_column_get_focus_area     (AyyiTreeViewColumn*, const GdkRectangle* background_area, const GdkRectangle* cell_area, GdkRectangle* focus_area);
gboolean	  _gtk_tree_view_column_cell_focus         (AyyiTreeViewColumn*, gint direction, gboolean            left, gboolean            right);
void		  _gtk_tree_view_column_cell_draw_focus    (AyyiTreeViewColumn*, GdkWindow*, const GdkRectangle*, const GdkRectangle*, const GdkRectangle*, guint flags);
void		  _gtk_tree_view_column_cell_set_dirty	   (AyyiTreeViewColumn*, gboolean install_handler);
void          _gtk_tree_view_column_get_neighbor_sizes (AyyiTreeViewColumn*, AyyiCellRenderer*, gint*, gint*);
#endif

G_END_DECLS

#endif

