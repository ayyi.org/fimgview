/* ayyitreeselection.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <string.h>
#include "debug/debug.h"
#include "selection.h"
#include "viewprivate.h"
#include "rbtree.h"
//#include "model.h"
#include "marshalers.h"

static void ayyi_tree_selection_finalize          (GObject*);
#if 0
static gint ayyi_tree_selection_real_select_all   (AyyiTreeSelection*);
#endif
static gint ayyi_tree_selection_real_unselect_all (AyyiTreeSelection*);
static gint ayyi_tree_selection_real_select_node  (AyyiTreeSelection*, AyyiRBTree*, AyyiRBNode*, gboolean select);

enum
{
  CHANGED,
  LAST_SIGNAL
};

static guint tree_selection_signals [LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (AyyiTreeSelection, ayyi_tree_selection, G_TYPE_OBJECT)


static void
ayyi_tree_selection_class_init (AyyiTreeSelectionClass *class)
{
	GObjectClass* object_class = (GObjectClass*) class;

	object_class->finalize = ayyi_tree_selection_finalize;
	class->changed = NULL;

	tree_selection_signals[CHANGED] = g_signal_new ("changed", G_OBJECT_CLASS_TYPE (object_class), G_SIGNAL_RUN_FIRST, G_STRUCT_OFFSET (AyyiTreeSelectionClass, changed), NULL, NULL, _gtk_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
ayyi_tree_selection_init (AyyiTreeSelection *selection)
{
	selection->type = AYYI_SELECTION_SINGLE;
}


static void
ayyi_tree_selection_finalize (GObject* object)
{
	AyyiTreeSelection* selection = AYYI_TREE_SELECTION (object);

	if (selection->destroy) {
		GDestroyNotify d = selection->destroy;

		selection->destroy = NULL;
		d (selection->user_data);
	}

	/* chain parent_class' handler */
	G_OBJECT_CLASS (ayyi_tree_selection_parent_class)->finalize (object);
}


/**
 * _ayyi_tree_selection_new:
 *
 * Creates a new #AyyiTreeSelection object.  This function should not be invoked,
 * as each #AyyiTreeView will create its own #AyyiTreeSelection.
 *
 * Return value: A newly created #AyyiTreeSelection object.
 **/
AyyiTreeSelection*
_ayyi_tree_selection_new (void)
{
	return g_object_new (AYYI_TYPE_TREE_SELECTION, NULL);
}


/**
 * _ayyi_tree_selection_new_with_tree_view:
 * @tree_view: The #AyyiTreeView.
 *
 * Creates a new #AyyiTreeSelection object.  This function should not be invoked,
 * as each #AyyiTreeView will create its own #AyyiTreeSelection.
 *
 * Return value: A newly created #AyyiTreeSelection object.
 **/
AyyiTreeSelection*
_ayyi_tree_selection_new_with_tree_view (AGlTreeView* tree_view)
{
	g_return_val_if_fail (tree_view, NULL);

	AyyiTreeSelection* selection = _ayyi_tree_selection_new ();
	_ayyi_tree_selection_set_tree_view (selection, tree_view);

	return selection;
}


/**
 * _ayyi_tree_selection_set_tree_view:
 * @selection: A #AyyiTreeSelection.
 * @tree_view: The #AyyiTreeView.
 *
 * Sets the #AyyiTreeView of @selection.  This function should not be invoked, as
 * it is used internally by #AyyiTreeView.
 **/
void
_ayyi_tree_selection_set_tree_view (AyyiTreeSelection* selection, AGlTreeView* tree_view)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));

	selection->tree_view = tree_view;
}


/**
 * ayyi_tree_selection_set_mode:
 * @selection: A #AyyiTreeSelection.
 * @type: The selection mode
 *
 * Sets the selection mode of the @selection.  If the previous type was
 * #AYYI_SELECTION_MULTIPLE, then the anchor is kept selected, if it was
 * previously selected.
 **/
void
ayyi_tree_selection_set_mode (AyyiTreeSelection* selection, AyyiSelectionMode type)
{
	AyyiTreeSelectionFunc tmp_func;
	g_return_if_fail(AYYI_IS_TREE_SELECTION(selection));

	if (selection->type == type)
		return;

	if (type == AYYI_SELECTION_NONE) {
		/* We do this so that we unconditionally unset all rows
		*/
		tmp_func = selection->user_func;
		selection->user_func = NULL;
		ayyi_tree_selection_unselect_all (selection);
		selection->user_func = tmp_func;

		ayyi_tree_row_reference_free (selection->tree_view->priv->anchor);
		selection->tree_view->priv->anchor = NULL;

	} else if (type == AYYI_SELECTION_SINGLE || type == AYYI_SELECTION_BROWSE) {
		AyyiRBTree* tree = NULL;
		AyyiRBNode* node = NULL;
		gint selected = FALSE;
		AyyiTreePath* anchor_path = NULL;

		if (selection->tree_view->priv->anchor) {
			anchor_path = ayyi_tree_row_reference_get_path (selection->tree_view->priv->anchor);

			if (anchor_path) {
				_ayyi_tree_view_find_node (selection->tree_view, anchor_path, &tree, &node);

				if (node && AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED))
					selected = TRUE;
			}
		}

		/* We do this so that we unconditionally unset all rows
		*/
		tmp_func = selection->user_func;
		selection->user_func = NULL;
		ayyi_tree_selection_unselect_all (selection);
		selection->user_func = tmp_func;

		if (node && selected)
			_ayyi_tree_selection_internal_select_node (selection, node, tree, anchor_path, 0, FALSE);
		if (anchor_path)
			ayyi_tree_path_free (anchor_path);
	}

	selection->type = type;
}


/**
 * ayyi_tree_selection_get_mode:
 * @selection: a #AyyiTreeSelection
 *
 * Gets the selection mode for @selection. See
 * ayyi_tree_selection_set_mode().
 *
 * Return value: the current selection mode
 **/
AyyiSelectionMode
ayyi_tree_selection_get_mode (AyyiTreeSelection* selection)
{
	g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), AYYI_SELECTION_SINGLE);

	return selection->type;
}

/**
 * ayyi_tree_selection_set_select_function:
 * @selection: A #AyyiTreeSelection.
 * @func: The selection function.
 * @data: The selection function's data.
 * @destroy: The destroy function for user data.  May be NULL.
 *
 * Sets the selection function.  If set, this function is called before any node
 * is selected or unselected, giving some control over which nodes are selected.
 * The select function should return %TRUE if the state of the node may be toggled,
 * and %FALSE if the state of the node should be left unchanged.
 **/
void
ayyi_tree_selection_set_select_function (AyyiTreeSelection* selection, AyyiTreeSelectionFunc func, gpointer data, GDestroyNotify destroy)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (func != NULL);

	if (selection->destroy) {
		GDestroyNotify d = selection->destroy;

		selection->destroy = NULL;
		d (selection->user_data);
	}

	selection->user_func = func;
	selection->user_data = data;
	selection->destroy = destroy;
}

#if 0
/**
 * ayyi_tree_selection_get_select_function: (skip)
 * @selection: A #AyyiTreeSelection.
 *
 * Returns the current selection function.
 *
 * Return value: The function.
 *
 * Since: 2.14
 **/
AyyiTreeSelectionFunc
ayyi_tree_selection_get_select_function (AyyiTreeSelection *selection)
{
  g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), NULL);

  return selection->user_func;
}

/**
 * ayyi_tree_selection_get_user_data: (skip)
 * @selection: A #AyyiTreeSelection.
 *
 * Returns the user data for the selection function.
 *
 * Return value: The user data.
 **/
gpointer
ayyi_tree_selection_get_user_data (AyyiTreeSelection *selection)
{
  g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), NULL);

  return selection->user_data;
}

/**
 * ayyi_tree_selection_get_tree_view:
 * @selection: A #AyyiTreeSelection
 * 
 * Returns the tree view associated with @selection.
 * 
 * Return value: (transfer none): A #AyyiTreeView
 **/
AyyiTreeView*
ayyi_tree_selection_get_tree_view (AyyiTreeSelection* selection)
{
	g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), NULL);

	return selection->tree_view;
}
#endif

/**
 * ayyi_tree_selection_get_selected:
 * @selection: A #AyyiTreeSelection.
 * @model: (out) (allow-none) (transfer none): A pointer to set to the #AyyiTreeModel, or NULL.
 * @iter: (out) (allow-none): The #AyyiTreeIter, or NULL.
 *
 * Sets @iter to the currently selected node if @selection is set to
 * #AYYI_SELECTION_SINGLE or #AYYI_SELECTION_BROWSE.  @iter may be NULL if you
 * just want to test if @selection has any selected nodes.  @model is filled
 * with the current model as a convenience.  This function will not work if you
 * use @selection is #AYYI_SELECTION_MULTIPLE.
 *
 * Return value: TRUE, if there is a selected node.
 **/
gboolean
ayyi_tree_selection_get_selected (AyyiTreeSelection* selection, AyyiTreeModel* model, AyyiTreeIter* iter)
{
	g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), FALSE);
	g_return_val_if_fail (selection->type != AYYI_SELECTION_MULTIPLE, FALSE);
	g_return_val_if_fail (selection->tree_view, FALSE);

	if (iter)
		memset(iter, 0, sizeof (AyyiTreeIter));

//	if (model)
//		*model = selection->tree_view->priv->model;

	if (!selection->tree_view->priv->anchor)
		return FALSE;

	AyyiTreePath* anchor_path = ayyi_tree_row_reference_get_path (selection->tree_view->priv->anchor);

	if (!anchor_path)
		return FALSE;

	gboolean retval = FALSE;

	AyyiRBTree* tree;
	AyyiRBNode* node;
	gboolean found_node = !_ayyi_tree_view_find_node (selection->tree_view, anchor_path, &tree, &node);

	if (found_node && AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED)) {
		/* we only want to return the anchor if it exists in the rbtree and
		 * is selected.
		 */
		if (!iter)
			retval = TRUE;
		else
			retval = ayyi_tree_model_get_iter (selection->tree_view->priv->model, iter, anchor_path);
	} else {
		/* We don't want to return the anchor if it isn't actually selected.
		 */
		retval = FALSE;
	}

	ayyi_tree_path_free (anchor_path);

	return retval;
}


/**
 * ayyi_tree_selection_get_selected_rows:
 * @selection: A #AyyiTreeSelection.
 * @model: (out) (allow-none) (transfer none): A pointer to set to the #AyyiTreeModel, or %NULL.
 *
 * Creates a list of path of all selected rows. Additionally, if you are
 * planning on modifying the model after calling this function, you may
 * want to convert the returned list into a list of #AyyiTreeRowReference<!-- -->s.
 * To do this, you can use ayyi_tree_row_reference_new().
 *
 * To free the return value, use:
 * |[
 * g_list_foreach (list, (GFunc) ayyi_tree_path_free, NULL);
 * g_list_free (list);
 * ]|
 *
 * Return value: (element-type AyyiTreePath) (transfer full): A #GList containing a #AyyiTreePath for each selected row.
 *
 **/
GList*
ayyi_tree_selection_get_selected_rows (AyyiTreeSelection* selection, AyyiTreeModel** model)
{
	GList* list = NULL;

	g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), NULL);
	g_return_val_if_fail (selection->tree_view != NULL, NULL);

	if (model)
		*model = selection->tree_view->priv->model;

	if (selection->tree_view->priv->tree == NULL || selection->tree_view->priv->tree->root == NULL)
		return NULL;

	if (selection->type == AYYI_SELECTION_NONE)
		return NULL;
	else if (selection->type != AYYI_SELECTION_MULTIPLE) {
		AyyiTreeIter iter;

		if (ayyi_tree_selection_get_selected (selection, NULL, &iter)) {
			AyyiTreePath* path = ayyi_tree_model_get_path (selection->tree_view->priv->model, &iter);
			list = g_list_append (list, path);

			return list;
		}

		return NULL;
	}

	AyyiRBTree* tree = selection->tree_view->priv->tree;
	AyyiRBNode* node = selection->tree_view->priv->tree->root;

	while (node->left != tree->nil)
		node = node->left;
	AyyiTreePath* path = ayyi_tree_path_new_first ();

	do {
		if (AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED))
			list = g_list_prepend (list, ayyi_tree_path_copy (path));

		if (node->children) {
			tree = node->children;
			node = tree->root;

			while (node->left != tree->nil)
				node = node->left;

			ayyi_tree_path_append_index (path, 0);
		} else {
			gboolean done = FALSE;

			do {
				node = _ayyi_rbtree_next (tree, node);
				if (node) {
					done = TRUE;
					ayyi_tree_path_next (path);
				} else {
					node = tree->parent_node;
					tree = tree->parent_tree;

					if (!tree) {
						ayyi_tree_path_free (path);

						goto done;
					}

					ayyi_tree_path_up (path);
				}
			} while (!done);
		}
	} while (TRUE);

	ayyi_tree_path_free (path);

	done:
	return g_list_reverse (list);
}


static void
ayyi_tree_selection_count_selected_rows_helper (AyyiRBTree* tree, AyyiRBNode* node, gpointer data)
{
	gint* count = (gint*)data;

	if (AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED))
		(*count)++;

	if (node->children)
		_ayyi_rbtree_traverse (node->children, node->children->root, G_PRE_ORDER, ayyi_tree_selection_count_selected_rows_helper, data);
}


/**
 * ayyi_tree_selection_count_selected_rows:
 * @selection: A #AyyiTreeSelection.
 *
 * Returns the number of rows that have been selected in @tree.
 *
 * Return value: The number of rows selected.
 * 
 * Since: 2.2
 **/
gint
ayyi_tree_selection_count_selected_rows (AyyiTreeSelection *selection)
{
	gint count = 0;

	g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), 0);
	g_return_val_if_fail (selection->tree_view, 0);

	if (!selection->tree_view->priv->tree || !selection->tree_view->priv->tree->root)
		return 0;

	if (selection->type == AYYI_SELECTION_SINGLE || selection->type == AYYI_SELECTION_BROWSE) {
		if (ayyi_tree_selection_get_selected (selection, NULL, NULL))
			return 1;
		else
			return 0;
	}

	_ayyi_rbtree_traverse (selection->tree_view->priv->tree, selection->tree_view->priv->tree->root, G_PRE_ORDER, ayyi_tree_selection_count_selected_rows_helper, &count);

	return count;
}


/* ayyi_tree_selection_selected_foreach helper */
static void
model_changed (gpointer data)
{
	gboolean* stop = (gboolean*)data;

	*stop = TRUE;
}


/**
 * ayyi_tree_selection_selected_foreach:
 * @selection: A #AyyiTreeSelection.
 * @func: (scope call): The function to call for each selected node.
 * @data: user data to pass to the function.
 *
 * Calls a function for each selected node. Note that you cannot modify
 * the tree or selection from within this function. As a result,
 * ayyi_tree_selection_get_selected_rows() might be more useful.
 **/
void
ayyi_tree_selection_selected_foreach (AyyiTreeSelection* selection, AyyiTreeSelectionForeachFunc func, gpointer data)
{
	AyyiTreeIter iter;

	gboolean stop = FALSE;

	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view != NULL);

	if (func == NULL || selection->tree_view->priv->tree == NULL || selection->tree_view->priv->tree->root == NULL)
		return;

	if (selection->type == AYYI_SELECTION_SINGLE || selection->type == AYYI_SELECTION_BROWSE) {
		if (ayyi_tree_row_reference_valid (selection->tree_view->priv->anchor)) {
			AyyiTreePath* path = ayyi_tree_row_reference_get_path (selection->tree_view->priv->anchor);
			ayyi_tree_model_get_iter (selection->tree_view->priv->model, &iter, path);
			(* func) (selection->tree_view->priv->model, path, &iter, data);
			ayyi_tree_path_free (path);
		}
		return;
	}

	AyyiRBTree* tree = selection->tree_view->priv->tree;
	AyyiRBNode* node = selection->tree_view->priv->tree->root;

	while (node->left != tree->nil)
		node = node->left;

	AyyiTreeModel* model = selection->tree_view->priv->model;
	g_object_ref (model);

	/* connect to signals to monitor changes in treemodel */
	gulong inserted_id = g_signal_connect_swapped (model, "row-inserted", G_CALLBACK (model_changed), &stop);
	gulong deleted_id = g_signal_connect_swapped (model, "row-deleted", G_CALLBACK (model_changed), &stop);
	gulong reordered_id = g_signal_connect_swapped (model, "rows-reordered", G_CALLBACK (model_changed), &stop);
	gulong changed_id = g_signal_connect_swapped (selection->tree_view->priv->proxy, "notify::model", G_CALLBACK (model_changed), &stop);

	/* find the node internally */
	AyyiTreePath* path = ayyi_tree_path_new_first ();

	do {
		if (AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED)) {
			ayyi_tree_model_get_iter (model, &iter, path);
			(* func) (model, path, &iter, data);
		}

		if (stop)
			goto out;

		if (node->children) {
			tree = node->children;
			node = tree->root;

			while (node->left != tree->nil)
				node = node->left;

			ayyi_tree_path_append_index (path, 0);
		} else {
			gboolean done = FALSE;

			do {
				node = _ayyi_rbtree_next (tree, node);
				if (node != NULL) {
					done = TRUE;
					ayyi_tree_path_next (path);
				} else {
					node = tree->parent_node;
					tree = tree->parent_tree;

					if (tree == NULL) {
						/* we've run out of tree */
						/* We're done with this function */

						goto out;
					}

					ayyi_tree_path_up (path);
				}
			} while (!done);
		}
	} while (TRUE);

  out:
	if (path)
		ayyi_tree_path_free (path);

	g_signal_handler_disconnect (model, inserted_id);
	g_signal_handler_disconnect (model, deleted_id);
	g_signal_handler_disconnect (model, reordered_id);
	g_signal_handler_disconnect (selection->tree_view->priv->proxy, changed_id);
	g_object_unref (model);

	/* check if we have to spew a scary message */
	if (stop)
		g_warning ("The model has been modified from within ayyi_tree_selection_selected_foreach.\n"
			"This function is for observing the selections of the tree only.  If\n"
			"you are trying to get all selected items from the tree, try using\n"
			"ayyi_tree_selection_get_selected_rows instead.\n");
}


/**
 * ayyi_tree_selection_select_path:
 * @selection: A #AyyiTreeSelection.
 * @path: The #AyyiTreePath to be selected.
 *
 * Select the row at @path.
 **/
void
ayyi_tree_selection_select_path (AyyiTreeSelection* selection, AyyiTreePath* path)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view);
	g_return_if_fail (path);

	AyyiRBNode* node;
	AyyiRBTree* tree;
	gboolean ret = _ayyi_tree_view_find_node (selection->tree_view, path, &tree, &node);

	if (node == NULL || AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED) || ret == TRUE)
		return;

	AyyiTreeSelectMode mode = (selection->type == AYYI_SELECTION_MULTIPLE)
		? AYYI_TREE_SELECT_MODE_TOGGLE
		: 0;

	_ayyi_tree_selection_internal_select_node (selection, node, tree, path, mode, FALSE);
}


/**
 * ayyi_tree_selection_unselect_path:
 * @selection: A #AyyiTreeSelection.
 * @path: The #AyyiTreePath to be unselected.
 *
 * Unselects the row at @path.
 **/
void
ayyi_tree_selection_unselect_path (AyyiTreeSelection* selection, AyyiTreePath* path)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION(selection));
	g_return_if_fail (selection->tree_view);
	g_return_if_fail (path);

	AyyiRBNode* node;
	AyyiRBTree* tree;
	gboolean ret = _ayyi_tree_view_find_node (selection->tree_view, path, &tree, &node);

	if (node == NULL || !AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED) || ret == TRUE)
		return;

	_ayyi_tree_selection_internal_select_node (selection, node, tree, path, AYYI_TREE_SELECT_MODE_TOGGLE, TRUE);
}


/**
 * ayyi_tree_selection_select_iter:
 * @selection: A #AyyiTreeSelection.
 * @iter: The #AyyiTreeIter to be selected.
 *
 * Selects the specified iterator.
 **/
void
ayyi_tree_selection_select_iter (AyyiTreeSelection* selection, AyyiTreeIter* iter)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view);
	g_return_if_fail (selection->tree_view->priv->model);
	g_return_if_fail (iter);

	AyyiTreePath* path = ayyi_tree_model_get_path (selection->tree_view->priv->model, iter);

	if (!path)
		return;

	ayyi_tree_selection_select_path (selection, path);
	ayyi_tree_path_free (path);
}


/**
 * ayyi_tree_selection_unselect_iter:
 * @selection: A #AyyiTreeSelection.
 * @iter: The #AyyiTreeIter to be unselected.
 *
 * Unselects the specified iterator.
 **/
void
ayyi_tree_selection_unselect_iter (AyyiTreeSelection* selection, AyyiTreeIter* iter)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view);
	g_return_if_fail (selection->tree_view->priv->model);
	g_return_if_fail (iter);

	AyyiTreePath* path = ayyi_tree_model_get_path (selection->tree_view->priv->model, iter);

	if (!path)
		return;

	ayyi_tree_selection_unselect_path (selection, path);
	ayyi_tree_path_free (path);
}


/**
 * ayyi_tree_selection_path_is_selected:
 * @selection: A #AyyiTreeSelection.
 * @path: A #AyyiTreePath to check selection on.
 * 
 * Returns %TRUE if the row pointed to by @path is currently selected.  If @path
 * does not point to a valid location, %FALSE is returned
 * 
 * Return value: %TRUE if @path is selected.
 **/
gboolean
ayyi_tree_selection_path_is_selected (AyyiTreeSelection* selection, AyyiTreePath* path)
{
	g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), FALSE);
	g_return_val_if_fail (path, FALSE);
	g_return_val_if_fail (selection->tree_view, FALSE);

	if (!selection->tree_view->priv->model)
		return FALSE;

	AyyiRBNode* node;
	AyyiRBTree* tree;
	gboolean ret = _ayyi_tree_view_find_node (selection->tree_view, path, &tree, &node);

	if ((node == NULL) || !AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED) || ret == TRUE)
		return FALSE;

	return TRUE;
}


/**
 * ayyi_tree_selection_iter_is_selected:
 * @selection: A #AyyiTreeSelection
 * @iter: A valid #AyyiTreeIter
 * 
 * Returns %TRUE if the row at @iter is currently selected.
 * 
 * Return value: %TRUE, if @iter is selected
 **/
gboolean
ayyi_tree_selection_iter_is_selected (AyyiTreeSelection* selection, AyyiTreeIter* iter)
{
	g_return_val_if_fail (AYYI_IS_TREE_SELECTION (selection), FALSE);
	g_return_val_if_fail (iter, FALSE);
	g_return_val_if_fail (selection->tree_view, FALSE);
	g_return_val_if_fail (selection->tree_view->priv->model, FALSE);

	AyyiTreePath* path = ayyi_tree_model_get_path (selection->tree_view->priv->model, iter);
	if (!path)
		return FALSE;

	gboolean retval = ayyi_tree_selection_path_is_selected (selection, path);
	ayyi_tree_path_free (path);

	return retval;
}


struct _TempTuple
{
   AyyiTreeSelection* selection;
   gint dirty;
};


#if 0
static void
select_all_helper (AyyiRBTree* tree, AyyiRBNode* node, gpointer data)
{
	struct _TempTuple *tuple = data;<

	if (node->children)
		_ayyi_rbtree_traverse (node->children, node->children->root, G_PRE_ORDER, select_all_helper, data);

	if (!AYYI_RBNODE_FLAG_SET (node, GTK_RBNODE_IS_SELECTED)) {
		tuple->dirty = ayyi_tree_selection_real_select_node (tuple->selection, tree, node, TRUE) || tuple->dirty;
	}
}


/* We have a real_{un,}select_all function that doesn't emit the signal, so we
 * can use it in other places without fear of the signal being emitted.
 */
static gint
ayyi_tree_selection_real_select_all (AyyiTreeSelection* selection)
{
	struct _TempTuple *tuple;

	if (!selection->tree_view->priv->tree)
		return FALSE;

	/* Mark all nodes selected */
	tuple = g_new (struct _TempTuple, 1);
	tuple->selection = selection;
	tuple->dirty = FALSE;

	_ayyi_rbtree_traverse (selection->tree_view->priv->tree, selection->tree_view->priv->tree->root, G_PRE_ORDER, select_all_helper, tuple);
	if (tuple->dirty) {
		g_free (tuple);
		return TRUE;
	}
	g_free (tuple);
	return FALSE;
}

/**
 * ayyi_tree_selection_select_all:
 * @selection: A #AyyiTreeSelection.
 *
 * Selects all the nodes. @selection must be set to #AYYI_SELECTION_MULTIPLE
 * mode.
 **/
void
ayyi_tree_selection_select_all (AyyiTreeSelection* selection)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view != NULL);

	if (!selection->tree_view->priv->tree || !selection->tree_view->priv->model)
		return;

	g_return_if_fail (selection->type == AYYI_SELECTION_MULTIPLE);

	if (ayyi_tree_selection_real_select_all (selection))
		g_signal_emit (selection, tree_selection_signals[CHANGED], 0);
}
#endif


static void
unselect_all_helper (AyyiRBTree* tree, AyyiRBNode* node, gpointer data)
{
	struct _TempTuple* tuple = data;

	if (node->children)
		_ayyi_rbtree_traverse (node->children, node->children->root, G_PRE_ORDER, unselect_all_helper, data);
	if (AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED)) {
		tuple->dirty = ayyi_tree_selection_real_select_node (tuple->selection, tree, node, FALSE) || tuple->dirty;
	}
}


static gboolean
ayyi_tree_selection_real_unselect_all (AyyiTreeSelection* selection)
{
  struct _TempTuple *tuple;

  if (selection->type == AYYI_SELECTION_SINGLE ||
      selection->type == AYYI_SELECTION_BROWSE)
    {
      AyyiRBTree *tree = NULL;
      AyyiRBNode *node = NULL;
      AyyiTreePath *anchor_path;

      if (selection->tree_view->priv->anchor == NULL)
	return FALSE;

      anchor_path = ayyi_tree_row_reference_get_path (selection->tree_view->priv->anchor);

      if (anchor_path == NULL)
        return FALSE;

      _ayyi_tree_view_find_node (selection->tree_view, anchor_path, &tree, &node);

      ayyi_tree_path_free (anchor_path);

      if (tree == NULL)
        return FALSE;

      if (AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED))
	{
	  if (ayyi_tree_selection_real_select_node (selection, tree, node, FALSE))
	    {
	      ayyi_tree_row_reference_free (selection->tree_view->priv->anchor);
	      selection->tree_view->priv->anchor = NULL;
	      return TRUE;
	    }
	}
      return FALSE;
    }
  else
    {
      tuple = g_new (struct _TempTuple, 1);
      tuple->selection = selection;
      tuple->dirty = FALSE;

      _ayyi_rbtree_traverse (selection->tree_view->priv->tree, selection->tree_view->priv->tree->root, G_PRE_ORDER, unselect_all_helper, tuple);

      if (tuple->dirty)
        {
          g_free (tuple);
          return TRUE;
        }
      g_free (tuple);
      return FALSE;
    }
}


/**
 * ayyi_tree_selection_unselect_all:
 * @selection: A #AyyiTreeSelection.
 *
 * Unselects all the nodes.
 **/
void
ayyi_tree_selection_unselect_all (AyyiTreeSelection* selection)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view != NULL);

	if (!selection->tree_view->priv->tree || !selection->tree_view->priv->model)
		return;

	if (ayyi_tree_selection_real_unselect_all (selection))
		g_signal_emit (selection, tree_selection_signals[CHANGED], 0);
}


enum
{
  RANGE_SELECT,
  RANGE_UNSELECT
};


static gint
ayyi_tree_selection_real_modify_range (AyyiTreeSelection* selection, gint mode, AyyiTreePath* start_path, AyyiTreePath* end_path)
{
	AyyiRBNode *start_node, *end_node;
	AyyiRBTree *start_tree, *end_tree;
	AyyiTreePath *anchor_path = NULL;
	gboolean dirty = FALSE;

	switch (ayyi_tree_path_compare (start_path, end_path)) {
		case 1:
			_ayyi_tree_view_find_node (selection->tree_view, end_path, &start_tree, &start_node);
			_ayyi_tree_view_find_node (selection->tree_view, start_path, &end_tree, &end_node);
			anchor_path = start_path;
			break;
		case 0:
			_ayyi_tree_view_find_node (selection->tree_view, start_path, &start_tree, &start_node);
			end_tree = start_tree;
			end_node = start_node;
			anchor_path = start_path;
			break;
		case -1:
			_ayyi_tree_view_find_node (selection->tree_view, start_path, &start_tree, &start_node);
			_ayyi_tree_view_find_node (selection->tree_view, end_path, &end_tree, &end_node);
			anchor_path = start_path;
			break;
	}

	g_return_val_if_fail (start_node, FALSE);
	g_return_val_if_fail (end_node, FALSE);

	if (anchor_path) {
		if (selection->tree_view->priv->anchor)
		ayyi_tree_row_reference_free (selection->tree_view->priv->anchor);

		selection->tree_view->priv->anchor = ayyi_tree_row_reference_new_proxy (selection->tree_view->priv->proxy, selection->tree_view->priv->model, anchor_path);
	}

	do {
		dirty |= ayyi_tree_selection_real_select_node (selection, start_tree, start_node, (mode == RANGE_SELECT) ? TRUE : FALSE);

		if (start_node == end_node)
			break;

		if (start_node->children) {
			start_tree = start_node->children;
			start_node = start_tree->root;
			while (start_node->left != start_tree->nil)
				start_node = start_node->left;

		} else {
			_ayyi_rbtree_next_full (start_tree, start_node, &start_tree, &start_node);
			if (start_tree == NULL) {
				/* we just ran out of tree.  That means someone passed in bogus values.
				 */
				return dirty;
			}
		}
	} while (TRUE);

	return dirty;
}


/**
 * ayyi_tree_selection_select_range:
 * @selection: A #AyyiTreeSelection.
 * @start_path: The initial node of the range.
 * @end_path: The final node of the range.
 *
 * Selects a range of nodes, determined by @start_path and @end_path inclusive.
 * @selection must be set to #AYYI_SELECTION_MULTIPLE mode. 
 **/
void
ayyi_tree_selection_select_range (AyyiTreeSelection* selection, AyyiTreePath* start_path, AyyiTreePath* end_path)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view);
	g_return_if_fail (selection->type == AYYI_SELECTION_MULTIPLE);
	g_return_if_fail (selection->tree_view->priv->model);

	if (ayyi_tree_selection_real_modify_range (selection, RANGE_SELECT, start_path, end_path))
		g_signal_emit (selection, tree_selection_signals[CHANGED], 0);
}


/**
 * ayyi_tree_selection_unselect_range:
 * @selection: A #AyyiTreeSelection.
 * @start_path: The initial node of the range.
 * @end_path: The initial node of the range.
 *
 * Unselects a range of nodes, determined by @start_path and @end_path
 * inclusive.
 *
 * Since: 2.2
 **/
void
ayyi_tree_selection_unselect_range (AyyiTreeSelection* selection, AyyiTreePath* start_path, AyyiTreePath* end_path)
{
	g_return_if_fail (AYYI_IS_TREE_SELECTION (selection));
	g_return_if_fail (selection->tree_view);
	g_return_if_fail (selection->tree_view->priv->model);

	if (ayyi_tree_selection_real_modify_range (selection, RANGE_UNSELECT, start_path, end_path))
		g_signal_emit (selection, tree_selection_signals[CHANGED], 0);
}


gboolean
_ayyi_tree_selection_row_is_selectable (AyyiTreeSelection* selection, AyyiRBNode* node, AyyiTreePath* path)
{
	AyyiTreeIter iter;
	gboolean sensitive = FALSE;

	if (!ayyi_tree_model_get_iter (selection->tree_view->priv->model, &iter, path))
		sensitive = TRUE;

	if (!sensitive && selection->tree_view->priv->row_separator_func) {
		/* never allow separators to be selected */
		if ((* selection->tree_view->priv->row_separator_func) (selection->tree_view->priv->model, &iter, selection->tree_view->priv->row_separator_data))
			return FALSE;
	}

	if (selection->user_func)
		return (*selection->user_func) (selection, selection->tree_view->priv->model, path, AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED), selection->user_data);
	else
		return TRUE;
}


/* Called internally by aglview.c It handles actually selecting the tree.
 */

/*
 * docs about the 'override_browse_mode', we set this flag when we want to
 * unset select the node and override the select browse mode behaviour (that is
 * 'one node should *always* be selected').
 */
void
_ayyi_tree_selection_internal_select_node (AyyiTreeSelection* selection, AyyiRBNode* node, AyyiRBTree* tree, AyyiTreePath* path, AyyiTreeSelectMode mode, gboolean override_browse_mode)
{
	g_return_if_fail(selection);

	gint dirty = FALSE;
	AyyiTreePath* anchor_path = NULL;

	if (selection->type == AYYI_SELECTION_NONE)
		return;

	if (selection->tree_view->priv->anchor)
		anchor_path = ayyi_tree_row_reference_get_path (selection->tree_view->priv->anchor);

	if (selection->type == AYYI_SELECTION_SINGLE || selection->type == AYYI_SELECTION_BROWSE) {
		/* just unselect */
		if (selection->type == AYYI_SELECTION_BROWSE && override_browse_mode) {
			dirty = ayyi_tree_selection_real_unselect_all (selection);
		}
		/* Did we try to select the same node again? */
		else if (selection->type == AYYI_SELECTION_SINGLE && anchor_path && ayyi_tree_path_compare (path, anchor_path) == 0) {
			if ((mode & AYYI_TREE_SELECT_MODE_TOGGLE) == AYYI_TREE_SELECT_MODE_TOGGLE) {
				dirty = ayyi_tree_selection_real_unselect_all (selection);
			}
		} else {
			if (anchor_path) {
				/* We only want to select the new node if we can unselect the old one,
				 * and we can select the new one. */
				dirty = _ayyi_tree_selection_row_is_selectable (selection, node, path);

				/* if dirty is FALSE, we weren't able to select the new one, otherwise, we try to
				 * unselect the new one
				 */
				if (dirty)
					dirty = ayyi_tree_selection_real_unselect_all (selection);

				/* if dirty is TRUE at this point, we successfully unselected the
				 * old one, and can then select the new one */
				if (dirty) {
					if (selection->tree_view->priv->anchor) {
						ayyi_tree_row_reference_free0 (selection->tree_view->priv->anchor);
					}

					if (ayyi_tree_selection_real_select_node (selection, tree, node, TRUE)) {
						selection->tree_view->priv->anchor = ayyi_tree_row_reference_new_proxy (selection->tree_view->priv->proxy, selection->tree_view->priv->model, path);
					}
				}
			} else {
				if (ayyi_tree_selection_real_select_node (selection, tree, node, TRUE)) {
					dirty = TRUE;
					if (selection->tree_view->priv->anchor)
						ayyi_tree_row_reference_free (selection->tree_view->priv->anchor);

					selection->tree_view->priv->anchor = ayyi_tree_row_reference_new_proxy (selection->tree_view->priv->proxy, selection->tree_view->priv->model, path);
				}
			}
		}
	}
	else if (selection->type == AYYI_SELECTION_MULTIPLE) {
		if ((mode & AYYI_TREE_SELECT_MODE_EXTEND) == AYYI_TREE_SELECT_MODE_EXTEND && (anchor_path == NULL)) {
			if (selection->tree_view->priv->anchor)
				ayyi_tree_row_reference_free (selection->tree_view->priv->anchor);

			selection->tree_view->priv->anchor = ayyi_tree_row_reference_new_proxy (selection->tree_view->priv->proxy, selection->tree_view->priv->model, path);
			dirty = ayyi_tree_selection_real_select_node (selection, tree, node, TRUE);
		} else if ((mode & (AYYI_TREE_SELECT_MODE_EXTEND | AYYI_TREE_SELECT_MODE_TOGGLE)) == (AYYI_TREE_SELECT_MODE_EXTEND | AYYI_TREE_SELECT_MODE_TOGGLE)) {
			ayyi_tree_selection_select_range (selection, anchor_path, path);
		} else if ((mode & AYYI_TREE_SELECT_MODE_TOGGLE) == AYYI_TREE_SELECT_MODE_TOGGLE) {
			gint flags = node->flags;
			if (selection->tree_view->priv->anchor)
				ayyi_tree_row_reference_free (selection->tree_view->priv->anchor);

			selection->tree_view->priv->anchor = ayyi_tree_row_reference_new_proxy (selection->tree_view->priv->proxy, selection->tree_view->priv->model, path);

			if ((flags & AYYI_RBNODE_IS_SELECTED) == AYYI_RBNODE_IS_SELECTED)
				dirty |= ayyi_tree_selection_real_select_node (selection, tree, node, FALSE);
			else
				dirty |= ayyi_tree_selection_real_select_node (selection, tree, node, TRUE);
		} else if ((mode & AYYI_TREE_SELECT_MODE_EXTEND) == AYYI_TREE_SELECT_MODE_EXTEND) {
			dirty = ayyi_tree_selection_real_unselect_all (selection);
			dirty |= ayyi_tree_selection_real_modify_range (selection, RANGE_SELECT, anchor_path, path);
		} else {
			dirty = ayyi_tree_selection_real_unselect_all (selection);

			if (selection->tree_view->priv->anchor)
				ayyi_tree_row_reference_free (selection->tree_view->priv->anchor);

			selection->tree_view->priv->anchor = ayyi_tree_row_reference_new_proxy (selection->tree_view->priv->proxy, selection->tree_view->priv->model, path);

			dirty |= ayyi_tree_selection_real_select_node (selection, tree, node, TRUE);
		}
	}

	if (anchor_path)
		ayyi_tree_path_free (anchor_path);

	if (dirty)
		g_signal_emit (selection, tree_selection_signals[CHANGED], 0);  
}


void 
_ayyi_tree_selection_emit_changed (AyyiTreeSelection* selection)
{
	g_signal_emit (selection, tree_selection_signals[CHANGED], 0);
}


/* NOTE: Any {un,}selection ever done _MUST_ be done through this function!
 */

static gint
ayyi_tree_selection_real_select_node (AyyiTreeSelection* selection, AyyiRBTree* tree, AyyiRBNode* node, gboolean select)
{
	gboolean toggle = FALSE;

	select = !! select;

	if (AYYI_RBNODE_FLAG_SET (node, AYYI_RBNODE_IS_SELECTED) != select) {
		AyyiTreePath* path = _ayyi_tree_view_find_path (selection->tree_view, tree, node);
		toggle = _ayyi_tree_selection_row_is_selectable (selection, node, path);
		ayyi_tree_path_free (path);
	}

	if (toggle) {
		node->flags ^= AYYI_RBNODE_IS_SELECTED;

		_ayyi_tree_view_queue_draw_node (selection->tree_view, tree, node, NULL);

		return TRUE;
	}

	return FALSE;
}

#define __AYYI_TREE_SELECTION_C__
