/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2012-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __worker_h__
#define __worker_h__

#include <glib-object.h>

typedef struct {
    GAsyncQueue*  msg_queue;
    GList*        jobs;
} Worker;

typedef struct _QueueItem QueueItem;

typedef void   (*WorkDoneFn) (GObject*, GError*, gpointer);
typedef void   (*WorkFn)     (GObject*, gpointer);
typedef void   (*WorkFreeFn) (gpointer);

struct _QueueItem
{
	GWeakRef         ref;
	WorkFn           work;
	WorkDoneFn       done;
	WorkFreeFn       free;
	void*            user_data;
	gboolean         cancelled;
};

void worker_init        (Worker*);
void worker_push_job    (Worker*, GObject*, WorkFn, WorkDoneFn, WorkFreeFn, gpointer);
void worker_cancel_jobs (Worker*, GObject*);

#endif
