/**
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2012-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include "agl/utils.h"
#include "debug/debug.h"
#include "worker/worker.h"

static gpointer worker_thread (gpointer);


void
worker_init (Worker* worker)
{
	worker->msg_queue = g_async_queue_new();

	if(!g_thread_new("file load thread", worker_thread, worker)){
		perr("error creating thread\n");
	}
}


	typedef struct {
		Worker*    worker;
		QueueItem* job;
	} WorkerJob;

	// note that withoug an idle fn, unreffing in the worker can cause a finalize in the worker thread
	static gboolean worker_unref_caller (gpointer _r)
	{
		void* caller = _r;
		g_object_unref(caller); // remove the reference added by g_weak_ref_get()
		return G_SOURCE_REMOVE;
	}

	/*
	 *   Do clean-up and notifications in the main thread
	 */
	static gboolean worker_post (gpointer _wj)
	{
		WorkerJob* wj = _wj;
		QueueItem* job = wj->job;
		Worker* w = wj->worker;

		if (!job->cancelled) {
			void* caller = g_weak_ref_get(&job->ref);
			if (job->done) job->done(caller, NULL, job->user_data);
			if (caller) g_object_unref(caller);
		}
		if (job->free && job->user_data) {
			g_clear_pointer(&job->user_data, job->free);
		}

		w->jobs = g_list_remove(w->jobs, job);

		g_weak_ref_set(&job->ref, NULL);
		g_free(job);
		g_free(wj);

		return G_SOURCE_REMOVE;
	}

static gpointer
worker_thread (gpointer data)
{
	dbg(2, "new file load thread.");
	Worker* w = data;

	g_return_val_if_fail(w->msg_queue, NULL);

	g_async_queue_ref(w->msg_queue);

	// check for new work
	while (true) {
		QueueItem* job = g_async_queue_pop(w->msg_queue); // blocking
		dbg(2, "starting new job: %p", job);

		void* caller = g_weak_ref_get(&job->ref);
		if (caller) {
			if (!job->cancelled) {
				// note that the job is run directly in the worker thread.
				job->work(caller, job->user_data);
			}
			g_idle_add(worker_unref_caller, caller);
		}

		g_timeout_add(1, worker_post,
			AGL_NEW(WorkerJob,
				.job = job,
				.worker = w
			)
		);

		g_usleep(100);
	}

	return NULL;
}


/*
 *  If caller is destroyed, the job will be cancelled
 *  Note that the ref count for the caller is not incremented as
 *  this will prevent the cancellation from occurring.
 */
void
worker_push_job (Worker* w, GObject* caller, WorkFn work, WorkDoneFn done, WorkFreeFn free, gpointer user_data)
{
	QueueItem* item = AGL_NEW(QueueItem,
		.work = work,
		.done = done,
		.free = free,
		.user_data = user_data
	);

	g_weak_ref_set(&item->ref, caller);

	w->jobs = g_list_append(w->jobs, item);
	g_async_queue_push(w->msg_queue, item);
}


void
worker_cancel_jobs (Worker* w, GObject* waveform)
{
	GList* l = w->jobs;
	for(;l;l=l->next){
		QueueItem* j = l->data;
		if(!j->cancelled){
			GObject* wav = g_weak_ref_get(&j->ref);
			if(wav){
				if(wav == waveform) j->cancelled = true;
				g_object_unref(wav);
			}
		}
	}
	int n_jobs = g_list_length(w->jobs);
	dbg(n_jobs ? 1 : 2, "n_jobs=%i", n_jobs);
}


