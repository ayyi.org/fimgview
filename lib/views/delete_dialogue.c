/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2016-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | DELETE CONFIRMATION DIALOGUE                                         |
 | Displays two buttons 'Ok' and 'Cancel'.                              |
 | It is passed a promise which gets resolved with true or false when   |
 | a button is pressed                                                  |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "debug/debug.h"
#include "agl/event.h"
#include "behaviours/style.h"
#include "views/button.h"
#include "views/delete_dialogue.h"

static AGl* agl = NULL;

static AGlActorClass actor_class = {0, "Delete", (AGlActorNew*)delete_dialogue};

static bool delete_dialogue_event (AGlActor*, AGlEvent*, AGliPt);
static void button_action_cancel  (AGlActor*, gpointer);
static void button_action_ok      (AGlActor*, gpointer);


AGlActorClass*
delete_dialogue_get_class ()
{
	return &actor_class;
}


static void
delete_dialogue__set_state (AGlActor* actor)
{
}


static bool
delete_dialogue__paint (AGlActor* actor)
{
	DeleteDialogue* dialogue = (DeleteDialogue*)actor;

	agl_print(10, 20, 0, 0xffffffff, "Delete %i selected files?", dialogue->n_items);

	return true;
}


static void
delete_dialogue__set_size (AGlActor* actor)
{
	actor->region = (AGlfRegion){
		.x2 = agl_actor__width(actor->parent) - actor->region.x1 * 2,
		.y2 = agl_actor__height(actor->parent) - actor->region.y1 * 2,
	};
}


AGlActor*
delete_dialogue (AMPromise* promise)
{
	agl = agl_get_instance();

	DeleteDialogue* view = agl_actor__new(DeleteDialogue,
		.actor = {
			.class = &actor_class,
			.set_state = delete_dialogue__set_state,
			.set_size = delete_dialogue__set_size,
			.paint = delete_dialogue__paint,
			.on_event = delete_dialogue_event,
		},
	);

	AGlActor* button1 = agl_button(0, "Cancel", button_action_cancel, NULL, promise);
	button1->region = (AGlfRegion){10, 35 + LINE_HEIGHT, 100, 35 + 2 * LINE_HEIGHT};
	agl_actor__add_child((AGlActor*)view, button1);

	AGlActor* button2 = agl_button(0, "Ok", button_action_ok, NULL, promise);
	button2->region = (AGlfRegion){120, 35 + LINE_HEIGHT, 220, 35 + 2 * LINE_HEIGHT};
	agl_actor__add_child((AGlActor*)view, button2);

	return (AGlActor*)view;
}


static bool
delete_dialogue_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
{
	switch (event->type) {
		case AGL_KEY_PRESS:
			switch (((AGlEventKey*)event)->keyval) {
				case 65293: // Return
					;AGlActor* button = agl_actor__find_by_name(actor, "Ok");
					button_action_ok(button, ((ButtonActor*)button)->user_data);
					return AGL_HANDLED;
				default:
					dbg(0, "%i", ((AGlEventKey*)event)->keyval);
					break;
			}
			break;
		default:
			break;

	}
	return AGL_NOT_HANDLED;
}


static void
button_action_cancel (AGlActor* actor, gpointer _)
{
	PF;
	AMPromise* promise = _;

	PromiseVal val = {.i = false};
	am_promise_resolve(promise, &val);
}


static void
button_action_ok (AGlActor* actor, gpointer user_data)
{
	PF;
	AMPromise* promise = user_data;

	PromiseVal val = {.i = true};
	am_promise_resolve(promise, &val);
}


