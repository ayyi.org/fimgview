/*
 +----------------------------------------------------------------------+
 | This file is part of Imgview.                                        |
 | copyright (C) 2016-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include "debug/debug.h"
#include "agl/actor.h"
#include "agl/event.h"
#include "agl/shader.h"
#include "icon/utils.h"
#include "tree/model.h"
#include "tree/aglview.h"
#include "tree/selection.h"
#include "file_manager/diritem.h"
#include "file_manager/view_iface.h"
#include "file_manager/mimetype.h"
#include "file_manager/support.h"
#include "file_manager/pixmaps.h"
#include "behaviours/style.h"
#include "behaviours/cache.h"
#include "views/scrollbar.h"
#include "views/files.impl.h"
#include "views/files.h"

#define _g_free0(var) (var = (g_free (var), NULL))

#define header_height     tree_row_height
#define RHS_PADDING       20 // dont draw under scrollbar
#define N_ROWS_VISIBLE(A) (agl_actor__height(((AGlActor*)A)) / tree_row_height - 1)
#define scrollable_height (view->view->model->items->len)
#define max_scroll_offset (scrollable_height - N_ROWS_VISIBLE(actor) + 2)

static void files_free  (AGlActor*);
static void files_init  (AGlActor*);
static bool files_paint (AGlActor*);
static bool files_event (AGlActor*, AGlEvent*, AGliPt);

static AGlActorClass actor_class = {0, "Files", (AGlActorNew*)files_view, files_free};

static void   files_on_scroll         (Observable*, int, gpointer);
static iRange files_row_range         (FilesView*);
static int    files_n_visible_columns (FilesView*, int[], int);

static AGl* agl = NULL;
static int instance_count = 0;

typedef enum {
	F_COL_ICON = 0,
	F_COL_NAME,
	F_COL_SIZE,
	F_COL_DATE,
	F_COL_OWNER,
	F_COL_GROUP,
} SortColum;

static int sort_types[] = {F_COL_ICON, F_COL_NAME, 0, F_COL_DATE, F_COL_SIZE, F_COL_OWNER, F_COL_GROUP, 0}; // map FmSortType to column
static int col[] = {0, FONT_SIZE * 1.6 + 8, 220, 360, 400, 440, 480};


AGlActorClass*
files_view_get_class ()
{
	if (!agl) {
		agl = agl_get_instance();
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());

		dir_init();
	}

	return &actor_class;
}


AGlActor*
files_view (void* _)
{
	instance_count++;

	files_view_get_class();

	FilesView* view = agl_actor__new(FilesView,
		.treeview = {
			.actor = {
				.class = &actor_class,
				.colour = 0x66ff99ff,
				.init = files_init,
				.paint = files_paint,
				.set_size = agl_tree_view_set_size,
				.on_event = files_event,
			},
		},
		.viewmodel = vm_directory_new()
	);

	agl_tree_view_construct((AGlTreeView*)view, NULL);

	view->viewmodel->view = (ViewIface*)(view->view = directory_view_new(view->viewmodel, view));

	observable_subscribe(((AGlTreeView*)view)->scroll, files_on_scroll, view);

	return (AGlActor*)view;
}


static bool
files_paint (AGlActor* actor)
{
	FilesView* view = (FilesView*)actor;
	AGlTreeView* tree_view = (AGlTreeView*)view;
	DirectoryView* dv = view->view;
	GPtrArray* items = dv->model->items;

	int aw = agl_actor__width(actor);

	char* col_heads[] = {"Filename", "Size", "Owner", "Group"};
	int n_cols = files_n_visible_columns(view, col, G_N_ELEMENTS(col));
	int sort = sort_types[view->view->sort_type];

	PLAIN_COLOUR2 (agl->shaders.plain) = 0x222222ff;
	agl_use_program ((AGlShader*)agl->shaders.plain);
	agl_rect_ ((AGlRect){col[sort] -2, -2, col[sort + 1] - col[sort], tree_row_height - 2});

	for (int c=0;c<n_cols;c++) {
		agl_push_clip(0.f, 0.f, MIN(col[c + 2] - 6, agl_actor__width(actor) - RHS_PADDING), 1000000.);
		agl_print(col[c + 1], -actor->scrollable.y1, 0, 0xffffffff, col_heads[c]);
		agl_pop_clip();
	}

	if (items->len) {
		int selected = 0;
		AyyiTreeIter iter;
		if (tree_view->selection->type == AYYI_SELECTION_SINGLE) {
			if (ayyi_tree_selection_get_selected (tree_view->selection, NULL, &iter)) {
				selected = GPOINTER_TO_INT(iter.user_data);
			}
		}

		AyyiTreePath* cursor;
		agl_tree_view_get_cursor(tree_view, &cursor, NULL);
		AyyiTreeIter cursor_iter = {0,};
		if (cursor) {
			if (ayyi_tree_model_get_iter(agl_tree_view_get_model(tree_view), &cursor_iter, cursor)) {
				//dbg(0, "cursor=%i", GPOINTER_TO_INT(cursor_iter.user_data));
			}
			ayyi_tree_path_free(cursor);
		}

		int y = header_height;
		iRange rows = files_row_range(view);

		for (int i = rows.start; i < rows.end; i++) {
			int row_y = y + i * tree_row_height;

			if (tree_view->selection->type == AYYI_SELECTION_SINGLE) {
				if (i == selected) {
					PLAIN_COLOUR2 (agl->shaders.plain) = 0x6677ff77;
					agl_use_program (agl->shaders.plain);
					agl_rect_((AGlRect){0, row_y - 2, agl_actor__width(actor), tree_row_height});
				}
			} else {
				AyyiTreeIter iter = (AyyiTreeIter){.user_data = GINT_TO_POINTER(i)};
				if (ayyi_tree_selection_iter_is_selected(tree_view->selection, &iter)) {
					PLAIN_COLOUR2 (agl->shaders.plain) = i == GPOINTER_TO_INT(cursor_iter.user_data) ? 0x5588ffaa : STYLE.selection;
					agl_use_program (agl->shaders.plain);
					agl_rect_((AGlRect){0, y + i * tree_row_height - 2, agl_actor__width(actor), tree_row_height});
				}
			}

																		if(i >= items->len) dbg(0, "i=%i len=%i", i, items->len);
																		g_return_val_if_fail(i < items->len, false);
			ViewItem* vitem = items->pdata[i];
			if (!vitem) {
				pwarn("vitem! i=%i %i", i, items->len);
				break;
			}

			DirItem* item = vitem->item;
			if(item){
				char size[16] = {'\0'}; if(item->flags != ITEM_FLAG_NEED_RESCAN_QUEUE) snprintf(size, 15, "%zu", item->size);
				const char* val[] = {item->leafname, size, user_name(item->uid), group_name(item->gid)};
				for(int c=0;c<n_cols;c++){
					if(col[c + 1] > aw) break;

					agl_push_clip(0, row_y, MIN(col[c + 2] - 6, agl_actor__width(actor)/* - RHS_PADDING*/), actor->region.y2 + 20.);
					agl_print(col[c + 1], y + i * tree_row_height, 0, STYLE.text, val[c]);
					agl_pop_clip();
				}

				if(item->mime_type){
					guint t = get_icon_texture_by_mimetype(item->mime_type);
					agl->shaders.texture->uniform.fg_colour = 0xffffffff;
					agl_use_program((AGlShader*)agl->shaders.texture);
					agl_textured_rect(t, 0, y + i * tree_row_height, FONT_SIZE * 1.6, FONT_SIZE * 1.6, NULL);
				}
			}
		}
	}else{
		agl_print(col[1], tree_row_height, 0, 0xffffff77, "No files");
	}

	return true;
}


static void
files_on_row_add (AyyiTreeModel* tree_model, AyyiTreePath* path, AyyiTreeIter* iter, AGlActor* actor)
{
	FilesView* view = (FilesView*)actor;

	int i = GPOINTER_TO_INT(iter->user_data);
	iRange rows = files_row_range(view);
	if(i >= rows.start && i <= rows.end){
		agl_actor__invalidate(actor);
	}
}


static void
files_on_select (AyyiTreeSelection* selection, gpointer actor)
{
	if(selection->type == AYYI_SELECTION_SINGLE){
		AyyiTreeIter iter;
		if(ayyi_tree_selection_get_selected (selection, NULL, &iter)){
			dbg(0, "  selected=%i", GPOINTER_TO_INT(iter.user_data));
			agl_actor__invalidate(actor);
		}
	}else{
		agl_actor__invalidate(actor);
	}
}


static void
files_init (AGlActor* a)
{
	FilesView* view = (FilesView*)a;
	AGlTreeView* tree_view = (AGlTreeView*)view;

	agl_tree_view_init(a);

#if 0
#ifdef AGL_ACTOR_RENDER_CACHE
	a->fbo = agl_fbo_new(agl_actor__width(a), agl_actor__height(a), 0, AGL_FBO_HAS_STENCIL);
	a->cache.enabled = true;
#endif
#endif

	g_signal_connect(view->viewmodel, "row-inserted", (GCallback)files_on_row_add, a);
	g_signal_connect(tree_view->selection, "changed", (GCallback)files_on_select, a);
}


static bool
files_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
{
	// chain up
	return agl_tree_view_event (actor, event, xy);
}


static void
files_free (AGlActor* actor)
{
	FilesView* view = (FilesView*)actor;

	g_object_unref(view->view);

	agl_tree_view_free(actor);

#ifdef WITH_VALGRIND
	if(!--instance_count){
		if(view->viewmodel) vm_directory_set_path(view->viewmodel, NULL);
	}

	void finalize_notify(gpointer data, GObject* was)
	{
		FilesView* view = data;
		view->viewmodel = NULL;
	}

	g_object_weak_ref((GObject*)view->viewmodel, finalize_notify, view);

	while(view->viewmodel){
		g_object_unref(view->viewmodel);
	}
#endif
}


static void
files_on_scroll (Observable* o, int row, gpointer _view)
{
	dbg(1, "row=%i", row);

	FilesView* view = _view;
	AGlActor* actor = _view;
	DirectoryView* dv = view->view;
	GPtrArray* items = dv->model->items;

	row = MAX(row, 0);

	// TODO move to parent class
	int height = actor->scrollable.y2 - actor->scrollable.y1;
	//int position = header_height + row * tree_row_height;
	int position = row * tree_row_height;
	actor->scrollable.y1 = -position;
	actor->scrollable.y2 = actor->scrollable.y1 + height;

	#define N_ROWS_TO_PRELOAD 50
	int n_rows = MIN(N_ROWS_VISIBLE(view) + N_ROWS_TO_PRELOAD, items->len - row);
	int r = row;
	for(int i=0;i<n_rows;i++,r++){
		ViewItem* vitem = items->pdata[r];
		DirItem* item = vitem->item;
		if(item->flags & ITEM_FLAG_NEED_RESCAN_QUEUE){
			vm_directory_fetch_item (view->viewmodel, item, NULL, NULL);
		}
	}
}


void
files_view_set_directory (FilesView* view, const char* path)
{
	vm_directory_set_path(view->viewmodel, path);

	// Because of delayed model creation, model will never be created when directory is empty
	AyyiTreeModel* model = agl_tree_view_get_model((AGlTreeView*)view);
	if(!model) return;

	ayyi_tree_selection_select_iter(((AGlTreeView*)view)->selection, &(AyyiTreeIter){0,});
}


int
files_view_row_at_coord (FilesView* view, int x, int y)
{
	y += ((AGlTreeView*)view)->scroll->value * tree_row_height - header_height;
	if(y < 0) return -1;
	int r = y / tree_row_height;
	GPtrArray* items = view->view->model->items;

	return (r > items->len)
		? -1
		: r;
}


static iRange
files_row_range (FilesView* view)
{
	DirectoryView* dv = view->view;
	AGlTreeView* tree_view = (AGlTreeView*)view;
	GPtrArray* items = dv->model->items;

	int n_rows = N_ROWS_VISIBLE(view);
	return (iRange){tree_view->scroll->value, MIN(items->len, tree_view->scroll->value + n_rows)};
}


static int
files_n_visible_columns (FilesView* view, int col[], int n)
{
	int aw = agl_actor__width((AGlActor*)view);

	for (int c=0;c<n-1;c++) {
		if (col[c + 1] > aw) return c;
	}

	return n - 1;
}
