/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#pragma once

#include "agl/utils.h"
#include "agl/shader.h"

typedef struct {
	AGlShader      shader;
	struct {
		AGliPt     btn_size;
	}              uniform;
} ButtonShader;

typedef struct {
	AGlShader      shader;
	struct {
		AGliPt     centre;
		float      radius;
	}              uniform;
} CircleShader;

enum {
    RING_RADIUS = 0,
    RING_CENTRE,
    RING_COLOUR,
    RING_BG_COLOUR,
};

extern ScrollbarShader v_scrollbar_shader;
extern ButtonShader button_shader;
extern AGlShader ring;
extern CircleShader circle_shader;

#define H_SCROLLBAR_COLOUR() \
	(((AGlUniformUnion*)&h_scrollbar_shader.shader.uniforms[0])->value.i[0])
#define H_SCROLLBAR_BG_COLOUR() \
	(((AGlUniformUnion*)&h_scrollbar_shader.shader.uniforms[1])->value.i[0])

#define BUTTON_COLOUR() \
	(((AGlUniformUnion*)&button_shader.shader.uniforms[0])->value.i[0])
#define BUTTON_BG_COLOUR() \
	(((AGlUniformUnion*)&button_shader.shader.uniforms[1])->value.i[0])
#define BUTTON_FILL_COLOUR() \
	(((AGlUniformUnion*)&button_shader.shader.uniforms[2])->value.i[0])

#define CIRCLE_COLOUR() \
	(((AGlUniformUnion*)&circle_shader.shader.uniforms[0])->value.i[0])
#define CIRCLE_BG_COLOUR() \
	(((AGlUniformUnion*)&circle_shader.shader.uniforms[1])->value.i[0])
