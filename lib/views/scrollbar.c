/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 | scrollbar.c
 |
 | A variation on AGl Scrollbar that uses a custom observable where the
 | max value is a derived value due to the potentially large number of
 | rows in the file view.
 */

#include "config.h"
#include "debug/debug.h"
#include "agl/actor.h"
#include "agl/event.h"
#include "agl/observable.h"
#include "views/typedefs.h"
#include "agl/shader.h"
#include "scrollbar.h"

#define am_source_remove0(S) {if(S) g_source_remove(S); S = 0;}

#ifdef DEBUG
#define AGL_DEBUG if(agl->debug)
#else
#define AGL_DEBUG if(false)
#endif

extern ScrollbarShader v_scrollbar_shader;

#define R 2
#define Rf 2.0
#define SCROLLBAR_WIDTH (2. * Rf + 8.)
#define V_SCROLLBAR_H_PADDING 2
#define V_SCROLLBAR_V_PADDING 3
#define H_SCROLLBAR_H_PADDING 3
#define H_SCROLLBAR_V_PADDING 2

static void scrollbar_free                 (AGlActor*);
static void scrollbar_view_set_position    (AGlActor*, int);
static void scrollbar_set_size             (AGlActor*);
static bool scrollbar_on_event             (AGlActor*, AGlEvent*, AGliPt);
static void scrollbar_on_scrollable_width  (AGlObservable*, AGlVal, gpointer);
static void scrollbar_on_scrollable_height (AGlObservable*, AGlVal, gpointer);
static void scrollbar_start_activity       (AGlActor*, bool);

static void hscrollbar_bar_position        (AGlActor*, iRange*);
static void vscrollbar_bar_position        (AGlActor*, iRange*);


typedef struct {
    AGlActor       actor;
    AGlOrientation orientation;
    int            step_size;
    AGlActor*      scrollable;
    Observable*    position;
    AGlObservable* size;
    struct {
      float        opacity;
      WfAnimatable animation;
    }              handle;
    struct {
      float        opacity;
      WfAnimatable animation;
    }              trough;
} ScrollbarActor;

static struct Press {
    AGliPt     pt;
    AGliPt     offset; // TODO this should probably be part of the actor_context
} press = {{0},};

static guint activity = 0;

static AGl* agl = NULL;
static AGlActorClass actor_class = {0, "Scrollbar", (AGlActorNew*)scrollbar_view, scrollbar_free};


AGlActorClass*
scrollbar_view_get_class ()
{
	return &actor_class;
}


static void
scrollbar_on_scroll (Observable* observable, int row, gpointer scrollbar)
{
	scrollbar_view_set_position ((AGlActor*)scrollbar, row * ((ScrollbarActor*)scrollbar)->step_size);
	scrollbar_start_activity ((AGlActor*)scrollbar, false);
}


AGlActor*
scrollbar_view (AGlActor* scrollable, AGlOrientation orientation, Observable* position)
{
	agl = agl_get_instance();

	bool scrollbar_draw_gl1 (AGlActor* actor)
	{
		return true;
	}

	bool scrollbar_draw_h (AGlActor* actor)
	{
#if 0
		if (!actor->disabled) {
			iRange bar;
			hscrollbar_bar_position(actor, &bar);

			if(((ScrollbarActor*)actor)->animation.val.f > 0.61){
				PLAIN_COLOUR2 (agl->shaders.plain) = 0xffffff66;
				agl_use_program (agl->shaders.plain);
				agl_rect (0., 1., agl_actor__width(actor), agl_actor__height(actor) - 1.);
			}

#if 0 // TODO define this shader
			H_SCROLLBAR_COLOUR() = 0xffffff00 + (int)(((ScrollbarActor*)actor)->animation.val.f * 0xff);
			H_SCROLLBAR_BG_COLOUR() = H_SCROLLBAR_COLOUR() & 0xffffff00;
			h_scrollbar_shader.uniform.centre1 = (AGliPt){bar.start + 4, 6};
			h_scrollbar_shader.uniform.centre2 = (AGliPt){bar.end   - 4, 6};
			agl_use_program((AGlShader*)&h_scrollbar_shader);
#endif

			agl_rect(H_SCROLLBAR_H_PADDING + bar.start, H_SCROLLBAR_V_PADDING, bar.end - bar.start, 11);
		}
#endif
		return true;
	}

	bool scrollbar_draw_v (AGlActor* actor)
	{
		ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

		if (!actor->disabled) {
			if (scrollbar->handle.opacity < 0.05) {
				return true;
			}

			if (scrollbar->trough.opacity > 0.05) {
				PLAIN_COLOUR2 (agl->shaders.plain) = 0xffffff00 + (int)(255.0f * scrollbar->trough.opacity);
				agl_use_program ((AGlShader*)agl->shaders.plain);
				agl_rect (1, 0., agl_actor__width(actor) - 2, agl_actor__height(actor));
			}

			iRange bar = {0,};
			vscrollbar_bar_position(actor, &bar);

			V_SCROLLBAR_COLOUR() = 0x6677ff00 + (int)(255.0f * scrollbar->handle.opacity);
			V_SCROLLBAR_BG_COLOUR() = V_SCROLLBAR_COLOUR() & 0xffffff00;
			v_scrollbar_shader.uniform.centre1 = (AGliPt){6, bar.start + 4};
			v_scrollbar_shader.uniform.centre2 = (AGliPt){6, bar.end - 4};
			agl_use_program((AGlShader*)&v_scrollbar_shader);

			agl_rect (V_SCROLLBAR_H_PADDING, bar.start, agl_actor__width(actor) - 2 * V_SCROLLBAR_H_PADDING, bar.end - bar.start);
		}
		return true;
	}

	void scrollbar_init (AGlActor* actor)
	{
		ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

		if (scrollbar->orientation == AGL_ORIENTATION_VERTICAL) {
			if (!v_scrollbar_shader.shader.program) {
				agl_create_program(&v_scrollbar_shader.shader);
				v_scrollbar_shader.uniform.radius = 3;
			}

			actor->paint = agl->use_shaders ? scrollbar_draw_v : agl_actor__null_painter;

			if (scrollbar->size) agl_observable_subscribe (scrollbar->size, scrollbar_on_scrollable_height, actor);
		} else {
#if 0
			if(!h_scrollbar_shader.shader.program){
				agl_create_program(&h_scrollbar_shader.shader);
				h_scrollbar_shader.uniform.radius = 3;
			}
#endif
			actor->paint = agl->use_shaders ? scrollbar_draw_h : scrollbar_draw_gl1;

			if (scrollbar->size) agl_observable_subscribe (scrollbar->size, scrollbar_on_scrollable_width, actor);
		}
	}

	ScrollbarActor* scrollbar = agl_actor__new (ScrollbarActor,
		.actor = {
			.class = &actor_class,
			.init = scrollbar_init,
			.set_size = scrollbar_set_size,
			.paint = agl_actor__null_painter,
			.on_event = scrollbar_on_event,
		},
		.orientation = orientation,
		.scrollable = scrollable,
		.position = position,
		.step_size = 20,
		// TODO fade in and out when leaving/entering window
		.handle = {
			.opacity = 0.5,
			.animation = {
				.start_val.f  = 0.7,
				.target_val.f = 0.7,
				.type         = WF_FLOAT
			}
		},
		.trough = {
			.opacity = 0.0,
			.animation = {
				.start_val.f  = 0.0,
				.target_val.f = 0.0,
				.type         = WF_FLOAT
			},
		}
	);
	scrollbar->handle.animation.val.f = &scrollbar->handle.opacity;
	scrollbar->trough.animation.val.f = &scrollbar->trough.opacity;

	observable_subscribe (position, scrollbar_on_scroll, scrollbar);

	return (AGlActor*)scrollbar;
}


static void
scrollbar_free (AGlActor* actor)
{
	ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

	if (scrollbar->size) agl_observable_unsubscribe (scrollbar->size, scrollbar_on_scrollable_height, actor);
	am_source_remove0(activity);
	g_free(actor);
}


static void
scrollbar_set_size (AGlActor* actor)
{
	AGlActor* scrollable = ((ScrollbarActor*)actor)->scrollable;

	if (scrollable->region.x2 > 0.) {
		if (((ScrollbarActor*)actor)->orientation == AGL_ORIENTATION_VERTICAL) {
			// must be full scrollable height in order to catch events
			actor->region = (AGlfRegion){
				.x1 = scrollable->region.x2 - SCROLLBAR_WIDTH,
				.x2 = scrollable->region.x2,
				.y1 = scrollable->region.y1 + V_SCROLLBAR_V_PADDING,
				.y2 = scrollable->region.y2 - V_SCROLLBAR_V_PADDING
			};
		} else {
			int offset = 0;

			actor->region = (AGlfRegion){
				.x1 = 0,
				.x2 = scrollable->region.x2,
				.y1 = scrollable->region.y2 - SCROLLBAR_WIDTH + offset,
				.y2 = scrollable->region.y2                   + offset
			};
		}
	}
}


static void
hscrollbar_bar_position (AGlActor* actor, iRange* pos)
{
	*pos = (iRange){0,};
}


static void
vscrollbar_bar_position (AGlActor* actor, iRange* pos)
{
	AGlActor* scrollable = ((ScrollbarActor*)actor)->scrollable;

	float size = agl_actor__height (scrollable);
	int scrollable_size = scrollable->scrollable.y2 - scrollable->scrollable.y1;

	if (size < scrollable_size) {
		double h_pct = size / (double)(scrollable_size);
		double y_pct = - scrollable->scrollable.y1 / (double)(scrollable_size - size);
		int handle = MAX(20, size * h_pct);

		pos->start = y_pct * (size - handle - 2 * V_SCROLLBAR_V_PADDING);
		pos->end = pos->start + handle;
	} else {
		*pos = (iRange){0,}; // no scrollbar needed
	}
}


static bool
scrollbar_on_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
{
	void animation_done (WfAnimation* animation, gpointer user_data)
	{
	}

	int y_to_row (ScrollbarActor* scrollbar, int y)
	{
		AGlActor* scrollable = scrollbar->scrollable;

		iRange bar = {0,};
		vscrollbar_bar_position (actor, &bar);

		int useable_bar_range = agl_actor__height(actor) - (bar.end - bar.start);
		float scale = (scrollable->scrollable.y2 - scrollable->scrollable.y1) / (float)useable_bar_range;

		return CLAMP(y, 0, useable_bar_range) * scale / scrollbar->step_size;
	}

	ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

	bool handled = false;

	switch (event->type) {
		case AGL_MOTION_NOTIFY:
			if (actor_context.grabbed == actor) {
				if (scrollbar->orientation == AGL_ORIENTATION_VERTICAL) {
					observable_set (scrollbar->position, y_to_row (scrollbar, xy.y - press.offset.y));
#if 0
				} else {
					float pos = x - press.offset.x;
					/*
					iRange bar; hscrollbar_bar_position (actor, &bar);
					int total = agl_actor__width(((AGlActor*)actor->root)) - (bar.end - bar.start) - 0 * SCROLLBAR_H_PADDING;
					float pct = MIN(1.0, pos / total);
					*/
					int dx = xy.x - press.pt.x;
					double vp_size = arr_gl_pos2px_(arrange, &am_object_val(&song->loc[AM_LOC_END]).sp) + 20.0;
					double scale = vp_size / (double)agl_actor__width(actor);
#endif
				}
			} else {
				iRange bar = {0,};
				vscrollbar_bar_position (actor, &bar);
				scrollbar->handle.animation.target_val.f = (xy.y > bar.start && xy.y < bar.end) ? 1.0 : 0.7;
				agl_actor__start_transition(actor, g_list_append(NULL, &scrollbar->handle.animation), animation_done, NULL);
			}
			break;
		case AGL_ENTER_NOTIFY:
			scrollbar->trough.animation.target_val.f = 0.15;
			agl_actor__start_transition(actor, g_list_append(NULL, &scrollbar->trough.animation), animation_done, NULL);

			iRange bar = {0,};
			vscrollbar_bar_position (actor, &bar);
			scrollbar_start_activity(actor, (xy.y > bar.start && xy.y < bar.end));
			break;
		case AGL_LEAVE_NOTIFY:
			scrollbar_start_activity(actor, false);

			scrollbar->trough.animation.target_val.f = 0.0;
			agl_actor__start_transition(actor, g_list_append(NULL, &scrollbar->trough.animation), animation_done, NULL);

			scrollbar->handle.animation.target_val.f = 0.7;
			agl_actor__start_transition(actor, g_list_append(NULL, &scrollbar->handle.animation), animation_done, NULL);
			break;
		case AGL_BUTTON_PRESS:
			{
				iRange bar = {0,};
				if (((ScrollbarActor*)actor)->orientation == AGL_ORIENTATION_VERTICAL) {
					vscrollbar_bar_position (actor, &bar);
					if(xy.y >= bar.start && xy.y <= bar.end){
						actor_context.grabbed = actor;
						handled = true;
					} else {
						observable_set (scrollbar->position, y_to_row (scrollbar, xy.y - (bar.end - bar.start) / 2));
						handled = true;
					}
				} else {
					hscrollbar_bar_position (actor, &bar);
					if (xy.x > bar.start && xy.x < bar.end) {
						actor_context.grabbed = actor;
						handled = true;
					}
				}

				press = (struct Press){
					.pt = xy,
					.offset = (AGliPt){xy.x - bar.start, xy.y - bar.start}
				};
			}
			break;
		case AGL_BUTTON_RELEASE:
			actor_context.grabbed = NULL;
			handled = true;
			break;
		default:
			break;
	}

	return handled;
}


static void
scrollbar_on_scrollable_height (AGlObservable* observable, AGlVal value, gpointer scrollbar)
{
	scrollbar_set_size (scrollbar);
	scrollbar_start_activity (scrollbar, false);
	agl_actor__invalidate (scrollbar);
}


static void
scrollbar_on_scrollable_width (AGlObservable* observable, AGlVal value, gpointer scrollbar)
{
}


static void
scrollbar_view_set_position (AGlActor* actor, int position)
{
	AGlActor* scrollable = actor->parent;

	int height = scrollable->scrollable.y2 - scrollable->scrollable.y1;

	int max_position = height - agl_actor__height(actor);

	AGL_DEBUG if(position > max_position) pwarn("position=%i max_position=%i", position, max_position);
	position = CLAMP(position, 0, max_position);

	agl_actor__invalidate(actor);
}


static gboolean
go_inactive (gpointer _view)
{
	AGlActor* actor = _view;
	ScrollbarActor* scrollbar = _view;

	if (agl_actor__is_hovered(actor)) {
		return G_SOURCE_CONTINUE;
	}

	activity = 0;

	scrollbar->handle.animation.target_val.f = 0.0;
	agl_actor__start_transition (actor, g_list_append(NULL, &scrollbar->handle.animation), NULL, NULL);

	return G_SOURCE_REMOVE;
}


/*
 *  hovered refers to the handle, not the trough
 */
static void
scrollbar_start_activity (AGlActor* actor, bool hovered)
{
	ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

	scrollbar->handle.animation.target_val.f = hovered ? 1.0 : 0.7;
	agl_actor__start_transition (actor, g_list_append(NULL, &scrollbar->handle.animation), NULL, NULL);

	if (activity) {
		g_source_remove (activity);
	}
	activity = g_timeout_add (5000, go_inactive, actor);
}
