/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2016-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __views_files_h__
#define __views_files_h__

#include "agl/actor.h"
#include "views/typedefs.h"
#include "views/directory.h"

typedef struct _FilesView FilesView;
#include "views/files.impl.h"

struct _FilesView {
   AGlTreeView        treeview;
   VMDirectory*       viewmodel;
   DirectoryView*     view;
};

AGlActorClass* files_view_get_class ();

AGlActor* files_view               (void*);
void      files_view_set_directory (FilesView*, const char*);
int       files_view_row_at_coord  (FilesView*, int x, int y);

#endif
