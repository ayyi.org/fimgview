/*
 +----------------------------------------------------------------------+
 | This file is part of libwaveform                                     |
 | https://github.com/ayyi/libwaveform                                  |
 | copyright (C) 2018-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "agl/actor.h"

#define STATUSBAR_HEIGHT (LINE_HEIGHT * 1.4)

typedef struct {
    AGlActor actor;
	char*    text;
} Statusbar;

AGlActor* statusbar     (void*);
void      statusbar_set (Statusbar*, const char* fmt, ...);
