/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2015-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib.h>
#include "debug/debug.h"
#include "agl/ext.h"
#include "views/shader.h"
#include "views/shaders/shaders.c"

static void _ring_set_uniforms();
static void _circle_set_uniforms();


#if 0
ButtonShader button_shader = {{
	.uniforms = (AGlUniformInfo[]) {
	   {"colour", 4, GL_COLOR_ARRAY, -1,},
	   {"bg_colour", 4, GL_COLOR_ARRAY, -1,},
	   {"fill_colour", 4, GL_COLOR_ARRAY, -1,},
	   {"radius", 1, GL_FLOAT, -1,},
	   END_OF_UNIFORMS
	},
	_button_set_uniforms,
	&button_text
}};
#endif

CircleShader circle_shader = {{
	.uniforms = (AGlUniformInfo[]) {
	   {"colour", 4, GL_COLOR_ARRAY, -1,},
	   {"bg_colour", 4, GL_COLOR_ARRAY, -1,},
	   END_OF_UNIFORMS
	},
	_circle_set_uniforms,
	&circle_text
}};

AGlShader ring = {
	.uniforms = (AGlUniformInfo[]) {
	   {"radius",    1, GL_FLOAT, -1, {7,  }},
	   {"centre",    2, GL_FLOAT, -1, {8,8,}},
	   {"colour",    4, GL_FLOAT, -1, {0,  }},
	   {"bg_colour", 4, GL_FLOAT, -1, {0,0,0,1}},
	   END_OF_UNIFORMS
	},
	_ring_set_uniforms,
	&ring_text
};


static inline void
set_uniform_f (AGlShader* shader, int u, float* prev)
{
	//AGlUniformInfo* uniform = &shader->uniforms[u];
	if(shader->uniforms[u].value[0] != *prev){
		glUniform1f(shader->uniforms[u].location, shader->uniforms[u].value[0]);
		*prev = shader->uniforms[u].value[0];
	}
}


#if 0
static void
_button_set_uniforms ()
{
	float btn_size[2] = {button_shader.uniform.btn_size.x, button_shader.uniform.btn_size.y};
	glUniform2fv(glGetUniformLocation(button_shader.shader.program, "btn_size"), 1, btn_size);

	agl_set_uniforms (&button_shader.shader);
}
#endif


static void
_ring_set_uniforms ()
{
	glUniform4fv(ring.uniforms[3].location, 1, ring.uniforms[3].value);
	glUniform4fv(ring.uniforms[2].location, 1, ring.uniforms[2].value);
}


static void
_circle_set_uniforms ()
{
	float centre[2] = {circle_shader.uniform.centre.x, circle_shader.uniform.centre.y};
	glUniform2fv(glGetUniformLocation(circle_shader.shader.program, "centre"), 1, centre);
	glUniform1f(glGetUniformLocation(circle_shader.shader.program, "radius"), circle_shader.uniform.radius);

	agl_set_uniforms ((AGlShader*)&circle_shader);
}
