/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include "pango/pango.h"
#include "debug/debug.h"
#include "agl/actor.h"
#include "agl/event.h"
#include "agl/shader.h"
#include "agl/text/pango.h"
#include "behaviours/style.h"
#include "shader.h"
#include "src/application.h"
#include "button.h"

static AGl* agl = NULL;

#define BG_OPACITY(B) (button->bg_opacity)
#define RIPPLE(B) (button->ripple_radius)

struct {
	AGliPt pt;
} GlButtonPress;

guint bg_textures[1] = {0,};

static void     button_free         (AGlActor*);
static void     button_init         (AGlActor*);
static bool     button_paint        (AGlActor*);
static void     button_set_size     (AGlActor*);
static bool     button_on_event     (AGlActor*, AGlEvent*, AGliPt);
static uint32_t colour_mix_rgba     (uint32_t, uint32_t, float amount);
static uint32_t colour_lighter_rgba (uint32_t, int amount);

static AGlActorClass actor_class = {0, "Button", (AGlActorNew*)agl_button, button_free};


AGlActor*
agl_button (int icon, char* text, ButtonAction action, ButtonGetState get_state, gpointer user_data)
{
	agl = agl_get_instance();

	ButtonActor* button = agl_actor__new (ButtonActor,
		.actor = {
			.class    = &actor_class,
			.name     = g_strdup(text ? text : actor_class.name),
			.init     = button_init,
			.paint    = button_paint,
			.set_size = button_set_size,
			.on_event = button_on_event,
		},
		.icon      = icon,
		.text      = text,
		.action    = action,
		.get_state = get_state,
		.user_data = user_data,
	);

	button->animatables[0] = AGL_NEW(WfAnimatable,
		.val.f        = &button->bg_opacity,
		.target_val.f = 0.0,
		.start_val.f  = 0.0,
		.type         = WF_FLOAT
	);

	button->animatables[1] = AGL_NEW(WfAnimatable,
		.val.f = &button->ripple_radius,
		.type  = WF_FLOAT
	);

	return (AGlActor*)button;
}


static void
button_free (AGlActor* actor)
{
	g_free(((ButtonActor*)actor)->animatables[0]);
	g_free(((ButtonActor*)actor)->animatables[1]);
	g_free(actor);
}


static void
button_init (AGlActor* actor)
{
	if (!circle_shader.shader.program) agl_create_program(&circle_shader.shader);
}


static bool
button_paint (AGlActor* actor)
{
	ButtonActor* button = (ButtonActor*)actor;

	bool state = button->get_state ? button->get_state(actor, button->user_data) : false;

	AGlRect r = {
		.x = 0.0,
		.y = 0.0,
		.w = agl_actor__width(actor),
		.h = agl_actor__height(actor)
	};

	// background
	if (RIPPLE(button) > 0.0) {
		PLAIN_COLOUR2 (agl->shaders.plain) = state
			? colour_mix_rgba(STYLE.bg_alt, STYLE.bg_selected, RIPPLE(button) / 64.0)
			: colour_mix_rgba(STYLE.bg_selected, STYLE.bg_alt, RIPPLE(button) / 64.0);
	} else if(state) {
		PLAIN_COLOUR2 (agl->shaders.plain) = STYLE.bg_selected;
	} else {
		PLAIN_COLOUR2 (agl->shaders.plain) = STYLE.bg_alt;
	}

	// hover background
	if (BG_OPACITY(button) > 0.0) { // dont get events if disabled, so no need to check state (TODO turn off hover when disabling).
		float alpha = button->bg_opacity;
		uint32_t fg = colour_lighter_rgba(PLAIN_COLOUR2 (agl->shaders.plain), 16);
		PLAIN_COLOUR2 (agl->shaders.plain) = STYLE.bg_alt ? colour_mix_rgba(PLAIN_COLOUR2 (agl->shaders.plain), fg, alpha) : (fg & 0xffffff00) + (uint32_t)(alpha * 0xff);
	}

	if (!(RIPPLE(button) > 0.0)) {
		agl_use_program (agl->shaders.plain);
		agl_rect_(r);

	} else {
		// click ripple
		//int radius = RIPPLE(button);
#undef RIPPLE_EXPAND
#ifdef RIPPLE_EXPAND
		circle_shader.uniform.radius = radius;
		agl_use_program ((AGlShader*)&circle_shader);
		AGlRect rr = r;
		rr.w = rr.h = radius * 2;
		glTranslatef(GlButtonPress.pt.x - actor->region.x1 - radius, GlButtonPress.pt.y - radius, 0.0);
		agl_rect_(rr);
		glTranslatef(-(GlButtonPress.pt.x - actor->region.x1 - radius), -(GlButtonPress.pt.y - radius), -0.0);
#else
		CIRCLE_COLOUR() = colour_lighter_rgba(PLAIN_COLOUR2 (agl->shaders.plain), 32);
		CIRCLE_BG_COLOUR() = PLAIN_COLOUR2 (agl->shaders.plain);
		circle_shader.uniform.centre = GlButtonPress.pt;
		circle_shader.uniform.radius = RIPPLE(button);
		agl_use_program((AGlShader*)&circle_shader);
		agl_rect_(r);
#endif
	}

	if(button->icon){
		agl->shaders.texture->uniform.fg_colour = 0xffffff00 + (agl_actor__is_disabled(actor) ? 0x77 : 0xff);
		agl_use_program((AGlShader*)agl->shaders.texture);

		agl_textured_rect(bg_textures[button->icon + (state ? 1 : 0)], r.x, r.y + r.h / 2.0 - 8.0, 16.0, 16.0, NULL);
		//texture_box(arrange, bg_textures[button->icon], get_style_bg_color_rgba(GTK_STATE_NORMAL), 10.0, -10.0, 32.0, 32.0);

		//texture_box(arrange, bg_textures[TEXTURE_BG], get_style_bg_color_rgba(GTK_STATE_NORMAL), 200.0, 10.0, 50.0, 50.0);
		//texture_box(arrange, bg_textures[TEXTURE_ICON], 0xffffffff, 200.0, 61.0, w, h);
	}

	if(button->text){
		PangoFontDescription* font_desc = pango_font_description_new();
		pango_font_description_set_family(font_desc, "Sans");

		PangoLayout* layout = pango_layout_new (agl_pango_get_context());

		pango_font_description_set_size(font_desc, FONT_SIZE * PANGO_SCALE);
		pango_font_description_set_weight(font_desc, PANGO_WEIGHT_SEMIBOLD);
		pango_layout_set_font_description(layout, font_desc);

		pango_layout_set_text(layout, button->text, -1);

		PangoRectangle logical_rect;
		pango_layout_get_pixel_extents(layout, NULL, &logical_rect);

		agl_print_layout((agl_actor__width(actor) - logical_rect.width) / 2, (agl_actor__height(actor) - logical_rect.height) / 2., 0, STYLE.fg, layout);

		pango_font_description_free (font_desc);
	}

	return true;
}


static void
button_set_size (AGlActor* actor)
{
	//actor->region.y2 = actor->parent->region.y2 - actor->parent->region.y1;
}


static bool
button_on_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
{
	ButtonActor* button = (ButtonActor*)actor;

	if(button->disabled) return AGL_NOT_HANDLED;

	void animation_done (WfAnimation* animation, gpointer user_data)
	{
	}

	void ripple_done (WfAnimation* animation, gpointer user_data)
	{
		ButtonActor* button = user_data;
		RIPPLE(button) = 0.0;
	}

	switch (event->type){
		case AGL_ENTER_NOTIFY:
			dbg (1, "ENTER_NOTIFY");
			//set_cursor(actor->root->widget->window, CURSOR_H_DOUBLE_ARROW);

			button->bg_opacity = 1.0;
			agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[0]), animation_done, NULL);
			return AGL_HANDLED;
		case AGL_LEAVE_NOTIFY:
			dbg (1, "LEAVE_NOTIFY");
			//set_cursor(actor->root->widget->window, CURSOR_NORMAL);

			button->bg_opacity = 0.0;
			agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[0]), animation_done, NULL);
			return AGL_HANDLED;
		case AGL_BUTTON_PRESS:
			return AGL_HANDLED;
		case AGL_BUTTON_RELEASE:
			dbg(1, "BUTTON_RELEASE");
			if(event->button.button == 1){
				if(button->action) button->action(actor, button->user_data);

				GlButtonPress.pt = xy;
				button->ripple_radius = 0.001;
				button->animatables[1]->target_val.f = 64.0;
				agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[1]), ripple_done, button);
				return AGL_HANDLED;
			}
			break;
		default:
			break;
	}
	return AGL_NOT_HANDLED;
}


void
gl_button_set_sensitive (AGlActor* actor, gboolean sensitive)
{
#ifdef AGL_DEBUG_ACTOR
	dbg(0, "%s: %i", actor->name, sensitive);
#endif
	((ButtonActor*)actor)->disabled = !sensitive;
}


static uint32_t
colour_mix_rgba (uint32_t a, uint32_t b, float _amount)
{
	int amount = _amount * 0xff;
	int red0 = a                >> 24;
	int grn0 = (a & 0x00ff0000) >> 16;
	int blu0 = (a & 0x0000ff00) >>  8;
	int   a0 = (a & 0x000000ff);
	int red1 = b                >> 24;
	int grn1 = (b & 0x00ff0000) >> 16;
	int blu1 = (b & 0x0000ff00) >>  8;
	int   a1 = (b & 0x000000ff);
	return
		(((red0 * (0xff - amount) + red1 * amount) / 256) << 24) +
		(((grn0 * (0xff - amount) + grn1 * amount) / 256) << 16) +
		(((blu0 * (0xff - amount) + blu1 * amount) / 256) <<  8) +
		(a0 * (0xff - amount) + a1 * amount) / 256;
}


static uint32_t
colour_lighter_rgba (uint32_t colour, int amount)
{
	// return a colour slightly lighter than the one given

	int fixed = 0x2 * amount; // 0x10 is just visible on black with amount=2
	float multiplier = 1.0;// + amount / 60.0;

	int red   = MIN(((colour & 0xff000000) >> 24) * multiplier + fixed, 0xff);
	/*
	printf("%s(): red: %x %x %x\n", __func__, (colour & 0xff000000) >> 24,
	                 (int)(((colour & 0xff000000) >> 24) * 1.2 + 0x600),
	                 red
	                 );
	*/
	int green = MIN(((colour & 0x00ff0000) >> 16) * multiplier + fixed, 0xff);
	int blue  = MIN(((colour & 0x0000ff00) >>  8) * multiplier + fixed, 0xff);
	int alpha = colour & 0x000000ff;

	//dbg(0, "%08x --> %08x", colour, red << 24 | green << 16 | blue << 8 | alpha);
	return red << 24 | green << 16 | blue << 8 | alpha;
}


