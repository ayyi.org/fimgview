/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2017-2023 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
* +----------------------------------------------------------------------+
* | Directory implements the AyyiTreeModel interface                     |
* | A VMDirectory pointer can safely be cast to AyyiTreeModel*           |
* +----------------------------------------------------------------------+
*
*/

#pragma once

#include "file_manager/dir.h"

#include <glib.h>

G_BEGIN_DECLS

typedef struct _VMDirectory VMDirectory;
typedef struct _VMDirectoryClass VMDirectoryClass;
typedef struct _VMDirectoryPrivate VMDirectoryPrivate;

struct _VMDirectory {
    GObject             parent_instance;
    ViewIface*          view;
    VMDirectoryPrivate* priv;
    GPtrArray*          items;            // array of ViewItem*
};

typedef void (*DirItemFn)(VMDirectory*, DirItem*, gpointer data);

VMDirectory* vm_directory_new          ();
VMDirectory* vm_directory_construct    (GType);
void         vm_directory_set_path     (VMDirectory*, const char*);
const char*  vm_directory_get_path     (VMDirectory*);
gboolean     vm_directory_match_filter (VMDirectory*, DirItem*);
void         vm_directory_fetch_item   (VMDirectory*, DirItem*, DirItemFn, gpointer);
void         vm_directory_wait_item    (VMDirectory*, DirItem*, DirItemFn, gpointer);

G_END_DECLS
