/*
 +----------------------------------------------------------------------+
 | This file is part of imgview                                         |
 | copyright (C) 2016-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | IMAGE ACTOR                                                          |
 | Displays an image file from disk.                                    |
 +----------------------------------------------------------------------+
 |
 */

#define USE_TEXTURE_CACHE

#include "config.h"
#include <stdio.h>
#include <jpeglib.h>
#include <math.h>
#include "debug/debug.h"
#include "agl/actor.h"
#include "agl/event.h"
#include "agl/fbo.h"
#include "agl/behaviours/key.h"
#include "agl/shader.h"
#include "src/imgload.h"
#include "views/panel.h"
#include "views/img.h"

#define DYNAMIC_ASSIGN(PROP, VAL) \
	if (PROP) g_free(PROP); \
	PROP = g_strdup(VAL);

static AGl* agl = NULL;

static void img_view__free (AGlActor*);
static void clear_pan      (ImgView*);

static AGlActorClass actor_class = {0, "Img", (AGlActorNew*)img_view, img_view__free};

static ActorKeyHandler img_view_zoom_in, img_view_zoom_out;

static ActorKey keys[] = {
	{XK_plus,        img_view_zoom_in},
	{XK_KP_Add,      img_view_zoom_in},
	{XK_equal,       img_view_zoom_in},
	{XK_minus,       img_view_zoom_out},
	{XK_KP_Subtract, img_view_zoom_out},
	{0,}
};

#define KEYS(A) ((KeyBehaviour*)(A)->behaviours[0])


AGlActorClass*
img_view_get_class ()
{
	if (!agl) {
		agl = agl_get_instance();
		wf_transition.length = 150;
		agl_actor_class__add_behaviour(&actor_class, key_get_class());
	}

	return &actor_class;
}


static void
img_view__init (AGlActor* actor)
{
#ifdef USE_TEXTURE_CACHE
	texture_cache_init_();
#else
	agl_enable(AGL_ENABLE_TEXTURE_2D | AGL_ENABLE_BLEND);
	glGenTextures(1, &view->texture);
#endif

	PanelView* panel = (PanelView*)actor->parent;
	panel->no_border = true;
}


static void
img_view__set_state (AGlActor* actor)
{
	agl->shaders.texture->uniform.fg_colour = 0xffffffff;
}


typedef struct {
    AGlQuad  coords;
    AGliSize size;
    AGliPt   pos;
    AGlfPt   max_offset;
} RenderData;


/*
 *  Zoom and offset are passed as arguments so the caller can choose
 *  to use either the target or current values
 */
static void
img_view__texture_coords (ImgView* view, float zoom, AGlPtf* offset, RenderData* r)
{
	AGlActor* actor = (AGlActor*)view;

	AGliSize size = {agl_actor__width(actor), agl_actor__height(actor)};

	float view_aspect = ((float)agl_actor__width(actor)) / agl_actor__height(actor);
	float img_aspect = ((float)view->texture.size.w) / view->texture.size.h;
	if (view_aspect > img_aspect) {
		size.w *= img_aspect / view_aspect;
	} else if(view_aspect < img_aspect) {
		size.h *= view_aspect / img_aspect;
	}

	if (zoom > 1.) {
		size.w = MIN(agl_actor__width(actor), size.w * zoom);
		size.h = MIN(agl_actor__height(actor), size.h * zoom);
	}

	AGlfPt pct_offset = {
		- offset->x / view->texture.size.w,
		- offset->y / view->texture.size.h
	};

	float mag = (view_aspect > img_aspect)
		? ((float)size.h) / view->texture.size.h
		: ((float)size.w) / view->texture.size.w;

	AGlfPt pct_visible = {
		((float)size.w) / view->texture.size.w / mag / zoom,
		((float)size.h) / view->texture.size.h / mag / zoom
	};

	*r = (RenderData){
		.coords = {
			(1. - pct_visible.x) / 2. + pct_offset.x,
			(1. - pct_visible.y) / 2. + pct_offset.y,
			(1. + pct_visible.x) / 2. + pct_offset.x,
			(1. + pct_visible.y) / 2. + pct_offset.y
		},
		.size = size,
		.pos = view->centered
			? (AGliPt){
				.x = (agl_actor__width(actor) - size.w) / 2,
				.y = (agl_actor__height(actor) - size.h) / 2
			}
			: (AGliPt){0,},
		.max_offset = {
			view->texture.size.w * (1. - ((float)size.w) / view->texture.size.w / mag / zoom) / 2.,
			view->texture.size.h * (1. - ((float)size.h) / view->texture.size.h / mag / zoom) / 2.
		}
	};
}


typedef enum {
	HOR,
	VERT,
} Direction;

typedef struct {
	ImgView* view;
	Direction dir;
	float speed;
	float max_speed; // always positive
	struct End {
		float speed;
		float distance;
	} end;
} Pan;

static Pan p;

#define IS_END() (p.seg == 2)

static void
transition_ease_in_pt (WfAnimation* animation, WfAnimatable* animatable, uint64_t time)
{
#ifdef DEBUG
	if (!p.view) {
		return pwarn("callback for terminated transition?");
	}
#endif

	float orig_val = animatable->start_val.pt.a[p.dir];
	float target_val = animatable->target_val.pt.a[p.dir];

	float* val = &animatable->val.pt->a[p.dir];

	int increasing = target_val > orig_val ? 1 : -1;
	float remaining = (target_val - *val) * increasing;

	#define END_DISTANCE (2.5 * powf(p.speed, 1.5))
	if ((!p.end.speed) && remaining > END_DISTANCE) {
		float acceleration = 0.2;
		p.speed = CLAMP(p.speed + acceleration, .5, p.max_speed);
	} else {
		if (!p.end.speed) {
			p.end = (struct End){
				.speed = p.speed,
				.distance = remaining
			};
		}

		#define final_speed 1
		#define travelled (p.end.distance - remaining)
		p.speed = final_speed + (1. - powf(travelled / p.end.distance, 2.5)) * (p.end.speed - final_speed);
	}

	*val += p.speed * increasing;
	if ((*val - target_val) * increasing > -1) {
		*val = target_val;

		gboolean clear (gpointer _)
		{
			clear_pan (p.view);

			return G_SOURCE_REMOVE;
		}
		g_idle_add(clear, NULL);
	}
}

static WfEasing ease_in = {NULL, NULL, NULL, transition_ease_in_pt};

void
agl_observable_set_string (AGlObservable* observable, const char* value)
{
	typedef struct {
	   AGlObservableFn fn;
	   gpointer        user;
	} Subscription;

	if (!observable->value.c || strcmp(value, observable->value.c)) {
		DYNAMIC_ASSIGN(observable->value.c, value);

		GList* l = observable->subscriptions;
		for (;l;l=l->next) {
			Subscription* subscription = l->data;
			subscription->fn(observable, observable->value, subscription->user);
		}
	}
}


static bool
img_view__paint (AGlActor* actor)
{
	// Not currently using an fbo cache.
	// As only one rect is being used, there is unlikely to be any benefit.

	ImgView* view = (ImgView*)actor;

	if (view->img && view->texture.id) {
		dbg(1, "%u: %.0f x %.0f", view->texture.id, agl_actor__width(actor), actor->region.y2);
		if (!glIsTexture(view->texture.id)) {
			pwarn("texture %u has been evicted", view->texture.id);
		}

		RenderData r = {0,};
		img_view__texture_coords(view, view->zoom, &view->offset, &r);
		agl_textured_rect(view->texture.id, r.pos.x, r.pos.y, r.size.w, r.size.h, &r.coords);

		agl_observable_set_string(view->rendered, view->img);
	}

	return true;
}


static void
img_view__set_size (AGlActor* actor)
{
}


static void
agl_actor__stop_transition (AGlActor* actor, WfAnimatable* animatable)
{
	GList* l = actor->transitions;
	GList* next = NULL;
	for (;l;l=next) {
		next = l->next; // store before the link is freed

		WfAnimation* animation = l->data;
		if (wf_animation_remove_animatable (animation, animatable)) {
			actor->transitions = g_list_remove(actor->transitions, animation);
		}
	}
}


static WfAnimation* animation = NULL;

static void clear_pan (ImgView* view)
{
	if (animation) {
		p = (Pan){0,};
		view->_offset.target_val.pt = view->offset;
		agl_actor__stop_transition ((AGlActor*)view, &view->_offset);
		animation = NULL;
	}
}


static bool
img_view_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
{
	ImgView* view = (ImgView*)actor;

	static int pressed_down_count = 0;
	static int pressed_key = 0;

	/*
	 *  param min is 0 for horizontal, or 1 for vertical
	 */
	void start_pan (AGlActor* actor, float delta, int min, int max)
	{
		ImgView* view = (ImgView*)actor;

		AGlPtf offset = view->offset;
		((float*)&offset)[min] += delta;
		RenderData r = {0,};
		img_view__texture_coords(view, view->zoom, &offset, &r);

		if (((float*)&r.coords)[min] < 0.f || ((float*)&r.coords)[max] > 1.f) {
			float max_offset = ((float*)&r.max_offset)[min];
			((float*)&offset)[min] = CLAMP(((float*)&offset)[min], -max_offset, max_offset);
		}
		float diff = ABS(view->offset.a[min] - offset.a[min]);
		if (diff > 0.) {
			p = (Pan){
				view,
				.dir = min,
				.max_speed = 40.,
			};
			view->_offset.target_val.pt = offset;
			animation = agl_actor__start_transition(actor, g_list_append(NULL, &view->_offset), NULL, NULL);
			animation->frame_fn = &ease_in;
			animation->length = ((int)diff) * 10;
			animation->end = animation->start + 1000 * animation->length;
		}
	}

	switch (event->type) {
		case AGL_KEY_PRESS:
			;int key = ((AGlEventKey*)event)->keyval;
			int vertical = 0;
			switch (key) {
				case XK_Down:
				case XK_Up:
					vertical = 1;
				case XK_Left:
				case XK_Right:
					pressed_down_count++;

					if (key != pressed_key) {
						clear_pan (view);
						pressed_key = key;
					}
					if (!animation) {
						start_pan (actor, key == XK_Right || key == XK_Down ? -8000 : 8000, 0 + vertical, 2 + vertical);
					}
					return AGL_HANDLED;
				default:
					;
			}
			break;
		case AGL_KEY_RELEASE:
			;int keyval = ((AGlEventKey*)event)->keyval;
			if (keyval == XK_Down || keyval == XK_Up || keyval == XK_Left || keyval == XK_Right) {

				gboolean release (gpointer _actor)
				{
					AGlActor* actor = _actor;

					pressed_down_count--;
					if (pressed_down_count < 1) {
						clear_pan ((ImgView*)actor);
					}
					return G_SOURCE_REMOVE;
				}
				if (keyval == pressed_key) g_timeout_add(50, release, actor);
			}
			break;
		default:
			return AGL_NOT_HANDLED;
	}
	return AGL_NOT_HANDLED;
}


AGlActor*
img_view (AGlActor* _)
{
	img_view_get_class();

	ImgView* view = agl_actor__new(ImgView,
		.actor = {
			.class = &actor_class,
			.program = (AGlShader*)agl->shaders.texture,
			.init = img_view__init,
			.set_state = img_view__set_state,
			.set_size = img_view__set_size,
			.paint = img_view__paint,
			.on_event = img_view_event,
		},
		.zoom = 1.,
		._zoom = {
			.type = WF_FLOAT,
			.target_val.f = 1.
		},
		._offset.type = WF_PT,
		.rendered = agl_observable_new(),
	);
	view->_zoom.val.f = &view->zoom;
	view->_offset.val.pt = &view->offset;

	KEYS((AGlActor*)view)->keys = &keys;

	return (AGlActor*)view;
}


static void
img_view__free (AGlActor* actor)
{
	ImgView* view = (ImgView*)actor;

	g_clear_pointer(&view->img, g_free);
	g_clear_pointer(&view->rendered->value.c, g_free);
	g_clear_pointer(&view->rendered, agl_observable_free);
	g_free(view);
}


Texture*
img_view_load_img (ImgView* view, char* img, MIME_type* mime_type, gpointer ref)
{
	dbg(1, "%s (%s/%s)", img, mime_type->media_type, mime_type->subtype);

	if (!strcmp(mime_type->media_type, "image")) {
		Texture* t = texture_cache_lookup_(ref);

		if (t) {
			//dbg(1, "from cache %i %ix%i", t->id, t->size.w, t->size.h);
		} else {

			ImgLoadItem item = {
				.item = ref,
				.path = img,
				.mime_type = mime_type,
			};

			load_img_a (&item);
			t = load_img_b (&item);

			g_return_val_if_fail(t, NULL);
		}
		return t;
	}
	return NULL;
}


/*
 *  Images are now loaded asynchronously.
 *  To be notified when the image has been displayed, subscribe to the `rendered` observable
 */
void
img_view_show_img (ImgView* view, char* img, MIME_type* mime_type, gpointer ref)
{
	g_return_if_fail(view);

	dbg(1, "%s", img);

	if (view->img) g_free(view->img);
	view->img = g_strdup(img);

	static guint idle_id = 0;
	static MIME_type* _mime_type;
	static gpointer _ref;

	_mime_type = mime_type;
	_ref = ref;

	gboolean _img_view_show_img (gpointer _view)
	{
		ImgView* view = _view;

		Texture* t;
		if ((t = img_view_load_img(view, view->img, _mime_type, _ref))) {
			texture_cache_freshen_(_ref); // prevent texture from being purged
			view->texture = *t;

			view->zoom = view->_zoom.target_val.f = 1.;
			view->offset = view->_offset.target_val.pt = (AGlPtf){0,};
			agl_actor__invalidate((AGlActor*)view);
		}

		idle_id = 0;
		return G_SOURCE_REMOVE;
	}

	if (!idle_id) idle_id = g_idle_add(_img_view_show_img, view);
}


static bool
img_view_zoom_in (AGlActor* actor, AGlModifierType modifiers)
{
	ImgView* view = (ImgView*)actor;

	view->_zoom.target_val.f = MIN(8., view->_zoom.target_val.f * 1.1);
	agl_actor__start_transition(actor, g_list_append(NULL, &view->_zoom), NULL, NULL);

	return AGL_HANDLED;
}


static bool
img_view_zoom_out (AGlActor* actor, AGlModifierType modifiers)
{
	ImgView* view = (ImgView*)actor;

	view->_zoom.target_val.f = MAX(1., view->_zoom.target_val.f / 1.1);
	GList* animatables = g_list_append(NULL, &view->_zoom);

	// get max_offset for the new zoom and adjust offset if needed
	RenderData r = {0,};
	img_view__texture_coords(view, view->_zoom.target_val.f, &view->offset, &r);
	UVal offset = view->_offset.target_val;
	if (view->offset.x > r.max_offset.x) {
		offset.pt.x = r.max_offset.x;
	}
	if (view->offset.x < -r.max_offset.x) {
		offset.pt.x = -r.max_offset.x;
	}
	if (view->offset.y > r.max_offset.y) {
		offset.pt.y = r.max_offset.y;
	}
	if (view->offset.y < -r.max_offset.y) {
		offset.pt.y = -r.max_offset.y;
	}

	if (memcmp(&offset, &view->_offset.target_val, sizeof(AGlPtf))) {
		view->_offset.target_val = offset;
		animatables = g_list_append(animatables, &view->_offset);
	}

	agl_actor__start_transition(actor, animatables, NULL, NULL);

	return AGL_HANDLED;
}
