/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "agl/actor.h"

typedef void      (*ButtonAction)   (AGlActor*, gpointer);
typedef bool      (*ButtonGetState) (AGlActor*, gpointer);

typedef struct {
    AGlActor       actor;
    guint          icon;
    char*          text;
    ButtonAction   action;
    ButtonGetState get_state;
    gboolean       disabled;
    float          bg_opacity;
    float          ripple_radius;
    WfAnimatable*  animatables[2];
    gpointer       user_data;
} ButtonActor;

AGlActor* agl_button (int icon, char*, ButtonAction, ButtonGetState, gpointer);
