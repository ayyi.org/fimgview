/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2017-2023 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
* +----------------------------------------------------------------------+
* | Display of a directory makes use of 3 objects:                       |
* |                                                                      |
* | * model     - file_manager Directory  - pure model                   |
* | * viewmodel - VMDirectory (this file) - does not render anything,    |
* |               but provides data specific to a particular view        |
* |               such as sorting, filtering.                            |
* |               (using AyyiTreeModel interface).                       |
* | * view      - views/files.c - pure view - rendering and user input   |
* |               only.                                                  |
* |               The view does not talk to the model, only to the VM.   |
* |               The selection is part of the view.                     |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib.h>
#include <glib-object.h>
#include <errno.h>
#include <fnmatch.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include "debug/debug.h"
#include "agl/utils.h"
#include "file_manager/typedefs.h"
#include "file_manager/diritem.h"
#include "file_manager/fscache.h"
#include "file_manager/view_iface.h"
#include "file_manager/support.h"
#include "file_manager/file_deleter.h"
#include "tree/model.h"
#include "directory.h"

#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

#define TYPE_DIRECTORY            (directory_get_type ())
#define DIRECTORY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_DIRECTORY, VMDirectory))
#define DIRECTORY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  TYPE_DIRECTORY, VMDirectoryClass))
#define IS_DIRECTORY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_DIRECTORY))
#define IS_DIRECTORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  TYPE_DIRECTORY))
#define DIRECTORY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TYPE_DIRECTORY, VMDirectoryClass))

struct _VMDirectoryClass {
    GObjectClass parent_class;
};

static gpointer vm_directory_parent_class = NULL;
static AyyiTreeModelIface* directory_ayyi_tree_model_parent_iface = NULL;

enum  {
	DIRECTORY_DUMMY_PROPERTY
};

typedef struct {
    DirItem*  item;
    DirItemFn fn;
    gpointer  user_data;
} ItemCallback;

struct _VMDirectoryPrivate {
	/*
    GList*      users;           // Functions to call on update
    char*       error;           // NULL => no error
    struct stat stat_info;       // Internal use

    gboolean	notify_active;   // Notify timeout is running
    gint		idle_callback;   // Idle callback ID

	gboolean    needs_update;
    GList*      recheck_list;    // Items to check on callback

    gboolean    have_scanned;    // TRUE after first complete scan
    gboolean    scanning;        // TRUE if we sent DIR_START_SCAN

    GHashTable* known_items;     // What our users know about
    GPtrArray*  new_items;       // New items to add in
    GPtrArray*  up_items;        // Items to redraw
    GPtrArray*  gone_items;      // Items removed
	*/
    gboolean	               scanning;        // State of the 'scanning' indicator
    gchar*                     sym_path;        // Path the user sees
    gchar*                     real_path;       // realpath(sym_path)
    Directory*                 directory;
    char*                      auto_select;     // If it we find while scanning
    gboolean                   show_hidden;
    gboolean 	               temp_show_hidden;// TRUE if hidden files are shown because the minibuffer leafname starts with a dot.

    FilterType                 filter;
    gchar*                     filter_string;   // Glob or regexp pattern
    gboolean                   filter_directories;

    GList*                     callbacks;       // of type ItemCallback
};

static void directory_ayyi_tree_model_interface_init (AyyiTreeModelIface*);

G_DEFINE_TYPE_WITH_CODE (
	VMDirectory,
	directory,
	G_TYPE_OBJECT,
	G_ADD_PRIVATE (VMDirectory)
	G_IMPLEMENT_INTERFACE (AYYI_TYPE_TREE_MODEL, directory_ayyi_tree_model_interface_init)
)

static GType              directory_real_get_column_type (AyyiTreeModel*, gint index);
static AyyiTreeModelFlags directory_real_get_flags       (AyyiTreeModel*);
static gboolean           directory_real_get_iter        (AyyiTreeModel*, AyyiTreeIter*, AyyiTreePath*);
static gint               directory_real_get_n_columns   (AyyiTreeModel*);
static AyyiTreePath*      directory_real_get_treepath    (AyyiTreeModel*, AyyiTreeIter*);
static void               directory_real_get_value       (AyyiTreeModel*, AyyiTreeIter*, gint column, GValue*);
static gboolean           directory_real_iter_children   (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* parent);
static gboolean           directory_real_iter_has_child  (AyyiTreeModel*, AyyiTreeIter*);
static gint               directory_real_iter_n_children (AyyiTreeModel*, AyyiTreeIter*);
static gboolean           directory_real_iter_next       (AyyiTreeModel*, AyyiTreeIter*);
static gboolean           directory_real_iter_nth_child  (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* parent, gint n);
static gboolean           directory_real_iter_parent     (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter* child);
static void               directory_delete_rows          (AyyiTreeModel*, void (*foreach)(AyyiTreeModelForeachFunc));

static void               directory_finalize             (GObject*);


static GType
directory_real_get_column_type (AyyiTreeModel* base, gint index)
{
	GType result = 0UL;
	result = (GType) NULL;
	return result;
}


static AyyiTreeModelFlags
directory_real_get_flags (AyyiTreeModel* base)
{
	return AYYI_TREE_MODEL_LIST_ONLY;
}


static gboolean
directory_real_get_iter (AyyiTreeModel* model, AyyiTreeIter* iter, AyyiTreePath* path)
{
	g_return_val_if_fail (path, FALSE);

	gint* indices = ayyi_tree_path_get_indices (path);
	if(indices){
		int i = indices[0];
		int n = directory_real_iter_n_children (model, NULL);
		if(i < n){
			if (iter) {
				*iter = (AyyiTreeIter){.user_data = GINT_TO_POINTER(i)};
			}
			return TRUE;
		}
	}
	return FALSE;
}


static gint
directory_real_get_n_columns (AyyiTreeModel* base)
{
	gint result = 0;
	return result;
}


static AyyiTreePath*
directory_real_get_treepath (AyyiTreeModel* base, AyyiTreeIter* iter)
{
	g_return_val_if_fail (iter, NULL);

	AyyiTreePath* path = ayyi_tree_path_new_from_indices(GPOINTER_TO_INT(iter->user_data), -1);
	return path;
}


static void
directory_real_get_value (AyyiTreeModel* model, AyyiTreeIter* iter, gint column, GValue* value)
{
	g_return_if_fail (iter);

	VMDirectory* vmd = (VMDirectory*)model;
	GPtrArray* items = vmd->items;
	ViewItem* item = items->pdata[GPOINTER_TO_INT(iter->user_data)];

	g_value_set_string(value, item->item->leafname);
}


static gboolean
directory_real_iter_children (AyyiTreeModel* base, AyyiTreeIter* iter, AyyiTreeIter* parent)
{
	if (iter) {
		AyyiTreeIter _iter = {0,};
		*iter = _iter;
	}
	return TRUE;
}


static gboolean
directory_real_iter_has_child (AyyiTreeModel* base, AyyiTreeIter* iter)
{
	return FALSE;
}


static gint
directory_real_iter_n_children (AyyiTreeModel* model, AyyiTreeIter* iter)
{
	VMDirectory* vmd = (VMDirectory*)model;
#if 0
	VMDirectoryPrivate* v = vmd->priv;
	Directory* d = v->directory;

	if(d)
		return g_hash_table_size(d->known_items) + d->new_items->len;
	else{
		gwarn("directory not set");
		return 0;
	}
#else
	return vmd->items->len;
#endif
}


static gboolean
directory_real_iter_next (AyyiTreeModel* model, AyyiTreeIter* iter)
{
	g_return_val_if_fail (iter, FALSE);

	VMDirectory* vmd = (VMDirectory*)model;
	VMDirectoryPrivate* v = vmd->priv;
	Directory* d = v->directory;

	if(!d) return FALSE; // model not yet created

	int index = GPOINTER_TO_INT(iter->user_data);

	// TODO may need to use vmd->items here instead of the hash table, not sure how reliable the hash table is
	return (d && (index < g_hash_table_size(d->known_items) + d->new_items->len))
		? (iter->user_data = GINT_TO_POINTER(++index), TRUE)
		: FALSE;
}


static gboolean
directory_real_iter_nth_child (AyyiTreeModel* model, AyyiTreeIter* iter, AyyiTreeIter* parent, gint n)
{
	VMDirectory* vmd = (VMDirectory*)model;

	if(n >= vmd->items->len) return false;

	*iter = (AyyiTreeIter){.user_data = GINT_TO_POINTER(n)};

	return TRUE;
}


static gboolean
directory_real_iter_parent (AyyiTreeModel* base, AyyiTreeIter* iter, AyyiTreeIter* child)
{
	g_return_val_if_fail (child, FALSE);

	AyyiTreeIter _iter = {0,};
	if (iter) {
		*iter = _iter;
	}
	return TRUE;
}


/*
 *  Implement the 'delete_rows' extension to GtkTreeModel interface
 */
static void
directory_delete_rows (AyyiTreeModel* model, void (*foreach)(AyyiTreeModelForeachFunc))
{
	PF;

	gboolean directory_delete_rows_item (AyyiTreeModel* model, AyyiTreePath* path, AyyiTreeIter* iter, gpointer out)
	{
		VMDirectory* vmd = (VMDirectory*)model;
#if 0
		Directory* dir = vmd->priv->directory;

		GValue value = {G_TYPE_STRING,};
		directory_real_get_value (model, iter, 0, &value);
		const char* leafname = g_value_get_string(&value);

		DirItem* item = g_hash_table_lookup(dir->known_items, leafname);
#else
		GPtrArray* items = vmd->items;
		ViewItem* item = items->pdata[GPOINTER_TO_INT(iter->user_data)];
#endif
		if(item){
			char filepath[FILENAME_MAX] = {0,};
			snprintf(filepath, FILENAME_MAX-1, "%s/%s", vm_directory_get_path(vmd), item->item->leafname);
			dbg(1, "will delete: %i: %s", GPOINTER_TO_INT(iter->user_data), filepath);

			// TODO this removes from view.model - need to also delete from directory model
			g_ptr_array_remove_index(items, GPOINTER_TO_INT(iter->user_data));

			file_deleter_add (filepath);

			ayyi_tree_model_row_deleted(model, path);
		}

		return true;
	}

	foreach(directory_delete_rows_item);
}


VMDirectory*
vm_directory_construct (GType object_type)
{
	VMDirectory* self = (VMDirectory*) g_object_new (object_type, NULL);

	self->items = g_ptr_array_new();
	g_ptr_array_set_free_func(self->items, g_free);

	return self;
}


VMDirectory*
vm_directory_new (void)
{
	return vm_directory_construct (TYPE_DIRECTORY);
}


static void
directory_class_init (VMDirectoryClass* klass)
{
	vm_directory_parent_class = g_type_class_peek_parent (klass);
	G_OBJECT_CLASS (klass)->finalize = directory_finalize;
}


static void
directory_ayyi_tree_model_interface_init (AyyiTreeModelIface* iface)
{
	directory_ayyi_tree_model_parent_iface = g_type_interface_peek_parent (iface);

	iface->get_column_type = (GType    (*)     (AyyiTreeModel*, gint))                         directory_real_get_column_type;
	iface->get_flags       = (AyyiTreeModelFlags(*)(AyyiTreeModel*))                           directory_real_get_flags;
	iface->get_iter        = (gboolean (*)     (AyyiTreeModel*, AyyiTreeIter*, AyyiTreePath*)) directory_real_get_iter;
	iface->get_n_columns   = (gint     (*)     (AyyiTreeModel*))                               directory_real_get_n_columns;
	iface->get_path        = (AyyiTreePath* (*)(AyyiTreeModel*, AyyiTreeIter*))                directory_real_get_treepath;
	iface->get_value       = (void     (*)     (AyyiTreeModel*, AyyiTreeIter*, gint, GValue*)) directory_real_get_value;
	iface->iter_children   = (gboolean (*)     (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter*)) directory_real_iter_children;
	iface->iter_has_child  = (gboolean (*)     (AyyiTreeModel*, AyyiTreeIter*))                directory_real_iter_has_child;
	iface->iter_n_children = (gint     (*)     (AyyiTreeModel*, AyyiTreeIter*))                directory_real_iter_n_children;
	iface->iter_next       = (gboolean (*)     (AyyiTreeModel*, AyyiTreeIter*))                directory_real_iter_next;
	iface->iter_nth_child  = (gboolean (*)     (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter*, gint)) directory_real_iter_nth_child;
	iface->iter_parent     = (gboolean (*)     (AyyiTreeModel*, AyyiTreeIter*, AyyiTreeIter*)) directory_real_iter_parent;
	iface->delete_rows     = (void     (*)     (AyyiTreeModel*, void (*foreach)(AyyiTreeModelForeachFunc))) directory_delete_rows;
}


static void
directory_init (VMDirectory* self)
{
	self->priv = directory_get_instance_private(self);
}


static void
directory_finalize (GObject* obj)
{
	VMDirectory* vd = G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_DIRECTORY, VMDirectory);
	VMDirectoryPrivate* v = vd->priv;

	g_clear_pointer(&v->sym_path, g_free);
	g_clear_pointer(&v->real_path, g_free);

	g_list_free_full(vd->priv->callbacks, g_free);
	vd->priv->callbacks = NULL;

	g_ptr_array_free(vd->items, true);
	vd->items = NULL;

	G_OBJECT_CLASS (vm_directory_parent_class)->finalize (obj);
}


/*
 *   Look through all items we want to display, and queue a recheck on any
 *   that require it.
 */
static void
queue_interesting (VMDirectory* vmd)
{
	VMDirectoryPrivate* v = vmd->priv;
	DirItem* item;
	ViewIter iter;

	int start, end;
	VIEW_IFACE_GET_CLASS(vmd->view)->get_visible(vmd->view, &start, &end);

	VIEW_IFACE_GET_CLASS(vmd->view)->get_iter(vmd->view, &iter, 0);
	while ((item = iter.next(&iter))) {
		if (iter.i > start - 1 && iter.i < end + 100 && item->flags & ITEM_FLAG_NEED_RESCAN_QUEUE) dir_queue_recheck(v->directory, item);
	}
}


void
_update_display (Directory* dir, DirAction action, GPtrArray* items, VMDirectory* vmd)
{
	VMDirectoryPrivate* v = vmd->priv;
	ViewIface* view = (ViewIface*)vmd->view;
	g_return_if_fail(view);

	switch (action)
	{
		case DIR_ADD:
			dbg(1, "DIR_ADD...");
			VIEW_IFACE_GET_CLASS(view)->add_items(view, items);
			break;
		case DIR_REMOVE:
//			view_delete_if(view, if_deleted, items);
			//toolbar_update_info(filer_window);
			break;
		case DIR_START_SCAN:
			dbg(1, "DIR_START_SCAN");
//			set_scanning_display(fm, TRUE);
//			file_manager__on_dir_changed();
			//toolbar_update_info(filer_window);
			break;
		case DIR_END_SCAN:
			dbg(1, "DIR_END_SCAN");
/*
			//if (filer_window->window->window) gdk_window_set_cursor(filer_window->window->window, NULL);
			set_scanning_display(fm, FALSE);
			//toolbar_update_info(filer_window);
			open_filer_window(fm);

			if (fm->had_cursor && !view_cursor_visible(view))
			{
				ViewIter start;
				view_get_iter(view, &start, 0);
				if (start.next(&start)) view_cursor_to_iter(view, &start);
				view_show_cursor(view);
				fm->had_cursor = FALSE;
			}
			//if (fm->auto_select) display_set_autoselect(filer_window, fm->auto_select);
			//null_g_free(&fm->auto_select);

			fm__create_thumbs(fm);

			if (fm->thumb_queue) start_thumb_scanning(fm);
*/

			break;
		case DIR_UPDATE:
			dbg(1, "DIR_UPDATE");
			VIEW_IFACE_GET_CLASS(view)->update_items(view, items);
			if(items){
				GList* l = v->callbacks;
				for(;l;){
					ItemCallback* cbi = l->data;
					for(int i=0;i<items->len;i++){
						DirItem* item = items->pdata[i];
						if(item == cbi->item){
							cbi->fn(vmd, item, cbi->user_data);

							GList* remove = l;
							g_clear_pointer(&remove->data, g_free);
							l = l->next;
							v->callbacks = g_list_delete_link(v->callbacks, remove);
							goto next;
						}
					}
					l = l->next;
					next:;
				}
			}
			break;
		case DIR_ERROR_CHANGED:
			//filer_set_title(filer_window);
			break;
		case DIR_QUEUE_INTERESTING:
			dbg(1, "DIR_QUEUE_INTERESTING");
			queue_interesting(vmd);
			break;
	}
}


static void
_attach (VMDirectory* vmd)
{
	VMDirectoryPrivate* v = vmd->priv;

	//gdk_window_set_cursor(filer_window->window->window, busy_cursor);
	view_clear(vmd->view);
	v->scanning = TRUE;
	dir_attach(v->directory, (DirCallback)_update_display, vmd);
	//filer_set_title(filer_window);
	//bookmarks_add_history(filer_window->sym_path);

	if (v->directory->error)
	{
#if 0
		if (spring_in_progress)
			g_printerr(_("Error scanning '%s':\n%s\n"),
				filer_window->sym_path,
				fm->directory->error);
		else
#endif
			//delayed_error(_("Error scanning '%s':\n%s"), filer_window->sym_path, filer_window->directory->error);
			dbg(0, "Error scanning '%s':\n%s", v->sym_path, v->directory->error);
	}
}


static void
detach (VMDirectory* vmd)
{
	VMDirectoryPrivate* v = vmd->priv;
	g_return_if_fail(v->directory);

	dir_detach(v->directory, (DirCallback)_update_display, vmd);
	_g_object_unref0(v->directory);
}


/*
 *  Remove trailing /s from path (modified in place)
 */
static void
tidy_sympath (gchar* path)
{
	g_return_if_fail(path != NULL);

	int l = strlen(path);
	while (l > 1 && path[l - 1] == '/') {
		l--;
		path[l] = '\0';
	}
}


/*
 *   Make filer_window display path. When finished, highlight item 'from', or
 *   the first item if from is NULL. If there is currently no cursor then
 *   simply wink 'from' (if not NULL).
 *   If the cause was a key event and we resize, warp the pointer.
 *
 *   Call with NULL path to free previously created resources
 */
void
vm_directory_set_path (VMDirectory* vmd, const char* path)
{
	const char* from = NULL;

	//dir_rescan();

	VMDirectoryPrivate* v = vmd->priv;

	dbg(2, "%s", path);
	g_return_if_fail(vmd);

	if(!path){
		if(v->directory) detach(vmd);
		v->directory = NULL;
		g_clear_pointer(&v->real_path, g_free);
		g_clear_pointer(&v->sym_path, g_free);
		return;
	}

	//fm__cancel_thumbnails(fm);

	char* sym_path = g_strdup(path);
	char* real_path = pathdup(path);
	Directory* new_dir = g_fscache_lookup(dir_cache, real_path);

	if (!new_dir) {
		dbg(0, "directory '%s' is not accessible", sym_path);
		g_free(real_path);
		g_free(sym_path);
		return;
	}

	char* from_dup = from && *from ? g_strdup(from) : NULL;

	if(v->directory) detach(vmd);
	g_free(v->real_path);
	g_free(v->sym_path);
	v->real_path = real_path;
	v->sym_path = sym_path;
	tidy_sympath(v->sym_path);

	v->directory = new_dir;

	g_free(v->auto_select);
	v->auto_select = from_dup;

	//had_cursor = had_cursor || view_cursor_visible(fm->view);

	//filer_set_title(filer_window);
	//if (filer_window->window->window) gdk_window_set_role(filer_window->window->window, filer_window->sym_path);
	view_cursor_to_iter(vmd->view, NULL);

	_attach(vmd);
}


const char*
vm_directory_get_path (VMDirectory* vmd)
{
	VMDirectoryPrivate* v = vmd->priv;

	return v->sym_path;
}


static inline gboolean
is_hidden (const char* dir, DirItem* item)
{
	// If the leaf name starts with '.' then the item is hidden
	if(item->leafname[0] == '.')
		return TRUE;

	return FALSE;
}


gboolean
vm_directory_match_filter (VMDirectory* vmd, DirItem* item)
{
	g_return_val_if_fail(item, FALSE);
	VMDirectoryPrivate* v = vmd->priv;

	if(is_hidden(v->real_path, item) && (!v->temp_show_hidden && !v->show_hidden)){
		return FALSE;
	}

	switch(v->filter) {
		case FILER_SHOW_GLOB:
			return fnmatch(v->filter_string, item->leafname, 0)==0 || (item->base_type==TYPE_DIRECTORY && !v->filter_directories);

		case FILER_SHOW_ALL:
		default:
			break;
	}
	return TRUE;
}


void
vm_directory_fetch_item (VMDirectory* vmd, DirItem* item, DirItemFn callback, gpointer user_data)
{
	// fn was added because Directory is private. perhaps does not need to be

	VMDirectoryPrivate* v = vmd->priv;

	g_return_if_fail(item);

	if (callback) {
		vm_directory_wait_item (vmd, item, callback, user_data);
	}

	dir_queue_recheck(v->directory, item);
}


/*
 *   The item must already be queued for this to work
 */
void
vm_directory_wait_item (VMDirectory* vmd, DirItem* item, DirItemFn callback, gpointer user_data)
{
	VMDirectoryPrivate* v = vmd->priv;

	g_return_if_fail(callback);

	v->callbacks = g_list_prepend(v->callbacks, AGL_NEW(ItemCallback,
		.item = item,
		.fn = callback,
		.user_data = user_data
	));
}
