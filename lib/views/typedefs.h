/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2018-2018 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __views_typedefs_h__
#define __views_typedefs_h__

#ifndef true
  #define true TRUE
#endif

#ifndef false
  #define false FALSE
#endif

#ifndef bool
  #define bool int
#endif

typedef struct { int start, end; } iRange;

#endif
