/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2018-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | statusbar.c                                                          |
 +----------------------------------------------------------------------+
 |
 */

#define WF_USE_TEXTURE_CACHE

#include "config.h"
#include "debug/debug.h"
#include "behaviours/cache.h"
#include "views/button.h"
#include "views/statusbar.h"

static AGl* agl = NULL;

static void statusbar__free (AGlActor*);
static bool statusbar_event (AGlActor*, AGlEvent*, AGliPt);

static AGlActorClass actor_class = {0, "Statusbar", (AGlActorNew*)statusbar, statusbar__free};


AGlActorClass*
statusbar_get_class ()
{
	static bool init_done = false;

	if (!init_done) {
		agl = agl_get_instance();
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());
		init_done = true;
	}

	return &actor_class;
}


static void
statusbar__set_state (AGlActor* actor)
{
}


static bool
statusbar__paint (AGlActor* actor)
{
	Statusbar* statusbar = (Statusbar*)actor;

	if (!actor->disabled)
		agl_print(0, 2, 0, 0xffffffff, statusbar->text);

	return true;
}


AGlActor*
statusbar (void* _)
{
	statusbar_get_class();

	Statusbar* view = agl_actor__new(Statusbar,
		.actor = {
			.class = &actor_class,
			.set_state = statusbar__set_state,
			.paint = statusbar__paint,
			.on_event = statusbar_event,
		},
	);

	return (AGlActor*)view;
}


static void
statusbar__free (AGlActor* actor)
{
	Statusbar* statusbar = (Statusbar*)actor;

	g_clear_pointer(&statusbar->text, g_free);
	g_free(actor);
}


void
statusbar_set (Statusbar* statusbar, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	char* text = g_strdup_vprintf(fmt, args);
	va_end(args); // text now contains the string.

	if(!text || !statusbar->text || strcmp(text, statusbar->text)){
		if(statusbar->text) g_free(statusbar->text);
		statusbar->text = text;

		agl_actor__invalidate((AGlActor*)statusbar);
	} else {
		if (text) g_free(text);
	}
}


static bool
statusbar_event (AGlActor* actor, AGlEvent* event, AGliPt xy)
{
	return AGL_NOT_HANDLED;
}

