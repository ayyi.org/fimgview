/**
* +----------------------------------------------------------------------+
* | This file is part of imgview                                         |
* | copyright (C) 2018-2023 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#pragma once

#include "agl/observable.h"
#include "texture_cache/texture_cache.h"

typedef struct {
    AGlActor     actor;
    char*        img;
    Texture      texture;
    AGlObservable* rendered; // stream of type char* to notify when images were rendered
    bool         centered; // image is centered in the actor (by default is at top left)
    float        zoom;
    AGlPtf       offset;   // applies when zooming
    WfAnimatable _zoom;
    WfAnimatable _offset;
} ImgView;


AGlActorClass* img_view_get_class ();

AGlActor* img_view          (AGlActor*);
Texture*  img_view_load_img (ImgView*, char*, MIME_type*, gpointer ref);
void      img_view_show_img (ImgView*, char*, MIME_type*, gpointer ref);
