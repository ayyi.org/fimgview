/* AyyiIconTheme - a loader for icon themes
 * gtk-icon-loader.h Copyright (C) 2002, 2003 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __ayyi_icon_theme_h__
#define __ayyi_icon_theme_h__

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk/gdk.h>
#ifdef __GTK_H__
#error "AyyiIconTheme should not be used with Gtk"
#endif

G_BEGIN_DECLS

#define AYYI_TYPE_ICON_INFO              (gtk_icon_info_get_type ())

#define AYYI_TYPE_ICON_THEME             (gtk_icon_theme_get_type ())
#define AYYI_ICON_THEME(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_ICON_THEME, AyyiIconTheme))
#define AYYI_ICON_THEME_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_ICON_THEME, AyyiIconThemeClass))
#define AYYI_IS_ICON_THEME(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_ICON_THEME))
#define AYYI_IS_ICON_THEME_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_ICON_THEME))
#define AYYI_ICON_THEME_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_ICON_THEME, AyyiIconThemeClass))

typedef struct _AyyiIconInfo         AyyiIconInfo;
typedef struct _AyyiIconTheme        AyyiIconTheme;
typedef struct _AyyiIconThemeClass   AyyiIconThemeClass;
typedef struct _AyyiIconThemePrivate AyyiIconThemePrivate;

struct _AyyiIconTheme
{
   GObject parent_instance;

   AyyiIconThemePrivate* (priv);
};

struct _AyyiIconThemeClass
{
   GObjectClass parent_class;

   void (*changed) (AyyiIconTheme*);
};

/**
 * AyyiIconLookupFlags:
 * @AYYI_ICON_LOOKUP_NO_SVG: Never return SVG icons, even if gdk-pixbuf
 *   supports them. Cannot be used together with %AYYI_ICON_LOOKUP_FORCE_SVG.
 * @AYYI_ICON_LOOKUP_FORCE_SVG: Return SVG icons, even if gdk-pixbuf
 *   doesn't support them.
 *   Cannot be used together with %AYYI_ICON_LOOKUP_NO_SVG.
 * @AYYI_ICON_LOOKUP_USE_BUILTIN: When passed to
 *   gtk_icon_theme_lookup_icon() includes builtin icons
 *   as well as files. For a builtin icon, gtk_icon_info_get_filename()
 *   returns %NULL and you need to call gtk_icon_info_get_builtin_pixbuf().
 * @AYYI_ICON_LOOKUP_GENERIC_FALLBACK: Try to shorten icon name at '-'
 *   characters before looking at inherited themes. For more general
 *   fallback, see gtk_icon_theme_choose_icon(). Since 2.12.
 * @AYYI_ICON_LOOKUP_FORCE_SIZE: Always return the icon scaled to the
 *   requested size. Since 2.14.
 * 
 * Used to specify options for gtk_icon_theme_lookup_icon()
 **/
typedef enum
{
  AYYI_ICON_LOOKUP_NO_SVG           = 1 << 0,
  AYYI_ICON_LOOKUP_FORCE_SVG        = 1 << 1,
  AYYI_ICON_LOOKUP_USE_BUILTIN      = 1 << 2,
  AYYI_ICON_LOOKUP_GENERIC_FALLBACK = 1 << 3,
  AYYI_ICON_LOOKUP_FORCE_SIZE       = 1 << 4
} AyyiIconLookupFlags;

#define AYYI_ICON_THEME_ERROR gtk_icon_theme_error_quark ()

/**
 * AyyiIconThemeError:
 * @AYYI_ICON_THEME_NOT_FOUND: The icon specified does not exist in the theme
 * @AYYI_ICON_THEME_FAILED: An unspecified error occurred.
 * 
 * Error codes for AyyiIconTheme operations.
 **/
typedef enum {
  AYYI_ICON_THEME_NOT_FOUND,
  AYYI_ICON_THEME_FAILED
} AyyiIconThemeError;

GQuark         gtk_icon_theme_error_quark           (void);

GType          gtk_icon_theme_get_type              (void) G_GNUC_CONST;

AyyiIconTheme* gtk_icon_theme_new                   (void);
AyyiIconTheme* gtk_icon_theme_get_default           (void);
AyyiIconTheme* gtk_icon_theme_get_for_screen        (GdkScreen*);
void           gtk_icon_theme_set_screen            (AyyiIconTheme*, GdkScreen*);

void           gtk_icon_theme_set_search_path       (AyyiIconTheme*, const gchar *path[], gint n_elements);
void           gtk_icon_theme_get_search_path       (AyyiIconTheme*, gchar      **path[], gint *n_elements);
void           gtk_icon_theme_append_search_path    (AyyiIconTheme*, const gchar *path);
void           gtk_icon_theme_prepend_search_path   (AyyiIconTheme*, const gchar *path);

void           gtk_icon_theme_set_custom_theme      (AyyiIconTheme*, const gchar *theme_name);

gboolean       gtk_icon_theme_has_icon              (AyyiIconTheme*, const gchar *icon_name);
gint*          gtk_icon_theme_get_icon_sizes        (AyyiIconTheme*, const gchar *icon_name);
AyyiIconInfo*  gtk_icon_theme_lookup_icon           (AyyiIconTheme*, const gchar *icon_name, gint size, AyyiIconLookupFlags);
AyyiIconInfo*  gtk_icon_theme_choose_icon           (AyyiIconTheme*, const gchar *icon_names[], gint size, AyyiIconLookupFlags);
GdkPixbuf*     gtk_icon_theme_load_icon             (AyyiIconTheme*, const gchar *icon_name, gint size, AyyiIconLookupFlags, GError**);

AyyiIconInfo*  gtk_icon_theme_lookup_by_gicon       (AyyiIconTheme*, GIcon*, gint size, AyyiIconLookupFlags);

GList*         gtk_icon_theme_list_icons            (AyyiIconTheme*, const gchar *context);
GList*         gtk_icon_theme_list_contexts         (AyyiIconTheme*);
char*          gtk_icon_theme_get_example_icon_name (AyyiIconTheme*);

gboolean       gtk_icon_theme_rescan_if_needed      (AyyiIconTheme*);

void           gtk_icon_theme_add_builtin_icon      (const gchar *icon_name, gint size, GdkPixbuf*);

GType          gtk_icon_info_get_type               (void) G_GNUC_CONST;
AyyiIconInfo*  gtk_icon_info_copy                   (AyyiIconInfo*);
void           gtk_icon_info_free                   (AyyiIconInfo*);

AyyiIconInfo*  gtk_icon_info_new_for_pixbuf         (AyyiIconTheme*, GdkPixbuf*);

gint           gtk_icon_info_get_base_size          (AyyiIconInfo*);
const gchar*   gtk_icon_info_get_filename           (AyyiIconInfo*);
GdkPixbuf*     gtk_icon_info_get_builtin_pixbuf     (AyyiIconInfo*);
GdkPixbuf*     gtk_icon_info_load_icon              (AyyiIconInfo*, GError**);
void           gtk_icon_info_set_raw_coordinates    (AyyiIconInfo*, gboolean raw_coordinates);

gboolean       gtk_icon_info_get_embedded_rect      (AyyiIconInfo*, GdkRectangle*);
gboolean       gtk_icon_info_get_attach_points      (AyyiIconInfo*, GdkPoint**, gint *n_points);
const gchar*   gtk_icon_info_get_display_name       (AyyiIconInfo*);

/* Non-public methods */
void          _gtk_icon_theme_check_reload          (GdkDisplay*);
void          _gtk_icon_theme_ensure_builtin_cache  (void);

G_END_DECLS

#define GtkIconInfo AyyiIconInfo
#define GTK_ICON_LOOKUP_FORCE_SIZE AYYI_ICON_LOOKUP_FORCE_SIZE

#endif
