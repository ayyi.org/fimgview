/* gtkiconcache.h
 * Copyright (C) 2004  Anders Carlsson <andersca@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __ayyi_icon_cache_h__
#define __ayyi_icon_cache_h__

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk/gdk.h>

typedef struct _AyyiIconCache AyyiIconCache;
typedef struct _AyyiIconData AyyiIconData;

struct _AyyiIconData
{
   gboolean  has_embedded_rect;
   gint      x0, y0, x1, y1;
  
   GdkPoint* attach_points;
   gint      n_attach_points;

   gchar*    display_name;
};

AyyiIconCache *_gtk_icon_cache_new                   (const gchar *data);
AyyiIconCache *_gtk_icon_cache_new_for_path          (const gchar *path);
gint           _gtk_icon_cache_get_directory_index   (AyyiIconCache*, const gchar *directory);
gboolean       _gtk_icon_cache_has_icon              (AyyiIconCache*, const gchar *icon_name);
gboolean       _gtk_icon_cache_has_icon_in_directory (AyyiIconCache*, const gchar *icon_name, const gchar *directory);
void	       _gtk_icon_cache_add_icons             (AyyiIconCache*, const gchar *directory, GHashTable*);

gint           _gtk_icon_cache_get_icon_flags        (AyyiIconCache*, const gchar *icon_name, gint directory_index);
GdkPixbuf     *_gtk_icon_cache_get_icon              (AyyiIconCache*, const gchar *icon_name, gint directory_index);
AyyiIconData  *_gtk_icon_cache_get_icon_data         (AyyiIconCache*, const gchar *icon_name, gint directory_index);

AyyiIconCache *_gtk_icon_cache_ref                   (AyyiIconCache*);
void           _gtk_icon_cache_unref                 (AyyiIconCache*);

#endif
