/*
 +----------------------------------------------------------------------+
 | copyright (C) 2012-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

typedef struct
{
	guint         id;
	gpointer      ref;
	AGliSize      size;
	int           time_stamp;
} Texture;

typedef void  (*OnSteal) (Texture*);

typedef struct _texture_cache TextureCache;
struct _texture_cache
{
	GArray*  t;             // type Texture
	OnSteal  on_steal;
};

void     texture_cache_init_         ();
void     texture_cache_set_on_steal_ (OnSteal);
Texture* texture_cache_lookup_       (gpointer);
Texture* texture_cache_assign_new_   (gpointer);
void     texture_cache_freshen_      (gpointer);
void     texture_cache_remove_       (gpointer);
