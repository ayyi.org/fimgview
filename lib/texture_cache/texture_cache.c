/*
 +----------------------------------------------------------------------+
 | copyright (C) 2012-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | TEXTURE CACHE                                                        |
 | Do not keep references to Texture objects as the location may change |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <glib.h>
#include <GL/glx.h>
#include "agl/utils.h"
#include "debug/debug.h"
#include "lib/views/typedefs.h"
#include "agl/typedefs.h"
#include "texture_cache.h"

#define WF_TEXTURE_ALLOCATION_INCREMENT 20
#define WF_TEXTURE_MAX                  40

static int time_stamp = 0;
static TextureCache* c1 = NULL;

static void texture_cache_gen              (TextureCache*);
static Texture* texture_cache_get          (TextureCache*, int);
static int  texture_cache_get_new          (TextureCache*);
static void texture_cache_assign           (TextureCache*, int, gpointer);
static int  texture_cache_find_empty       (TextureCache*);
static int  texture_cache_steal            (TextureCache*);
static void texture_cache_print            ();
static int  texture_cache_lookup_idx       (TextureCache*, gpointer);
static void texture_cache_unassign         (TextureCache*, gpointer);
static void texture_cache_shrink           (TextureCache*, int);
#ifdef _DEBUG
static int  texture_cache_lookup_idx_by_id (TextureCache*, guint);
static int  texture_cache_count_used       (TextureCache*);
#endif


void
texture_cache_init_ ()
{
	if (c1) return;

	c1 = AGL_NEW(TextureCache,
		.t = g_array_set_size(g_array_new(FALSE, TRUE, sizeof(Texture)), 0),
	);
}


void
texture_cache_set_on_steal_ (OnSteal fn)
{
	c1->on_steal = fn;
}


static void
texture_cache_gen (TextureCache* c)
{
	// Create an additional set of available textures.

	if (!c1) texture_cache_init_();

	static bool error_shown = false;

#if 0
	//check all textures
	{
		int i; for(i=0;i<c->t->len;i++){
			Texture* tx = &g_array_index(c->t, Texture, i);
			if(!glIsTexture(tx->id)) pwarn("not texture! %i: %i", i, tx->id);
		}
	}
#endif

	int size = c->t->len + WF_TEXTURE_ALLOCATION_INCREMENT;
	if (size > WF_TEXTURE_MAX){ if(_debug_){ pwarn("texture allocation full"); if(!error_shown) texture_cache_print(); error_shown = true;} return; }
	c->t = g_array_set_size(c->t, size);

	guint textures[WF_TEXTURE_ALLOCATION_INCREMENT];
	glGenTextures(WF_TEXTURE_ALLOCATION_INCREMENT, textures);
	dbg(2, "size=%i-->%i textures=%u...%u", size-WF_TEXTURE_ALLOCATION_INCREMENT, size, textures[0], textures[WF_TEXTURE_ALLOCATION_INCREMENT-1]);
	//gl_warn("failed to generate %i textures. cache_size=%i", WF_TEXTURE_ALLOCATION_INCREMENT, c->t->len);

	int t;
#ifdef _DEBUG
	//check the new textures are not already in the cache
	for (t=0;t< WF_TEXTURE_ALLOCATION_INCREMENT;t++) {
		int idx = texture_cache_lookup_idx_by_id (c, textures[t]);
		if (idx > -1) {
			Texture* tx = &g_array_index(c->t, Texture, t);
			pwarn("given duplicate texture id: %i wf=%p b=%i", textures[t], tx->wb.waveform, tx->wb.block);
		}
	}
#endif

	int i = 0;
	for (t=c->t->len-WF_TEXTURE_ALLOCATION_INCREMENT;t<c->t->len;t++, i++) {
		Texture* tx = &g_array_index(c->t, Texture, t);
		tx->id = textures[i];
	}
}


static void
texture_cache_shrink (TextureCache* c, int idx)
{
	dbg(1, "*** %i-->%i", c->t->len, idx);
	g_return_if_fail(!(idx % WF_TEXTURE_ALLOCATION_INCREMENT));

	guint textures[WF_TEXTURE_ALLOCATION_INCREMENT];

	int i = 0;
	for (int t=idx;t<idx+WF_TEXTURE_ALLOCATION_INCREMENT;t++, i++) {
		Texture* tx = &g_array_index(c->t, Texture, t);
		textures[i] = tx->id;
	}
	glDeleteTextures(WF_TEXTURE_ALLOCATION_INCREMENT, textures);
	c->t = g_array_set_size(c->t, c->t->len - WF_TEXTURE_ALLOCATION_INCREMENT);
}


Texture*
texture_cache_assign_new_ (gpointer ref)
{
	int t = texture_cache_get_new(c1);
	Texture* texture = texture_cache_get(c1, t);
	texture_cache_assign(c1, t, ref);

	return texture;
}


#ifdef DEBUG
	static guint timeout = 0;

		static gboolean _texture_cache_print(gpointer data)
		{
			texture_cache_print();
			timeout = 0;
			return G_SOURCE_REMOVE;
		}
#endif

static void
texture_cache_assign (TextureCache* c, int t, gpointer ref)
{
	g_return_if_fail(t >= 0);
	g_return_if_fail(t < c->t->len);

	Texture* tx = &g_array_index(c->t, Texture, t);
	tx->ref = ref;
	tx->time_stamp = time_stamp++;
	dbg(2, "t=%i time=%i", t, time_stamp);

#ifdef DEBUG
	if (_debug_ > 1) {
		if (timeout) g_source_remove(timeout);
		timeout = g_timeout_add(1000, _texture_cache_print, NULL);
	}
#endif
}


void
texture_cache_freshen_ (gpointer ref)
{
	int i = texture_cache_lookup_idx(c1, ref);
	if (i > -1) {
		Texture* tx = &g_array_index(c1->t, Texture, i);
		tx->time_stamp = time_stamp++;
	}
}


	static guint idle_id = 0;

	static gboolean texture_cache_clean (gpointer user_data)
	{
		gboolean last_block_is_empty (TextureCache* c)
		{
			int m = c->t->len - 1;
			if (m == -1) return false;
			gboolean empty = true;
			for (int i=0;i<WF_TEXTURE_ALLOCATION_INCREMENT;i++) {
				Texture* tx = &g_array_index(c->t, Texture, m);
				if (tx->ref) {
					empty = false;
					break;
				}
				m--;
			}
			return empty;
		}
		int i = 0;
		while(
			(c1->t->len > WF_TEXTURE_ALLOCATION_INCREMENT) //dont delete last block
			&& last_block_is_empty(c1)
			&& (i++ < 10)
		) texture_cache_shrink(c1, c1->t->len - WF_TEXTURE_ALLOCATION_INCREMENT);

		idle_id = 0;
		return G_SOURCE_REMOVE;
	}

static void
texture_cache_queue_clean ()
{
	if (!idle_id) idle_id = g_idle_add(texture_cache_clean, NULL);
}


static void
texture_cache_unassign (TextureCache* c, gpointer ref)
{
	g_return_if_fail(ref);

	int i = 0;
	int t;
	while ((t = texture_cache_lookup_idx(c, ref)) > -1) {
		g_return_if_fail(t < c->t->len);

		Texture* tx = &g_array_index(c->t, Texture, t);
		g_return_if_fail(tx);
		tx->ref = NULL;
		tx->time_stamp = 0;
		dbg(2, "t=%i removed", t);
		i++;
		g_return_if_fail(i <= 4);
	}

	texture_cache_queue_clean();

	//texture_cache_print();
}


static Texture*
texture_cache_get (TextureCache* c, int t)
{
	g_return_val_if_fail(t < c->t->len, NULL);

	return &g_array_index(c->t, Texture, t);
}


Texture*
texture_cache_lookup_ (gpointer ref)
{
	for (int i=0;i<c1->t->len;i++) {
		Texture* t = &g_array_index(c1->t, Texture, i);
		if (t->ref == ref) {
			dbg(3, "found %p at %i", ref, i);
			return t;
		}
	}
	dbg(2, "not found: b=%p", ref);
	return NULL;
}


static int
texture_cache_lookup_idx (TextureCache* c, gpointer ref)
{
	for (int i=0;i<c->t->len;i++) {
		Texture* t = &g_array_index(c->t, Texture, i);
		if (t->ref == ref) {
			return i;
		}
	}
	dbg(2, "not found: b=%p", ref);
	return -1;
}


#ifdef WF_DEBUG
static int
texture_cache_lookup_idx_by_id (TextureCache* c, guint id)
{
	for (int i=0;i<c->t->len;i++) {
		Texture* t = &g_array_index(c->t, Texture, i);
		if (t->id == id) {
			return i;
		}
	}
	return -1;
}
#endif


/*
 *  Returns the array index
 */
static int
texture_cache_get_new (TextureCache* c)
{
	int t = texture_cache_find_empty(c);
	if (t < 0) {
		texture_cache_gen(c);
		t = texture_cache_find_empty(c);
		if (t < 0) {
			t = texture_cache_steal(c);
		}
	}
	return t;
}


static int
texture_cache_find_empty (TextureCache* c)
{
	for (int t=0;t<c->t->len;t++) {
		Texture* tx = &g_array_index(c->t, Texture, t);
		if (!tx->ref) {
			dbg(3, "%i", t);
			return t;
		}
	}
	return -1;
}


static int
texture_cache_steal (TextureCache* c)
{
	int oldest = -1;
	int n = -1;
	for (int t=0;t<c->t->len;t++) {
		Texture* tx = &g_array_index(c->t, Texture, t);
		if (tx->ref) {
			if (oldest == -1 || tx->time_stamp < oldest) {
				n = t;
				oldest = tx->time_stamp;
			}
		}
	}
	if (n > -1) {
		// clear all references to this texture

		dbg(2, "%i time=%i", oldest, ((Texture*)&g_array_index(c->t, Texture, n))->time_stamp);
		Texture* tex = (Texture*)&g_array_index(c->t, Texture, n);

		if (c->on_steal) c->on_steal(tex);
	}
	return n;
}


void
texture_cache_remove_ (gpointer ref)
{
	texture_cache_unassign(c1, ref);
}


#ifdef _DEBUG
int
texture_cache_count_by_object (GObject* w)
{
	int n_found = 0;
	for (int j=0;j<2;j++) {
		TextureCache* c = c1;
		for (int i=0;i<c->t->len;i++) {
			Texture* t = &g_array_index(c->t, Texture, i);
			if (t->wb.waveform == w) n_found++;
		}
	}
	return n_found;
}
#endif


#ifdef _DEBUG
static int
texture_cache_count_used (TextureCache* c)
{
	int n_used = 0;
	if (c->t->len) {
		for (int i=0;i<c->t->len;i++) {
			Texture* t = &g_array_index(c->t, Texture, i);
			if (t->ref) n_used++;
		}
	}
	return n_used;
}
#endif


static void
texture_cache_print ()
{
	TextureCache* c = c1;
	int n_used = 0;
	GList* waveforms = NULL;
	if (c->t->len) {
		printf("         %2s %4s  %3s   %-4s\n", "id", "ts", "b", "wvfm");
		for (int i=0;i<c->t->len;i++) {
			Texture* t = &g_array_index(c->t, Texture, i);
			if (t->ref) {
				n_used++;
				if (!g_list_find(waveforms, t->ref)) waveforms = g_list_append(waveforms, t->ref);
			}
			printf("    %3i: %2u %4i %4i\n", i, t->id, t->time_stamp, g_list_index(waveforms, t->ref) + 1);
		}
	}
	dbg(0, "array_size=%i n_used=%i n_waveforms=%i", c->t->len, n_used, g_list_length(waveforms));
	g_list_free(waveforms);
}

