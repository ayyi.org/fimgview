/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2019-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
* | FileDeleter                                                          |
* | Provides throttling where deleting of many files is required         |
* +----------------------------------------------------------------------+
*
*/

#ifndef __file_deleter_h__
#define __file_deleter_h__

#include "tree/observable.h"

Observable* file_deleter     ();
void        file_deleter_add (const char*);

#endif
