/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2019-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <glib.h>
#include <gio/gio.h>
#include "debug/debug.h"
#include "file_deleter.h"

#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

Observable* _file_deleter = NULL;

static GList* delete_queue = NULL;
static GFile* deleting[8] = {NULL,};

static void delete_callback (GObject*, GAsyncResult*, gpointer);


Observable*
file_deleter ()
{
	if(!_file_deleter)
		_file_deleter = observable_new();

	return _file_deleter;
}


static int
find_empty_slot ()
{
	for(int i=0;i<G_N_ELEMENTS(deleting);i++){
		if(!deleting[i])
			return i;
	}
	return -1;
}


static void
delete_next ()
{
	g_assert(delete_queue);

	int slot;
	if((slot = find_empty_slot()) < 0)
		return;

	GFile* file = deleting[slot] = delete_queue->data;
	GCancellable* cancellable = NULL;
	g_file_delete_async (file, G_PRIORITY_DEFAULT, cancellable, delete_callback, GINT_TO_POINTER(slot));

	delete_queue = g_list_remove(delete_queue, file);
}


static void
delete_callback (GObject* source, GAsyncResult* result, gpointer user_data)
{
	int slot = GPOINTER_TO_INT(user_data);

	GError* error = NULL;
	g_file_delete_finish((GFile*)source, result, &error);
	if(error){
		pwarn("error: %s", error->message);
		g_error_free(error);
	}

	_g_object_unref0(deleting[slot]);

	observable_set(_file_deleter, g_list_length(delete_queue));

	if(delete_queue)
		delete_next();
}


void
file_deleter_add (const char* filepath)
{
	if(!_file_deleter)
		_file_deleter = observable_new();

	GFile* file = g_file_new_for_path (filepath);
	delete_queue = g_list_prepend(delete_queue, file);

	observable_set(_file_deleter, g_list_length(delete_queue));

	delete_next();
}


